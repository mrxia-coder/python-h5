# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 21:57
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-体验文件操作.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1.打开文件
f = open('test.txt', 'w')

# 2.读写文件
f.write('aaa')

# 3.关闭文件
f.close()
