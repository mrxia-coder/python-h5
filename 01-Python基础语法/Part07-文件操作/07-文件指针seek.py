# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 22:35
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 07-文件指针seek.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# r相关
fr = open('test.txt', 'r+')

# 指针在开始位置
print(fr.read())

# 修改文件指针位置
fr.seek(2)
print(fr.read())

# 指针放到文件结尾，无法读取数据
fr.seek(0, 2)
print(fr.read())

fr.close()


# a相关
fa = open('test.txt', 'a+')

# 指针在结尾位置，无法读取数据
print(fa.read())

# 修改文件指针位置到最开始位置,能读取所有数据
fa.seek(0)
print(fa.read())

# 指针放到文件某一位置，可以读取指针之后的所有数据
fa.seek(12, 0)
print(fa.read())

fa.close()
