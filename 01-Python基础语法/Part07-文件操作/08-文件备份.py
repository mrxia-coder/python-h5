# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 22:49
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 08-文件备份.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 1.接收用户输入的目标文件名
be_back_file = input("请输入您要备份的文件名：").strip()

# 2.提取文件后缀
point = be_back_file.rfind('.')
# print(point)  # 文件名后缀前.的索引位置

# 3.组织备份文件名
bak_file_name = be_back_file[:point] + '-bak' + be_back_file[point:]
print(f"您需要备份的文件是:{be_back_file}")

# 4.打开源文件和备份文件
try:
    old_f = open(be_back_file, 'rb')
    new_f = open(bak_file_name, 'wb')

    # 5.读取源文件内容，写入到备份文件
    while True:
        content = old_f.read(1024)
        if len(content) == 0:
            break
        new_f.write(content)

    # 6.关闭文件
    old_f.close()
    new_f.close()
    print(f"备份成功！备份之后的文件是:{bak_file_name}")
except FileNotFoundError:
    print("未找到您需要备份的文件！")
