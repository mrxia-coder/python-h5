# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 23:14
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 09-os模块文件和文件夹操作.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import os


# 1. rename() 重命名
os.rename('1.txt', '10.txt')

# 2. remove() 删除文件
os.remove('10.txt')

# 3. mkdir()  创建文件夹
os.mkdir('myDir')

# 4. rmdir()  删除文件夹
os.rmdir('myDir')

# 5. getcwd()  获取当前目录
print(os.getcwd())

# 6. chdir()  改变目录
print(os.getcwd())
os.chdir('./Notes')
print(os.getcwd())

# 7. listdir()  获取目录列表
os.chdir('../')
print(os.listdir('./'))

# 8. rename()  重命名文件夹
os.rename('bb', 'vbb')
