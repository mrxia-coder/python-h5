# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 22:27
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 06-访问模式特点02.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
1. r+ 、w+ 和 a+ 的区别
    r+: 如果打开的文件不存在，则会报错;默认打开文件时文件指针在文件开头，能读取所有数据
    w+: 如果打开的文件不存在，则新建文件；默认打开文件时文件指针在文件开头，但是会清空之前文件中的内容，如果没有做写入操作，则只是清空原内容
    a+: 如果打开的文件不存在，则新建文件；默认打开文件时文件指针在文件结尾，因为文件读取时从指针所在位置向后读取数据的，所以在不操作指针位置的清空下，无法读取数据
2. 文件指针对数据读取的影响
"""

# r+
f = open('test.txt', 'r+')
# f = open('test1.txt', 'r+')  # 文件不存在，则报错
con = f.read()
print(con)
f.close()


# w+
f = open('test.txt', 'w+')
# f = open('test1.txt', 'w+')  # 文件不存在，则创建文件
con = f.read()
print(con)
f.close()


# a+
f = open('test.txt', 'a+')
# f = open('test1.txt', 'a+')  # 文件不存在，则创建文件
con = f.read()
print(con)
f.close()
