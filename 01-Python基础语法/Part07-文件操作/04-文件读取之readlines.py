# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 22:17
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-文件读取之readlines.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


f = open('test.txt', 'r')

content = f.readlines()
print(content)  # ['aaaaa\n', 'bbbbb\n', 'ccccc\n', 'ddddd\n', 'eeeee\n', 'fffff']

f.close()
