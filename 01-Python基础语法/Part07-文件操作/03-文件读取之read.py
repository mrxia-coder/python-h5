# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 22:11
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-文件读取之read.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


f = open('test.txt', 'r')

# print(f.read())  # read()不写参数表示读取文件的所有内容
print(f.read(10))  # 文件中如果有换行，那么\n换行符号也占用一个字节

f.close()
