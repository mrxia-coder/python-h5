# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 23:26
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 10-批量重命名.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import os

# 设置重命名标识：如果为1则添加指定字符，flag取值为2则删除指定字符
flag = 1

# 获取指定目录(目录最后必须带"/")
dir_name = './templates/'

# 获取指定目录的文件列表
file_list = os.listdir(dir_name)
# print(file_list)


# 遍历文件列表内的文件
for name in file_list:
    # 添加指定字符
    if flag == 1:
        new_name = 'Python_' + name
    # 删除指定字符
    elif flag == 2:
        num = len('Python_')
        new_name = name[num:]

    # 重命名
    os.rename(dir_name + name, dir_name + new_name)

print(os.listdir(dir_name))
