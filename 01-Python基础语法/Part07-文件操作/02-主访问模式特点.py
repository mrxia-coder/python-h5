# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 22:02
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-主访问模式特点.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
1. 访问模式对文件的影响
2. 访问模式对write()的影响
2. 访问模式是否可以省略
"""

# r: 如果文件不存在，会报错; 只读，不支持写入操作
f = open('test.txt', 'r')
f.close()

# w: 文件不存在，则新建文件并写入数据；只能写入数据，如果文件已经有数据，则会覆盖原有内容
f = open('1.txt', 'w')
f.write('123')
f.close()
f = open('1.txt', 'w')
f.write('456')
f.close()

# a: 如果文件不存在，则新建文件并追加数据；如果文件已经有数据，则会在原有内容的最后追加数据
f = open('2.txt', 'a')
f.write('abc')
f.close()
f = open('2.txt', 'a')
f.write('def')
f.close()


# 访问模式是否可以省略：如果是r模式，可以省略，其他的模式不能省略
# f = open('100.txt')  # 如果文件不存在，会报错
f = open('1.txt')  # 文件存在，不报错，可以省略访问模式
f.close()