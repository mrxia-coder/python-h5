# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/3 22:20
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-文件读取之readline.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


f = open('test.txt', 'r')

content = f.readline()
print(f"第一行的内容是：{content}")

content = f.readline()
print(f"第二行的内容是：{content}")

f.close()
