# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 21:29
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-体验if语句.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
if 条件:
    条件成立执行的代码
    ......
"""

if True:
    print('条件成立执行的代码1')
    print('条件成立执行的代码2')

# 下方的代码没有缩进到if语句块，所以和if条件无关
print('我是无论条件是否成立都要执行的代码')

if False:
    print('条件成立执行的代码1')
    print('条件成立执行的代码2')

# 下方的代码没有缩进到if语句块，所以和if条件无关
print('我是无论条件是否成立都要执行的代码')
