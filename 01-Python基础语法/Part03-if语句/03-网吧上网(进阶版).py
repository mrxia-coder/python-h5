# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 21:36
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-网吧上网(简单版).py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
1. 获取用户输入
2. 保存用户年龄
3. if判断
"""

age = int(input("请输入你的年龄："))

if age >= 18:
    print(f"你的年龄是{age},已经成年，可以上网。")
