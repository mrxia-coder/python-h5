# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 0:14
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-if嵌套(坐公交).py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
1. 如果有钱，则可以上车
    2. 上车后，如果有空座，可以坐下
        上车后，如果没有空座，则站着等空座位
如果没钱，不能上车
"""

# 假设用 money = 1 表示有钱, money = 0表示没有钱; seat = 1 表示有空座，seat = 0 表示没有空座

money = 1
seat = 1
if money == 1:
    print("有钱，可以上车")
    if seat == 1:
        print("有空座，可以坐下")
    else:
        print("没有空座，只能站着")
else:
    print("没钱不能上车，只能走路了")
