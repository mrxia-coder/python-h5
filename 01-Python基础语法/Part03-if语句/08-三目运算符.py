# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 0:51
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 08-三目运算符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

a = 1
b = 2

c = a if a > b else b
print(c)


# 比较两个变量的大小，如果 变量1 大于 变量2 ，那么执行 变量1 - 变量2； 否则执行 变量2 - 变量 1
num1 = 10
num2 = 6

result = (num1 - num2) if num1 > num2 else (num2 -num1)
print(result)
