# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 23:47
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-多重判断(工作年龄).py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

age = int(input("请输入您的年龄：").strip())

if age < 18:
    print(f"您的年龄是{age},属于童工")
# elif (age >= 18) and (age <= 60):
elif 18 <= age <= 60:
    print(f"您的年龄是{age},合法工龄")
elif age > 60:
    print(f"您的年龄是{age},可以退休了")
