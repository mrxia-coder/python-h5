## 1、三目运算符

三目运算符也叫三元运算符。



## 2、三目运算符语法

```python
值1 if 条件 else 值2
```

快速体验：

```python
a = 1
b = 2

c = a if a > b else b
print(c)
```



```python
# 比较两个变量的大小，如果 变量1 大于 变量2 ，那么执行 变量1 - 变量2； 否则执行 变量2 - 变量 1
num1 = 10
num2 = 6

result = (num1 - num2) if num1 > num2 else (num2 -num1)
print(result)
```



