# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 21:36
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-网吧上网(简单版).py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

age = 20

if age >= 18:
    print("已经成年，可以上网。")
