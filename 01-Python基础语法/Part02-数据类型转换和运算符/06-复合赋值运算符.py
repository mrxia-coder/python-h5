# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 15:13
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-复合赋值运算符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

a = 100
a += 1
# 输出101  a = a + 1,最终a = 100 + 1
print(a)

b = 2
b *= 3
# 输出6  b = b * 3,最终b = 2 * 3
print(b)

c = 10
c += 1 + 2
# c = 10 + (1 + 2)
# 输出13, 先算运算符右侧1 + 2 = 3， c += 3 , 推导出c = 10 + 3
print(c)

d = 10
d *= 1 + 2
print(d)  # 由此可以看出上面的推导是正确的：先算运算符右侧 d = 10 * (1 + 2)