# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 23:30
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-数据类型转换函数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


num1 = 1
str1 = "10"

# 1. float() -- 转换成浮点型
print(float(num1))
print(type(float(num1)))
print((float(str1)))
print(type(float(str1)))

# 2. str() -- 转换成字符串类型
num2 = 10
print((str(num2)))
print(type(str(num2)))

# 3. tuple() -- 将一个序列转换成元组
list1 = [10, 20, 30]
print(tuple(list1))
print(type(tuple(list1)))

# 4. list() -- 将一个序列转换成列表
t1 = (100, 200, 300)
print(list(t1))
print(type(list(t1)))

# 5. eval() -- 将字符串中的数据转换成Python表达式原本类型
str1 = '10'
str2 = '[1, 2, 3]'
str3 = '(1000, 2000, 3000)'
str4 = "1 + 2 + 3"
print(type(eval(str1)))
print(type(eval(str2)))
print(type(eval(str3)))
print(eval(str4))
print(type(eval(str4)))