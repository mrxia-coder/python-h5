# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 23:15
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-输入.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
    1. 语法： input("提示信息")
    2. 特点
         2.1 当程序执行到`input`，等待用户输入，输入完成之后才继续向下执行。
         2.2 在Python中，`input`接收用户输入后，一般存储到变量，方便使用。
         2.3 在Python中，`input`会把接收到的任意用户输入的数据都当做字符串处理。
"""

password = input("请输入您的密码：")
print(f"您输入的密码是{password}")

print(type(password))