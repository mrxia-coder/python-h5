# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 15:30
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 07-比较运算符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

a = 7
b = 5
print(a == b)  # False
print(a != b)  # True
print(a < b)   # False
print(a > b)   # True
print(a <= b)  # False
print(a >= b)  # True