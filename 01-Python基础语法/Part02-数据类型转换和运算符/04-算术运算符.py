# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 14:54
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-算术运算符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 算术运算符 +
print(1 + 1)
print(1 + 1.1)  # 有float参与算术计算，得到的结果为float（取更高精度的数据类型）

# 算术运算符 -
print(1 - 1)
print(1 - 0.5)

# 算术运算符 *
print(2 * 3)
print(2 * 0.5)

# 算术运算符 /  除法运算之后的结果肯定是float类型，哪怕能够整除
print(4 / 2)

# 算术运算符 //（整除）
print(9 // 4.0)
print(10 // 3)

# 算术运算符 % (取余数)
print(9 % 4)

# 算术运算符 ** (指数运算)
print(2 ** 3)

# 算术运算符() 提高运算优先级
print((1 + 2) * 3 - (2 * 0.5))

# 拓展
print(2 * 3 ** 2)  # 指数运算优先级比乘法优先级高
print((2 * 3) ** 2)  # ()的7优先级高于指数运算
