# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 23:25
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-快速体验数据类型转换.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
    1. input()
    2. 检测数据类型
    3. 数据类型转换
    4. 检测是否转换成功
"""

number = input("请输入一个数字：")
print(number)
print(type(number))

number = int(number)
print(number)
print(type(number))