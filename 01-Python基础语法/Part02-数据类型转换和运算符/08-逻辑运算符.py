# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 15:37
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 08-逻辑运算符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


a = 0
b = 1
c = 2

# and 两边都为真才真
print((a < b) and (b < c))  # True
print((a > b) and (b < c))  # False

# or 两边有一个为真就为真
print((a > b) or (b < c))  # True

# 结果取反
print(not False)  # True
print(not (a > b))  # True

# 拓展： 数字之间做逻辑运算(and 和 or)
# and运算符，只要有一个值为0，则结果为0，否则结果为最后一个非0数字
print(a and b)  # 0
print(b and a)  # 0
print(a and c)  # 0
print(c and a)  # 0
print(b and c)  # 2
print(c and b)  # 1

# or运算符，只有所有值为0结果才为0，否则结果为第一个非0数字
print(a or b)  # 1
print(a or c)  # 2
print(b or c)  # 1
