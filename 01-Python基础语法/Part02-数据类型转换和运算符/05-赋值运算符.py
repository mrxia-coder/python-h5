# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/22 15:05
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-赋值运算符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

num = 1  # 单个变量赋值
a = b = 10  # 多个变量赋值
c, d, e, f = 1, 0.5, "hello", False
print(num, a, b, c, d, e, f)
