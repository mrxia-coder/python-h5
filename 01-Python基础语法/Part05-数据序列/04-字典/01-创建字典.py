# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 23:49
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-创建字典.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 创建有数据的字典
dict1 = {"name": "Tom", "age": 26, "gender": "男"}
print(dict1)

# 创建空字典
dict2 = {}
dict3 = dict()
print(dict2)
print(dict3)
