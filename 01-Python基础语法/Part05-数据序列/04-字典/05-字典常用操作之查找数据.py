# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 0:10
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-字典常用操作之查找数据.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

dict1 = {"name": "Tom", "age": 26, "gender": "男"}

# 1. key
print(dict1["name"])
try:
    print(dict1["names"])  # 报错key不存在
except KeyError:
    print("key不存在")

# 2. get()
print(dict1.get("name"))
print(dict1.get("names", "Tina"))
print(dict1.get("name"))

# 3. keys()
print(dict1.keys())
print(type(dict1.keys()))

# 4. vaules()
print(dict1.values())

# 5. items()
print(dict1.items())
