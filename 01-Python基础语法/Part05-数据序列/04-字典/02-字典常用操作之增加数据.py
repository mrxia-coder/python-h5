# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 23:55
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-字典常用操作之增加数据.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 新增字典数据
dict1 = {"name": "Tom", "age": 26, "gender": "男"}
dict1["id"] = 110
print(dict1)

# 注意，如果在新增数据的时候key已经存在在字典中，那么会直接修改该key对应的value值
dict1["age"] = 28
print(dict1)
