# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 23:55
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-字典常用操作之增加数据.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# del
dict1 = {"name": "Tom", "age": 26, "gender": "男"}
del(dict1)
try:
    print(dict1)
except NameError:
    print("对象不存在")

dict1 = {"name": "Tom", "age": 26, "gender": "男"}
del dict1["name"]
# del dict1["names"]  # 不存在会报错
print(dict1)


# clear
dict1 = {"name": "Tom", "age": 26, "gender": "男"}
dict1.clear()
print(dict1)
