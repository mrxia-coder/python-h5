# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 0:21
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-字典的遍历循环之遍历key.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

dict1 = {"name": "Tom", "age": 26, "gender": "男"}
for key in dict1.keys():
    print(key)
