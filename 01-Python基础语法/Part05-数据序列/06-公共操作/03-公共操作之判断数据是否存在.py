# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:01
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-公共操作之判断数据是否存在.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
list1 = [1, 2]
t1 = (3, 4)
dict1 = {"name": "Tom"}

print('e' in str1)
print('e' not in str1)

print(1 in list1)
print(1 not in list1)

print(3 in t1)
print(3 not in t1)

print("name" in dict1)
print("Tom" in dict1)  # False
print("name" in dict1.keys())
print("Tom" in dict1.values())
