# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:25
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 09-容器类型转换.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

list1 = [10, 20, 30, 20, 10]
s1 = {1, 2, 3, 4, 3, 6}
t1 = ('a', 'b', 'c', 'd')

# tuple()
print(tuple(list1))
print(tuple(s1))

# list()
print(list(s1))
print(list(t1))

# set()
print(set(list1))
print(set(t1))
