# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:05
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-公共方法之len.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
list1 = [1, 2]
t1 = (3, 4)
s1 = {11, 22, 33, 44}
dict1 = {"name": "Tom", "age": 26}

# 字符串
print(len(str1))

# list
print(len(list1))

# tuple
print(len(t1))

# set
print(len(s1))

# dict
print(len(dict1))
