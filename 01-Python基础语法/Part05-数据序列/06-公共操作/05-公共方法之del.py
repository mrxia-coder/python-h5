# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:05
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-公共方法之del.py
# @Version : v1.0.0
# @Desc    :
# @ide     : PyCharm

str1 = "hello"
list1 = [1, 2]
t1 = (3, 4)
s1 = {11, 22, 33, 44}
dict1 = {"name": "Tom", "age": 26}

# 字符串
del str1[1]
print(str1)
del str1
# print(str1)  # 报错，字符串不存在

# list
del list1[1]
print(list1)
del list1
# print(list1)  # 报错，list不存在

# tuple
# del t1[1]  # 报错， tuple不允许修改数据
del t1
print(t1)  # 报错，t1不存在

# set
del s1[1]
print(s1)
del s1
# print(s1)  # 报错，s1不存在

# dict
del dict1["name"]
print(dict1)
del dict1
# print(dict1)  # 报错，dict1不存在
