# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:20
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 08-公共操作方法之enumerate.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

list1 = ['a', 'b', 'c', 'd', 'e']

for l in enumerate(list1):
    print(l)

for i, char in enumerate(list1):
    print(f"索引为{i}的字符是{char}")

print()

for i, char in enumerate(list1, start=1):
    print(f"索引为{i}的字符是{char}")
