# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:05
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-公共方法之range.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 1 2 3 4 5 6 7 8 9
for i in range(1, 10, 1):
    print(i, end=" ")

print()

# 1 3 5 7 9
for i in range(1, 10, 2):
    print(i, end=" ")

print()

# 0 1 2 3 4 5 6 7 8 9
for i in range(10):
    print(i, end=" ")
