# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 21:54
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-公共操作之运算符加号.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
str2 = "world"

list1 = [1, 2]
list2 = [10, 20]

t1 = (3, 4)
t2 = (30, 40)

num1 = 10
num2 = 5

dict1 = {"name": "Tom"}
dict2 = {"age": 26}

# + 合并
print(str1 + str2)
print(list1 + list2)
print(t1 + t2)
print(num1 + num2)
# print(dict1 + dict2)  # 报错，字典不支持 + 运算符
