# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:05
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-公共方法之max和min.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
list1 = [1, 2]
t1 = (3, 4)
s1 = {11, 22, 33, 44}

# max()
print(max(str1))
print(max(list1))
print(max(t1))
print(max(s1))

# min()
print(min(str1))
print(min(list1))
print(min(t1))
print(min(s1))


