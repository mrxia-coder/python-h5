# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 21:54
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-公共操作之运算符乘号.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
list1 = [1, 2]
t1 = (3, 4)
num1 = 10
dict1 = {"name": "Tom"}

# * 复制
print(str1 * 5)
print(list1 * 3)
print(t1 * 4)
print(num1 * 12)
# print(dict1 * 5)  # 字典不支持 * 运算
