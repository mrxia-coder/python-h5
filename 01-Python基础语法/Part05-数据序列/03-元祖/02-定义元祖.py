# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:59
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-定义元祖.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 多个数据元组
t1 = (10, 20, 30)

# 单个数据元组必须在最后加上逗号
t2 = (10,)

t3 = (10,)
print(type(t3))  # tuple

t4 = (20)
print(type(t4))  # int

t5 = ('hello')
print(type(t5))  # str
