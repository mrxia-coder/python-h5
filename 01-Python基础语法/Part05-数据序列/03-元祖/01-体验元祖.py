# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:58
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-体验元祖.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

t1 = (10, 20, 30)
print(t1)

print(type(t1))
