# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 19:09
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-特例-修改元祖数据.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

t1 = (10, 20, 30, 40)
t2 = (10, 20, 30, ['a', 'b', 'c', 33], 40)

try:
    t1[1] = 50
    print(t1)
except TypeError:
    print("元祖不能被修改")

t2[3][0] = 'haha'
print(t2)
