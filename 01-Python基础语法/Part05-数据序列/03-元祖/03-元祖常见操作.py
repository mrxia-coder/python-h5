# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 19:02
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-元祖常见操作.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

t1 = (10, 20, 30, 40, 50)
print(t1[2])


# index()
print(t1.index(20))
try:
    print(t1.index(21)) # 不存在，报错
except ValueError:
    print("数据不存在")

# count()
print(t1.count(10))
print(t1.count(11))  # 不存在返回0


# len()
print(len(t1))