# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 0:42
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-结合常用操作之增加数据.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# remove()
s1 = {10, 20, 30, 40, 50}
s1.remove(10)
print(s1)
# s1.remove(10)  # 报错


# discard(0
s1 = {10, 20, 30, 40, 50}
s1.discard(10)
print(s1)
s1.discard(10)  # 虽然数据不存在了，但是也不报错


# pop()
s1 = {10, 20, 30, 40, 50}
pop_num = s1.pop()
print(pop_num)
print(s1)
