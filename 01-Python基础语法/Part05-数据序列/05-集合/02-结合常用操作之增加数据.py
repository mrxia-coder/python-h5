# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 0:42
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-结合常用操作之增加数据.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

s1 = {10, 20, 30}

# add()
s1.add(100)
print(s1)

s1.add(100)  # 集合有去重功能，如果追加的数据已经存在，则不作热河操作
print(s1)

# s1.add([1, 2, 3])  # 报错，add只能增加单个数据

# update()
s1.update([1, 2, 3, 10, 20, 30])
print(s1)

s1.update(10) # 报错，update只能增加一个数据序列
