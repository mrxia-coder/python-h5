# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 0:42
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-结合常用操作之增加数据.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

s1 = {10, 20, 30, 40, 50}

print(30 in s1)
print(10 not in s1)
