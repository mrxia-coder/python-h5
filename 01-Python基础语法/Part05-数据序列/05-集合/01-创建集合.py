# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 0:35
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-创建集合.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 创建有数据的集合
s1 = {10, 20, 30, 40, 50}
print(s1)  # {40, 10, 50, 20, 30}

s2 = {10, 20, 30, 40, 50, 10, 30, 60}
print(s2)  # {40, 10, 50, 20, 60, 30}

s3 = set('abcdefg')
print(s3)  # {'g', 'e', 'f', 'a', 'c', 'b', 'd'}

# 创建空集合
s4 = set()
print(s4)
print(type(s4))

s5 = {}
print(type(s5))  # <class 'dict'>