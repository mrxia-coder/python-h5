# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:07
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 08-列表数据删除.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

# del
# 1. 删除列表
del name_list
try:
    print(name_list)
except NameError:
    print("对象不存在")
# 2. 删除指定索引的数据
name_list = ['Tom', 'Lily', 'Rose']

del name_list[1]  # 删除索引为1的数据
print(name_list)

# pop()
name_list = ['Tom', 'Lily', 'Rose']

del_name = name_list.pop()
print(name_list)
print(del_name)

# remove()
name_list = ['Tom', 'Lily', 'Rose']

name_list.remove('Lily')
print(name_list)

# clear()
name_list = ['Tom', 'Lily', 'Rose']

name_list.clear()
print(name_list)