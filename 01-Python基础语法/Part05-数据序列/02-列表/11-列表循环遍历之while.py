# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:31
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 11-列表循环遍历之while.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

i = 0
while i < len(name_list):
    print(name_list[i])
    i += 1
