# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 17:56
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-列表数据增加之append.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

# 追加单个数据
name_list.extend('xiaoMing')
print(name_list)
# 输出 ['Tom', 'Lily', 'Rose', 'x', 'i', 'a', 'o', 'M', 'i', 'n', 'g']
# 如果列表追加单个数据(比如追加字符串)，则会把数据拆分之后一个一个追加


# 追加数据序列
name_list.extend(['Jack', 'Bob'])
print(name_list)
# 输出 ['Tom', 'Lily', 'Rose', 'x', 'i', 'a', 'o', 'M', 'i', 'n', 'g', 'Jack', 'Bob']
# 如果列表追加的是一个数据序列，则把数据序列中的每一个数据单独追加

