# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:28
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 10-复制列表.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

new_names = name_list.copy()
print(name_list)
print(id(name_list))

print(new_names)
print(id(new_names))
