# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 17:56
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-列表数据增加之append.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

name_list.insert(1, 'Jack')
print(name_list)
# 输出 ['Tom', 'Jack', 'Lily', 'Rose']

