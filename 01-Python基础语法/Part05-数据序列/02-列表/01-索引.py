# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 17:31
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-索引.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']
print(name_list)

# 通过索引找到Lily
print(name_list[1])

print(name_list[0])
print(name_list[2])
