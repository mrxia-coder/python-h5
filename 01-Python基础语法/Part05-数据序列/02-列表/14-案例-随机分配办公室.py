# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:37
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 14-案例-随机分配办公室.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

import random

# 1-8表示8位老师，
teachers = [1, 2, 3, 4, 5, 6, 7, 8]
rooms = [[], [], []]

for t in teachers:
    rooms[random.randint(0, 2)].append(t)

print(rooms)

# 验证
i = 1
for room in rooms:
    print(f"办公室{i}的人数是{len(room)},分别是以下老师：")
    for name in room:
        print(name)
    i += 1
