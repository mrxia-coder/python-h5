# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:32
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 12-列表循环遍历之for.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

for name in name_list:
    print(name)
