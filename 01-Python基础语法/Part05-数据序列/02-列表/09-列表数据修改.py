# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:17
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 09-列表数据修改.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

# 通过索引修改指定下标数据
name_list[0] = 'Tina'
print(name_list)



# reverse()
list1 = [1, 3, 6, 7, 2, 9]

list1.reverse()
print(list1)

# sort()
list1 = [1, 3, 6, 7, 2, 9]

list1.sort()
print(list1)

list1.sort(reverse=True)
print(list1)
