# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 17:46
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-判断是否存在.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

# 1. in
print('Tom' in name_list)  # True
print('Toms' in name_list)  # False

# not in
print('Tom' not in name_list)  # False
print('Toms' not in name_list)  # True
