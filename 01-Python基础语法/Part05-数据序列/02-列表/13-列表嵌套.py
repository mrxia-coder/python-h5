# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 18:34
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 13-列表嵌套.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = [['小明', '小红', '小绿'], ['Tom', 'Lily', 'Rose'], ['张三', '李四', '王五']]

# 1. 拿到李四所在的子列表
print(name_list[2])

# 2. 拿到李四
print(name_list[2][1])
