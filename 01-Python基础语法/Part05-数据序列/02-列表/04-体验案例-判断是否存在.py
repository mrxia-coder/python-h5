# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 17:49
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-体验案例-判断是否存在.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

# 用户输入用户名，判断用户名是否存在
username = input("请输入您的用户名：").strip()

if username in name_list:
    print(f"您输入的用户名{username}已经存在，请重新输入！")
else:
    print(f"您输入的用户名{username}可用。")
