# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/26 17:36
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-查找.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name_list = ['Tom', 'Lily', 'Rose']

# index()
print(name_list.index('Tom'))
try:
    print(name_list.index('Toms'))  # 数据不存在报错
except ValueError:
    print("数据不存在")

# count()
print(name_list.count('Tom'))
print(name_list.count('Toms'))  # 数据不存在，返回0

# len()
print(len(name_list))
