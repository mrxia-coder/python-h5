# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:50
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-多个for循环实现列表推导式.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 需求：创建这样的列表： [(1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]

# 普通实现
list1 = []
for i in range(1, 3):
    for j in range(0, 3):
        list1.append((i, j))
print(list1)

# 列表推导式实现
list2 = [(i, j) for i in range(1, 3) for j in range(0, 3)]
print(list2)
