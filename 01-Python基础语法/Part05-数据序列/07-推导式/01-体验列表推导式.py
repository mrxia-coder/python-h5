# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:33
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-体验列表推导式.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 需求：创建一个1-10的列表
list1 = []
# while
i = 1
while i < 11:
    list1.append(i)
    i += 1
print(list1)

# for
list2 = []
for i in range(1, 11):
    list2.append(i)
print(list2)

# 列表推导式
list3 = [i for i in range(1, 11)]
print(list3)
