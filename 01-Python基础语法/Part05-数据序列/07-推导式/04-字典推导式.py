# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:45
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-字典推导式.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 创建一个字典：字典key是1-5数字，value是这个数字的2次方。
dict1 = {i: i ** 2 for i in range(1, 6)}
print(dict1)

# 将两个列表合并为一个字典
list1 = ['name', 'age', 'gender']
list2 = ['Tom', 20, 'man']

dict2 = {list1[i]: list2[i] for i in range(len(list1))}
print(dict2)

# 注意： 如果两个列表的长度相等，那么无所谓取任意一个列表的长度即可，
#       如果两个列表的长度不一样，那么悠闲取较短的列表用来决定字典长度，如果去较长的列表，会报错


# 提取字典中目标数据
counts = {'MBP': 268, 'HP': 125, 'DELL': 201, 'Lenovo': 199, 'acer': 99}
# 需求：提取上述电脑数量大于等于200的字典数据
dict3 = {k: v for k, v in counts.items() if v >= 200}
print(dict3)
