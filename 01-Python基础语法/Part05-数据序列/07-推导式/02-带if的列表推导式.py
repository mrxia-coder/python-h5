# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/27 22:40
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-带if的列表推导式.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 需求：创建0-10的偶数列表 [2, 4, 6, 8, 10]

# range()步长实现
list1 = [i for i in range(2, 11, 2)]
print(list1)

# if列表推导式
list2 = [i for i in range(1, 11) if i % 2 == 0]
print(list2)
