# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 8:49
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-字符串输入.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 接收用户输入密码一般都是要隐藏真实密码而是显示*之类的替代，所以会用到getpass模块
# import getpass

# 这里用作字符串输入演示，就直接明文显示了

# password = getpass.getpass("请输入您的密码：").strip()
password = input("请输入您的密码：").strip()
print(password)