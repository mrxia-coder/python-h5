# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 23:19
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 13-字符串常用操作方法之判断.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# isalpha()
myStr1 = 'hello'
myStr2 = 'hello12345'

print(myStr1.isalpha())
print(myStr2.isalpha())

#  isdigit()
myStr3 = "hello12345"
myStr4 = "12345"

print(myStr3.isdigit())
print(myStr4.isdigit())

# isalnum()
myStr5 = "hello12345"
myStr6 = "12345"
myStr7 = "一"  # 中文'一'
myStr8 = "*&^$%^&"

print(myStr5.isalnum())  # True
print(myStr6.isalnum())  # True
print(myStr7.isalnum())  # True
print(myStr8.isalnum())  # False

# isspace()
myStr9 = "h e l l o"
myStr10 = '       '
myStr11 = ' \n  \t    '

print(myStr9.isspace())
print(myStr10.isspace())
print(myStr11.isspace())

