# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 23:16
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 12-字符串常用操作方法之判断开头或结尾.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

myStr = "hello world and python and java and php and goLand and javascript"

# startswith()  检查字符串是否是以指定子串开头，是则返回 True，否则返回 False。如果设置开始和结束位置下标，则在指定范围内检查。
print(myStr.startswith('hello'))
print(myStr.startswith('wa'))


# endswith()  检查字符串是否是以指定子串结尾，是则返回 True，否则返回 False。如果设置开始和结束位置下标，则在指定范围内检查。
print(myStr.endswith('script'))
print(myStr.endswith('the'))
