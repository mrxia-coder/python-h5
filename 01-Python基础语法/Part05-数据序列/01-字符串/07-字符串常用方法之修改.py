# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 22:33
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 07-字符串常用方法之修改.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

myStr = "hello world and python and java and php and goLand and javascript"

# 1. replace()  替
newStr = myStr.replace('and', 'he')
print(myStr.replace('and', 'he', 1))  # 指定替换1次
print(myStr.replace('and', 'he', 100))  # 指定替换次数大于总匹配到的次数，则全部替换
print(newStr)
print(myStr)

# replace()函数替换之后并不会修改原字符串，而是生成一个新的字符串作为返回值
# --- 说明字符串属于不可变数据类型


# 2. split()  分割字符串，返回一个列表, 丢失分割字符
list1 = myStr.split('and')
print(list1)
list2 = myStr.split('and', 2)
print(list2)

# 3. join()  合并列表里的字符串为一个大的字符串
myList = ['aa', 'bb', 'cc', 'dd']
bigStr = '...'.join(myList)
print(bigStr)
