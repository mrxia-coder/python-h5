# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 8:47
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-字符串输出.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 直接输出
print("hello world")

# 带变量的输出
name = "Tom"
print("我的名字是%s" % name)
print(f"我的名字是{name}")
