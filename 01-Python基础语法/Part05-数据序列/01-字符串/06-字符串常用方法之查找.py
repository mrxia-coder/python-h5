# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 9:20
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-字符串常用方法之查找.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

myStr = "hello world and python and java and php and goLand and javascript"

# 1.find()
print(myStr.find('and'))
print(myStr.find(' '))

print(myStr.find('and', 15, 40))

print(myStr.find('ands'))  # 查找不到，返回 -1

# 2. index()
print(myStr.index('and'))
print(myStr.index('and', 15, 40))

try:
    print(myStr.index('ands'))
except ValueError:
    print(f"要查找的子串在{myStr}中不存在")

# 3. count()
print(myStr.count('and'))
print(myStr.count('and', 15, 40))
print(myStr.count('ands'))  # 0

# 4.rfind()
print(myStr.rfind('and'))  # 与find()的查找反向相反，用法一样

# 5. rindex()
print(myStr.rindex('and')) # 与index()的查找反向相反，用法一样
