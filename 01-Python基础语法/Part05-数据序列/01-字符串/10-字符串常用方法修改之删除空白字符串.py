# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 23:12
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 10-字符串常用方法修改之删除空白字符串.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


str1 = "        hello world   "

# 1. lstrip() 去除字符串左侧的空白字符
print(str1.lstrip())

# 2. rstrip() 去除字符串右侧边的空白字符
print(str1.rstrip())

# 3. strip()  去除字符串两侧的空白字符
print(str1.strip())