# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 23:12
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 11-字符串常用方法修改之字符串对齐.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str2 = "hello"
# 1. ljust()  返回一个原字符串左对齐,并使用指定字符(默认空格)填充至对应长度 的新字符串
print(str2.ljust(10, '*'))

# 2. rjust()  返回一个原字符串右对齐,并使用指定字符(默认空格)填充至对应长度 的新字符串，语法和ljust()相同
print(str2.rjust(10, '*'))

# 3. center()  返回一个原字符串居中对齐,并使用指定字符(默认空格)填充至对应长度 的新字符串，语法和ljust()相同
print(str2.center(10, '*'))