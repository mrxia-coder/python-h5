# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 8:41
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-认识字符串.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 一对引号字符串
a = "hello world"
print(a)
print(type(a))

b = 'hello python'
print(b)
print(type(b))

# 三引号字符串
name1 = """Tom"""
name2 = '''StaryJie'''
msg1 = """I am Tom,
Nice to meet you!"""
msg2 = '''I am Staryjie,
Nice to meet you too!'''
print(name1)
print(name2)
print(msg1)
print(msg2)

# I'm Tom
c = "I'm Tom"
d = 'I\'m Tom'
print(c)
print(d)