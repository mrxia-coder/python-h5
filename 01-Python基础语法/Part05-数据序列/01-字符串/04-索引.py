# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 8:54
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-索引.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
print(str1)

# 如何获取字符串中某个特定的数据?
print(str1[0])
print(str1[1])
print(str1[2])
print(str1[3])
print(str1[4])

#   获得'e'字符
print(str1[1])