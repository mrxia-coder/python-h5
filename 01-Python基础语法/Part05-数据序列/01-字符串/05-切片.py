# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 8:59
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-切片.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "abcdefg"

print(str1[2:5:1])  # cde
print(str1[2:5])  # cde

print(str1[:5])  # 截取前5个元素  abcde

print(str1[1:])  # bcdefg
print(str1[:])  # abcdefg
print(str1[::2])  # aceg
print(str1[:-1])  # abcdef, 负1表示倒数第一个数据
print(str1[-4:-1])  # def
print(str1[::-1])  # gfedcba
print(str1[::2])  # 步长设置为2 aceg

str2 = "0123456789"
print(str2[1::2])  # 取奇数
print(str2[2::2])  # 取偶数
print(str2[::-1])  # 倒序 9876543210
print(str2[-5:-1])  # 5678
print(str2[-5:-1:-1])  # 不会输出，[-5:-1]是从左往右截取，[-5:-1:-1]变成从右往左选取了，方向相反
# 注意： 如果截取数据的方向(索引开始到结束的方向)和步长的方向(正数是从左往右，负数是从右往左)冲突，则无法截取到任何数据

print(str2[-2:-6:-1])  # 8765
