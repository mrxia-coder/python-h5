# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 22:48
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 09-字符串常用方法修改(非重点).py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

myStr = "hello world and python and java and php and goLand and javascript"

# 1. capitalize()  字符串首字母大写
print(myStr.capitalize())

# 2. title()  字符串中每个单词首字母大写(单词通过空格区分)
print(myStr.title())

# 3. lower() 字符串大写字母转小写字母
print(myStr.lower())

# 4. upper() 字符串小写字母转大写字母
print(myStr.upper())
