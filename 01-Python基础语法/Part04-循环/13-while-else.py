# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 8:00
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 13-while-else.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

i = 1
while i < 6:
    print(i)
    i += 1
else:
    print("end")
