# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 22:01
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-while循环快速体验.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
while 条件:
    条件成立要执行的代码
"""

count = 0
while count < 5:
    print(count)
    count += 1
    # TODO(staryjie@163.com): TODO注释，此处描写注释文字
