# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 7:57
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 12-for循环中的continue.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
for i in str1:
    if i == "l":
        continue
    print(i)
