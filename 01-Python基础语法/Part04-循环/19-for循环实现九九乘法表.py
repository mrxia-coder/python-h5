# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 8:17
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 19-for循环实现九九乘法表.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

for i in range(1, 10):
    for j in range(1, i+1):  # 控制每一行打印的数量
        print(f"{i} * {j} = {i * j}", end="\t")  # 一行打印结束
    print("")  # 换行
