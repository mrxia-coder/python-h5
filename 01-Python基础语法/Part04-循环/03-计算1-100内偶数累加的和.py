# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 22:30
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-计算1-100内偶数累加的和.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 方法1 条件判断和2取余数则累加
i = 0
result1 = 0
while i < 101:
    if i % 2 == 0:
        result1 += i
    i += 1

print(result1)

# 方法2 计数器控制
j = 2
result2 = 0
while j < 101:
    result2 += j
    j += 2  # 计数器必须保证是变化的，不然会陷入死循环

print(result2)
