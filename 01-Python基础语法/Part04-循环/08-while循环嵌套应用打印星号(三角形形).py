# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 23:28
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 07-while循环嵌套应用打印星号(正方形).py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

i = 0
while i < 5:
    j = 0  # 一行开始
    while j < i + 1:
        print(" * ", end="")
        j += 1
    print("")  # 一行结束，换行
    i += 1