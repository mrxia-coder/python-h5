# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/24 7:51
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 10-for循环快速体验.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

str1 = "hello"
for i in str1:
    print(i)
