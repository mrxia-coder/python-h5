# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 22:52
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-while循环中的break.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 举例：一共吃5个苹果，吃完第一个，吃第二个…，这里"吃苹果"的动作是不是重复执行？
# # 情况一：如果吃的过程中，吃完第三个吃饱了，则不需要再吃第4个和第五个苹果，即是吃苹果的动作停止，这里就是break控制循环流程，即==终止此循环==。

i = 1
while i < 6:
    if i > 3:
        print("吃饱了，不吃了")
        break
    print(f"吃第{i}个苹果")
    i += 1
