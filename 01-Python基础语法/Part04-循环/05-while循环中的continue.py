# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 22:55
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-while循环中的continue.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 举例：一共吃5个苹果，吃完第一个，吃第二个…，这里"吃苹果"的动作是不是重复执行？
# 情况二：如果吃的过程中，吃到第三个吃出一个大虫子...,是不是这个苹果就不吃了，开始吃第四个苹果，这里就是continue控制循环流程，即==退出当前一次循环继而执行下一次循环代码==。

i = 1
while i < 6:
    if i == 3:
        print(f"第{i}个苹果有虫子，这个不吃了")
        i += 1
        continue
    print(f"吃第{i}个苹果")
    i += 1
