# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 23:10
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-while循环嵌套.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

i = 0
while i < 3:
    j = 0
    while j < 3:
        print(i, j)
        j += 1
    i += 1
