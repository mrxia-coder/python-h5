# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/23 22:21
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-计算1-100累加的和.py
# @Version : v1.0.0
# @Desc    : 计算1-100累加的和
# @ide     : PyCharm

i = 1
result = 0

while i < 101:
    result += i
    i += 1

print(result)