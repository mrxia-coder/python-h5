## 1、lambda的应用场景

如果一个函数有一个返回值，并且只有一句代码，可以使用 lambda简化。



## 2、lambda语法

```python
lambda 参数列表 ： 表达式
```

> 注意：
>
> * lambda表达式的参数可有可无，函数的参数在lambda表达式中完全适用。
> * lambda表达式能接收任何数量的参数但只能返回一个表达式的值。

### 2.1 快速体验

```python
# 1. 函数
def fn1():
    return 200


result = fn1()
print(result)

# 2. lambda表达式  匿名函数
fn2 = lambda: 200

print(fn2)  # lambda表达式的内存地址
print(fn2())  # 直接结果
```



## 3、示例：计算a + b

### 3.1 函数实现

```python
def add(a, b):
    return a + b


result = add(1, 2)
print(result)
```

### 3.2 lambda实现

```python
result2 = lambda a, b: a + b
print(result2(1, 2))
```



## 4、lambda的参数形式

### 4.1 无参数

```python
fn1 = lambda: 100
print(fn1())
```

### 4.2 一个参数

```python
fn2 = lambda a: a
print(fn2('hello world'))
```

### 4.3 默认参数

```python
fn3 = lambda a, b, c=100: a + b + c
print(fn3(100, 200))
```

### 4.4 可变参数：*args

```python
fn4 = lambda *args: args
print(fn4(1, 2, 3, 4))
```

> 注意：这里的可变参数传入到lambda之后，返回值为元组。

### 4.5 可变参数：**kwargs

```python
fn5 = lambda **kwargs: kwargs
print(fn5(name="Tom", age=26))
```



## 5、lambda的应用

### 5.1 带判断的lambda

> 比较两个数的大小，返回较大的那个数

```python
bigger = lambda a, b: a if a > b else b
print(bigger(11, 23))  # 23
```

### 5.2 列表数据按字典key的值排序

```python
students = [
    {"name": "Zime", "age": 26},
    {"name": "Rose", "age": 24},
    {"name": "Anna", "age": 18},
    {"name": "Tom", "age": 20},
]

# 1.按name值升序排列
students.sort(key=lambda x: x["name"])
print(students)

# 2.按name值降序排列
students.sort(key=lambda x: x["name"], reverse=True)
print(students)

# 3.按age值降序排列
students.sort(key=lambda x: x["age"], reverse=True)
print(students)

# 4.age值降序排列
students.sort(key=lambda x: x["age"])
print(students)
```