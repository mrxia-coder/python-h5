# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 22:42
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-lambda参数形式.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1.无参数
fn1 = lambda: 100
print(fn1())

# 2.一个参数
fn2 = lambda a: a
print(fn2('hello world'))

# 3.默认参数
fn3 = lambda a, b, c=100: a + b + c
print(fn3(100, 200))

# 4.可变参数 *args
fn4 = lambda *args: args
print(fn4(1, 2, 3, 4))

# 5. 可变参数 **kwargs
fn5 = lambda **kwargs: kwargs
print(fn5(name="Tom", age=26))
