# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 22:33
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-l快速体验lambda表达式.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1. 函数
def fn1():
    return 200


result = fn1()
print(result)

# 2. lambda表达式  匿名函数
fn2 = lambda: 200

print(fn2)  # lambda表达式的内存地址
print(fn2())  # 直接结果
