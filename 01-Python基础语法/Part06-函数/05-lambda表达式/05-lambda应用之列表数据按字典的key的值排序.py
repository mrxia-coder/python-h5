# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 22:49
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-lambda应用之列表数据按字典的key的值排序.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


students = [
    {"name": "Zime", "age": 26},
    {"name": "Rose", "age": 24},
    {"name": "Anna", "age": 18},
    {"name": "Tom", "age": 20},
]

# 1.按name值升序排列
students.sort(key=lambda x: x["name"])
print(students)

# 2.按name值降序排列
students.sort(key=lambda x: x["name"], reverse=True)
print(students)

# 3.按age值降序排列
students.sort(key=lambda x: x["age"], reverse=True)
print(students)

# 4.age值降序排列
students.sort(key=lambda x: x["age"])
print(students)
