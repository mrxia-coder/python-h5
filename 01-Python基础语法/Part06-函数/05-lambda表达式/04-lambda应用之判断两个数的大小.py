# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 22:48
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-lambda应用之判断两个数的大小.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


bigger = lambda a, b: a if a > b else b
print(bigger(11, 23))

