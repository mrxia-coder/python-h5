# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 22:37
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-示例-实现a+b.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1.函数实现
def add(a, b):
    return a + b


result = add(1, 2)
print(result)

# 2.lambda实现
result2 = lambda a, b: a + b
print(result2(1, 2))
