# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 22:11
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-递归实现3以内数字累加的和.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1.回顾：函数返回值
def return_num():
    return 100


result = return_num()
print(result)


# 2.递归实现3以内数字累加
def sum_nums(num):
    # 1.如果是1，则直接返回1   --- 递归函数的出口
    if num == 1:
        return 1
    # 2.如果num大于1，重复执行累加并返回结果
    return num + sum_nums(num - 1)


sum_result = sum_nums(3)
print(sum_result)

"""
sum_result = sum_nums(3)
sum_result = 3 + sum_nums(3 - 1)  ==> sum_result = 3 + sum_nums(2)
sum_result = 3 + 2 + sum_nums(2 - 1) ==> sum_result = 3 + 2 + sum_nums(1)
sum_result = 3 + 2 + 1 = 6
sum_result = 6
"""