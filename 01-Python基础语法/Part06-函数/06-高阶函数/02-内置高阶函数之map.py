# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 23:12
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-内置高阶函数之map.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


list1 = [1, 2, 3, 4, 5]


def func1(x):
    return x ** 2


result1 = map(func1, list1)
print(result1)  # <map object at 0x0000015832AF6C88>
print(list(result1))  # [1, 4, 9, 16, 25]
