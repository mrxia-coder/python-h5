# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 23:06
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-体验高阶函数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1.求绝对值abs()
print(abs(-12))
print(abs(12))

# 2.四舍五入 round()
print(round(12.6))
print(round(12.5))
print(round(12.1))


# 3.需求：求两个数字绝对值的和
# 3.1 通过写函数实现
def add_num(a, b):
    return abs(a) + abs(b)


result = add_num(-1, -2)
print(result)


# 3.2 用高阶函数实现
def sum_num(a, b, f):
    return f(a) + f(b)


result2 = sum_num(-1, -2, abs)
print(result2)  # 1 + 2 = 3

result3 = sum_num(1.1, 2.9, round)
print(result3)  # 1 + 3 = 4
