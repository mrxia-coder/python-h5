# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 23:15
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-内置高阶函数之reduce.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


from functools import reduce

list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def func1(a, b):
    return a + b


result = reduce(func1, list1)
print(result)

# 检查
sum_result = 0
for i in list1:
    sum_result += i

print(sum_result)  # 55
