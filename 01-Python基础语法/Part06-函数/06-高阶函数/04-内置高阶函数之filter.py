# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/1 23:20
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-内置高阶函数之filter.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def func(x):
    return x % 2 == 0


result = filter(func, list1)
print(result)  # <filter object at 0x000002AD96A68FC8>
print(list(result))  # [2, 4, 6, 8, 10]
