# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 22:30
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-学员管理系统.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# info = []
# 先默认有一个学员信息，方便后期调试
info = [{'id': '0001', 'name': 'Jack', 'tel': '189'}]


def print_info():
    print('-' * 20)
    print('欢迎登录学员管理系统')
    print('1: 添加学员')
    print('2: 删除学员')
    print('3: 修改学员信息')
    print('4: 查询学员信息')
    print('5: 显示所有学员信息')
    print('6: 退出系统')
    print('-' * 20)


# 添加学员信息函数
def add_info():
    """添加学员信息函数"""
    new_id = input("请输入学号：").strip()
    new_name = input("请输入姓名：").strip()
    new_tel = input("请输入手机号：").strip()

    # 声明info全局变量
    global info

    # 判断学员是否已经存在
    for i in info:
        if new_id == i["id"]:
            print("该用户已存在！")
            print(info)
            return

    # 如果学员信息不存在，则添加该学员信息
    info_dict = {}
    info_dict["id"] = new_id
    info_dict["name"] = new_name
    info_dict["tel"] = new_tel

    info.append(info_dict)
    print(info)


# 删除学员信息函数
def del_info():
    """删除学员信息函数"""
    del_id = input("请输入要删除学员的学号：").strip()

    # 声明info全局变量
    global info
    for i in info:
        if del_id == i["id"]:
            info.remove(i)
            break
    else:
        print("该学员不存在！")

    print(info)


# 修改学员信息
def modify_info():
    """修改学员信息函数"""
    modify_id = input("请输入要修改的学员的学号：").strip()

    # 声明info全局变量
    global info
    for i in info:
        if modify_id == i["id"]:
            i["name"] = input("请输入姓名：").strip()
            i["tel"] = input("请输入手机号：").strip()
            break
    else:
        print("该学员不存在！")

    print(info)


# 查询学员信息
def search_info():
    """查询学员信息函数"""
    search_id = input("请输入要查询信息的学员学号：").strip()

    global info

    for i in info:
        if search_id == i["id"]:
            print("查询到的学员信息如下：")
            print(f"学员学号是【{i['id']}】,姓名是【{i['name']}】,手机号是【{i['tel']}】.")
            break
    else:
        print("该学员不存在！")


# 显示所有学员信息
def pring_all():
    """打印所有学员信息"""
    global info
    print('学号\t姓名\t手机号')
    for i in info:
        print(f'{i["id"]}\t{i["name"]}\t{i["tel"]}')


while True:
    # 1. 显示功能界面
    print_info()

    # 2. 用户选择功能
    user_num = input('请选择您需要的功能序号：')
    if user_num.isdigit():
        user_num = int(user_num)
    else:
        print("请输入数字！")
        continue

    # 3. 根据用户选择，执行不同的功能
    if user_num == 1:
        # print('添加学员')
        add_info()
    elif user_num == 2:
        # print('删除学员')
        del_info()
    elif user_num == 3:
        # print('修改学员信息')
        modify_info()
    elif user_num == 4:
        # print('查询学员信息')
        search_info()
    elif user_num == 5:
        # print('显示所有学员信息')
        pring_all()
    elif user_num == 6:
        exit_flag = input('确定要退出吗？(y/n)')
        if exit_flag.lower() == 'y':
            break
    else:
        print('输入错误，请重新输入!!!')
