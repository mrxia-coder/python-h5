# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 17:55
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 07-函数的嵌套调用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def testB():
    print("------ testB Start ------")
    print("这里是testB函数的代码....")
    print("------ testB Ended ------")


def testA():
    print("------ testA Start ------")
    testB()
    print("------ testA Ended ------")


testA()
