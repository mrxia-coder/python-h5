# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 17:42
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-函数说明文档.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# help(len)  # 查看函数的说明文档

def sum_num(num1, num2):
    """
    sum two number.
    :param num1: number1
    :param num2: number2
    :return: result of number1 + number2
    """
    result = num1 + num2
    return result


help(sum_num)
