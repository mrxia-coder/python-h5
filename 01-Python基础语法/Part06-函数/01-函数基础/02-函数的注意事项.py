# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 17:03
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-函数的注意事项.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 定义函数
def info_print():
    print('hello world')


# 调用函数
info_print()


"""
1. 函数必须先定义后调用，否则会报错
2. 如果不调用函数，函数内的代码不会被执行
3. 函数执行流程：
    3.1 定义函数之后，只会把代码加载到程序内存中，不会执行
    3.2 当函数被调用的时候，解释器会找到函数代码所在的内存地址，执行函数内的代码
    3.3 函数调用执行完之后，再次回到调用函数的地方，继续往下执行
"""