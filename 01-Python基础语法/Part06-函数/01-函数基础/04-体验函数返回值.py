# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 17:27
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-体验函数返回值.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def buy(good_name):
    return good_name


# 使用变量保存函数返回值
goods = buy("香烟")
print(goods)

"""
return的注意事项：
    1. 返回一个返回值给调用函数处通过变量接收后使用
    2. 函数体中， 遇到return就退出函数，之后的代码不会被执行
"""