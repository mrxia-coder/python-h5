# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 17:36
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-函数返回值的应用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 需求：制作一个计算器，计算任意两数字之和，并保存结果。


# def calculator(*kwarg):
#     result = 0
#     for num in kwarg:
#         result += num
#     return result


def calculator(num1, num2):
    return num1 + num2


result = calculator(1, 2)
print(result)
