# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 17:20
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-体验函数参数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def add_num():
    print(1 + 2)


def add_sum(num1, num2):
    print(num1 + num2)


add_num()
add_sum(121, 243)
add_sum(11, 24)
