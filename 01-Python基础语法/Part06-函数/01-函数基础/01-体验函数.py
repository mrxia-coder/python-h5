# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 16:32
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-体验函数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm
import os
from getpass import getpass

def menu():
    # os.system("clear")
    menu_info = """------ 请选择功能 ------
    1. 查询余额
    2. 取款
    3. 存款
    4. 退出
------------------------
请输入对应的序号选择功能：
    """
    print(menu_info)


def check_input(num):
    if num.isdigit():
        return True
    else:
        return False


total_money = 300000
password = getpass("请输入您的银行卡密码：").strip()
first_login = 0
while len(password) != 0:
    # TODO <staryjie@163.com> 对密码的判断逻辑有待加强，这里只是为了体现逻辑
    if first_login == 0:
        print("登录成功！")
        first_login += 1
    else:
        pass
    menu()
    select_num = input().strip()
    if select_num.lower() == 'clear':
        os.system('clear')
        continue
    if check_input(select_num):
        select_num = int(select_num)
        if select_num == 1:
            print("您的账号余额为：%.2f￥" % total_money)
            continue
        elif select_num == 2:
            get_money = input("请输入要取款的金额：").strip()
            if check_input(get_money):
                get_money = int(get_money)
                if get_money <= total_money:
                    print(f"成功取款{get_money},账户余额为:{total_money - get_money}￥.")
                    total_money -= get_money
                    continue
                else:
                    print("账户余额不足！")
                    continue
            else:
                print("取款金额必须是正整数！")
                continue
        elif select_num == 3:
            save_money = input("请输入存款金额：").strip()
            if check_input(save_money):
                save_money = int(save_money)
                total_money += save_money
                print(f"存款成功！账户余额为：{total_money}￥.")
                continue
            else:
                print("存款金额必须是正整数！")
                continue
        elif select_num == 4:
            print("退出成功！请取卡。\n感谢您的使用，欢迎下次光临！")
            first_login = 0
            break
        else:
            print("没有您选择的功能，请重新选择！")
            continue
    else:
        print("请输入数字序号！")
        continue
else:
    print("您输入的密码有误！")

