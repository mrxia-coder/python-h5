# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 17:59
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 08-函数嵌套调用之打印横线图形.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def print_line():
    print("-" * 20)


print_line()
