# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 18:05
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 10-函数嵌套调用之函数计算.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def sum_num(num1, num2, num3):
    return num1 + num2 + num3


# result = sum_num(1, 2, 3)
# print(result)


def get_average(num1, num2, num3):
    return sum_num(num1, num2, num3) / 3


average = get_average(2, 4, 6)
print(average)
