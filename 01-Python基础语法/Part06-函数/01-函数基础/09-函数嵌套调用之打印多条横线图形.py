# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/30 18:00
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 09-函数嵌套调用之打印多条横线图形.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def print_line():
    print("-" * 20)


def print_lines(num):
    for i in range(num):
        print_line()


print_lines(5)
