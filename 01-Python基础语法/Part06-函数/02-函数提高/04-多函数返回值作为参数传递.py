# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:24
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-多函数返回值作为参数传递.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def test1():
    return 50


def test2(num):
    print(num)


result = test1()
test2(result)
