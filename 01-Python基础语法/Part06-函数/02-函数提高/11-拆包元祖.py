# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:51
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 11-拆包元祖.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def return_num():
    return 100, 200  # 返回元祖(100, 200)


num1, num2 = return_num()
print(num1)
print(num2)
