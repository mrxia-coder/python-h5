# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:36
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 10-函数参数之不定长参数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# def user_info(*args):
#     print(args)
#
#
# user_info("Tom")  # ('Tom',)
# user_info("Tom", 26)  # ('Tom', 26)


def user_info(**kwargs):
    print(kwargs)


user_info(name="Tom", age=26, gender="男")
