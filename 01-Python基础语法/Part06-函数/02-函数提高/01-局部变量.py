# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:00
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-局部变量.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 局部变量
def testA():
    a = 100
    print(a)


testA()
# print(a)  # 报错：NameError: name 'a' is not defined