# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 22:06
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 15-了解引用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1. int类型
a = 1
b = a

print(id(a))  # 140706874683664
print(id(b))  # 140706874683664

a = 2
print(b)  # 1
print(id(b))  # 140706874683664

print(id(a))  # 140706878419248 这里重新赋值了a变量，所以引用地址发生了变化

# int类型两个变量引用的内存地址一样， 说明int类型是不可变类型


# 2. 列表
aa = [10, 20]
bb = aa

print(aa)
print(bb)
print(id(aa))  # 1909222232648
print(id(bb))  # 1909222232648

aa.append(30)

print(aa)
print(bb)

# 列表追加一个元素之后，引用地址会发生变化，所以列表为可变类型
print(id(aa))  # 1909222232648
print(id(bb))  # 1909222232648
