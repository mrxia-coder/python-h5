# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:20
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 03-多函数共享全局变量.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 定义全局变量
glo_num = 0


def test1():
    global glo_num
    glo_num = 100


def test2():
    print(glo_num)


# print(glo_num)
test2()
test1()
test2()
