# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:51
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 12-拆包字典.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


dict1 = {"name": "Tom", "age": 26}

a, b = dict1
print(a)  # name
print(b)  # age

print(dict1[a])  # Tom
print(dict1[b])  # 26
