# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:29
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-函数返回多个返回值.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def return_num():
    # return 1, 2  # 这样返回的是元祖
    # return [1, 2, 3]  # 返回列表
    return {"name": "Tom", "age": 26}  # 返回字典


result = return_num()
print(result)
