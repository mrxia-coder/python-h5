# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:26
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-函数多个return如何执行.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def return_num():
    return 1  # 函数遇到return直接结束，后面的代码不会执行
    return 2  # 这个return并不会被执行


result = return_num()
print(return_num())  # 1
