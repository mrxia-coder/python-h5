# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 22:17
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 16-引用当做实参.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def test1(a):
    print(a)
    print(id(a))
    a += a

    print(a)
    print(id(a))


# 1. int
b = 100
test1(b)
"""输出如下，计算前后引用的值发生了变化
100
140706878422384
200
140706878425584
"""

# 2. 列表
c = [11, 12]
test1(c)
"""输出如下，计算前后引用的值没有变
[11, 12]
2032164622920
[11, 12, 11, 12]
2032164622920
"""