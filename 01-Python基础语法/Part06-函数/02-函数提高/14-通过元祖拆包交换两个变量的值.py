# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 22:00
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 14-通过元祖拆包交换两个变量的值.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


a = 10
b = 20
print(a, b)

a, b = b, a
print(a, b)
