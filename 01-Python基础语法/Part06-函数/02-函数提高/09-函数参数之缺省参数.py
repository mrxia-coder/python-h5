# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:36
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 09-函数参数之缺省参数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def user_info(name, age, gender="男"):
    print(f"您的名字是{name},年龄是{age},性别是{gender}.")


user_info("Tom", 26)
user_info(age=26, name="Tom")
user_info("Tom", 26, "女")
