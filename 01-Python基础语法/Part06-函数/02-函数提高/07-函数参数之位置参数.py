# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:33
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 07-函数参数之位置参数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def user_info(name, age, gender):
    print(f"您的名字是{name},年龄是{age},性别是{gender}.")


# 传递和定义参数的顺序及个数必须一致。
user_info("Tom", 26, "男")
