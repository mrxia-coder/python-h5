# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:57
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 13-借助临时变量实现交换变量值.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


a = 10
b = 20
print(a, b)

# 临时变量接收a的值
temp = a
# a接收b的值
a = b
# b接收临时变量的值(a开始的值)
b = temp

print(a, b)
