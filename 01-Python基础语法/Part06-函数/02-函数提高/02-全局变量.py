# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:09
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-全局变量.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 全局变量
a = 100


def testA():
    print(a)


def testB():
    # global 关键字声明a是全局变量
    global a
    a = 200
    print(a)


testA()
testB()
print(a)

"""
总结：
    1. 函数内部直接修改 a = 200，其实是在函数内部声明一个新的局部变量a并赋值为200
    2. 函数体中，要修改全局变量，必须先提高global来声明这个变量为全局变量
"""