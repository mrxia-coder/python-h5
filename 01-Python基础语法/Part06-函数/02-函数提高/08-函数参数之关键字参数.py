# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/31 21:36
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 08-函数参数之关键字参数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def user_info(name, age, gender):
    print(f"您的名字是{name},年龄是{age},性别是{gender}.")


# 关键字参数与定义的参数个数必须一致，顺序可以不一样
user_info(name="Tom", age=26, gender="男")
user_info(age=26, gender="男", name="Tom")
user_info(gender="男", name="Tom", age=26)

"""
总结：
    1. 关键字参数传参必须与函数定义的参数个数一致， 顺序可以随便颠倒
    2. 关键字参数必须要在所有位置参数之后
"""