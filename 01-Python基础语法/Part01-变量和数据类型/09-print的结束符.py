# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 22:57
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 09-print的结束符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


print("hello", end="\n")
print("world", end="\t")

print("hello", end="...")
print("python")