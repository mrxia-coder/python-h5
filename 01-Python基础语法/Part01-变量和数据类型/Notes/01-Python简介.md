## 1、Python简介

```text
Python 是一种解释型、面向对象、动态数据类型的高级程序设计语言。
Python 由 Guido van Rossum 于 1989 年底发明，第一个公开发行版发行于 1991 年。像 Perl 语言一样, Python 源代码同样遵循 GPL(GNU General Public License) 协议。
```



## 2、Python的优点

Python是时下最流行、最火爆的编程语言之一，具体原因如下：

1. 简单、易学，适应人群广泛

    ![](http://static.staryjie.com/static/images/20200720153117.png)

2. 免费、开源

3. 应用领域广泛

    ![](http://static.staryjie.com/static/images/20200720153151.png)

> 备注：以下知名框架均是Python语言开发。

* Google开源机器学习框架：TensorFlow
* 开源社区主推学习框架：Scikit-learn
* 百度开源深度学习框架：Paddle



>  Python发展历史：https://baike.baidu.com/item/Python/407313?fr=aladdin



## 3、Python版本

- Python2.x
- Python3.x
    - Python3.5
    - Python3.6
    - Python3.7
    - Python3.8



## 4、总结

* Python优点：
    * 学习成本低
    * 开源、免费
    * 适应人群广
    * 应用领域广泛
* 该学习笔记使用的Python版本是：Python 3.7.5