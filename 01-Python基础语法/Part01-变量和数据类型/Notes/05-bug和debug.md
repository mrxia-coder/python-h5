## 1、认识Bug

所谓bug，就是程序中的错误。如果程序有错误，需要程序员排查问题，纠正错误。

![](http://static.staryjie.com/static/images/20200720230940.png)

## 2、Debug

既然程序出现了报错，那么为了程序能够正常运行，我们必须找到报错的问题并解决问题，这个找出问题并解决的过程就叫做Debug。



### 2.1 Debug工具

一般在我们使用的开发工具中都会自带一些方便我们程序员调试程序的工具，帮助我们调试程序，找出问题解决问题。

Debug工具是PyCharm IDE中集成的用来调试程序的工具，在这里程序员可以查看程序的执行细节和流程或者调解bug。



### 2.2 Debug的步骤

#### 2.2.1 打断点

* 断点位置

    目标要调试的代码块的第一行代码即可，即一个断点即可。

* 打断点的方法

    单击目标代码的行号右侧空白位置。

    ![](http://static.staryjie.com/static/images/20200720231727.png)



#### 2.2.2 Debug调试

打成功断点后，在文件内部任意位置 — 右键 -- Debug'文件名' — 即可调出Debug工具面板 -- 单击Step Over/F8，即可按步执行代码。

![](http://static.staryjie.com/static/images/20200720231848.png)

Pycharm的Debug界面：

![](http://static.staryjie.com/static/images/20200720231957.png)

上述界面中，有`Debugger`和`Console`两个输出界面：

* Debugger：显示变量和变量的细节
* Console：输出具体内容