## 1、解释器的作用

Python解释器作用：运行Python文件，将代码解释成电脑能够执行的文件，交给电脑执行。



## 2、Python解释器的种类

* CPython，C语言开发的解释器（官方），应用广泛的解释器。
* IPython，基于CPython的一种交互式解释器。
* 其他解释器
    * PyPy，基于Python语言开发的解释器。
    * JPython，运行在Java平台的解释器，直接把Python代码编译成Java字节码文件。
    * IronPython，运行在微软.Net平台上的Python解释器，可以直接把Python代码编译成.Net的字节码。



## 3、下载Python解释器

下载地址：https://www.python.org/downloads/release/python-375/

根据链接中你所需要的平台版本下载即可。

![](http://static.staryjie.com/static/images/20200720160856.png)



## 4、安装Python解释器

双击可执行文件 — 勾选[pip] -- [Next] -- [勾选添加环境变量] -- [Install]，按提示操作即可。

Windows和MacOS平台安装方法都差不多，直接下载可执行文件，双击根据提示安装即可。

Linux平台可以通过yum等包管理工具安装，或者通过编译源代码安装等等。



安装完成之后，需要加入系统的环境变量，加入环境变量完成之后，打开对于系统的Ternimal，输入下面的代码即可检查是否正确安装：

```bash
python --version
```

![](http://static.staryjie.com/static/images/20200720161255.png)

如上图所示，说明已经正确安装了Python解释器。