# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 22:47
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 07-f格式化字符串.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

name = "StaryJie"
age = 26
weight = 65.5

# 我的名字是x，我今年x岁了，体重x公斤
print("我的名字是%s，我今年%s岁了，体重%s公斤" % (name, age, weight))