# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 22:35
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 06-格式化字符串拓展.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


name = "StaryJie"
age = 26
weight = 65.5

# 我的名字是x，我今年x岁了，体重x公斤
print(f"我的名字是{name}，我今年{age}岁了，体重{weight}公斤")
