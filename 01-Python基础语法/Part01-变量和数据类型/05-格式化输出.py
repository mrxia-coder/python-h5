# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 22:11
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 05-格式化输出.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


age = 26
name = "StrayJie"
weight = 68.5
stu_id = 1
card_id = 330127199301222214

# 1. 今年我的年龄是x岁
print("今年我的年龄是%d岁" % age)

# 2. 我的名字是x
print("我的名字是%s" % name)

# 3. 我的体重是x公斤
print("我的体重是%f公斤" % weight)

# 4. 我的学号是x
print("我的学号是%06d" % stu_id)
print(("我的身份证号是%6d" % card_id))  # %06d，表示输出的整数显示位数，不足以0补全，超出当前位数则原样输出

# 5. 我的名字是x，我今年x岁
print("我的名字是%s，我今年%d岁" % (name, age))
print("我的名字是%s，我明年%d岁" % (name, age + 1))

# 6. 我的名字是x，今年x岁了，体重x公斤,学号是x
print("我的名字是%s，今年%d岁了，体重%.2f公斤,学号是%06d" % (name, age, weight, stu_id))
