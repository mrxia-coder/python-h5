# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/20 22:26
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 01-注释.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 单行注释，只能注释一行内容，以#开头，注释的内容不会被Python解释器执行
print('hello world')

print('hello python')  # 如果要在代码后面注释，需要加两个空格和#号

"""
    多行注释可以使用六个单引号或者六个单引号
    注释的内容都不会被执行
    在一行代码后面也可以写注释
"""
