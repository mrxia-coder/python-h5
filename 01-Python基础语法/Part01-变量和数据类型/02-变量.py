# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/20 22:49
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 02-变量.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
    1. 定义变量
        语法： 变量名 = 值
    2. 使用变量
    3. 变量的特点
"""

# 定义变量
my_name = "StaryJie"
schoolName = "清华大学"

# 使用变量
print(my_name)
print(schoolName)