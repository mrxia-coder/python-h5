# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/20 23:47
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 04-认识数据类型.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
    1. 根据实际经验,将不同的数据存储到不同的变量中
    2. 通过type()函数检测对于变量的数据类型
"""

myName = "StaryJie"
age = 26
height = 178.5
isAdult = True
hobbies = ["吃饭", "睡觉", "打豆豆"]
tup = ("1", "2", "3",)
info = {
    "name": myName,
    "age": age,
    "isAdult": isAdult,
    "hobbies": hobbies,
}

se = {"1", "2", "3"}

print(type(myName))
print(type(age))
print(type(height))
print(type(isAdult))
print(type(hobbies))
print(type(tup))
print(type(info))
print(type(se))
