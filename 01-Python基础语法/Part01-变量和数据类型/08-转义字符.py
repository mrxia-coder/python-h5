# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/7/21 22:47
# @Author  : staryjie
# @Site    : blog.59devops.com
# @File    : 08-转义字符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

print("hello")
print("world")

# \n换行
print("hello\nworld")


print("abcd")

# \t制表符
print("\tabcd")
print("ab\tcd")
