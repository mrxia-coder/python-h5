## 1、视图介绍和项目准备

### 1.1 视图介绍

- 视图就是`应用`中`views.py`文件中的函数

- 视图的第一个参数必须为`HttpRequest`对象，还可能包含下参数如

    - 通过正则表达式组获取的位置参数
    - 通过正则表达式组获得的关键字参数

- 视图必须返回一个`HttpResponse`对象或子对象作为响应

    - 子对象： `JsonResponse` `HttpResponseRedirect`

- 视图负责接受Web请求`HttpRequest`，进行逻辑处理，返回Web响应`HttpResponse`给请求者

    - 响应内容可以是`HTML内容`，`404错误`，`重定向`，`json数据`...

- 视图处理过程如下图：

    ![](http://static.staryjie.com/static/images/20200911085751.png)

> 使用视图时需要进行两步操作，两步操作不分先后
>
> 1. 配置`URLconf`
> 2. 在`应用/views.py`中定义视图



### 1.2 项目准备

#### 1.2.1 创建项目

```bash
# 激活虚拟环境
workon py3_django_1.11

# 创建项目
django-admin startproject bookmanager02
```



#### 1.2.2 创建子应用

```bash
# 进入项目目录
cd bookmanager02

# 创建子应用
django-admin startapp book
# 或者
python manage.py startapp book
```



#### 1.2.3 注册子应用

`bookmanager02/settings.py`

```python
# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 注册子应用
    'book.apps.BookConfig',
]
```



#### 1.2.4 本地化

`bookmanager02/settings.py`

```python
# LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'zh-hans'

# TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Shanghai'
```



#### 1.2.5 定义模型

`book/models.py`

```python
from django.db import models


# Create your models here.
# 准备书籍列表信息的模型类
class BookInfo(models.Model):
    # 创建字段，字段类型...
    name = models.CharField(max_length=20, unique=True, verbose_name="名称")
    pub_date = models.DateField(null=True, verbose_name="发行日期")
    read_count = models.IntegerField(default=0, verbose_name='阅读量')
    comment_count = models.IntegerField(default=0, verbose_name='评论量')
    is_delete = models.BooleanField(default=False, verbose_name='逻辑删除')

    class Meta:
        db_table = 'bookinfo'  # 指明数据库表名
        verbose_name = '图书'  # 在admin站点中显示的名称

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.name


# 准备人物列表信息的模型类
class PeopleInfo(models.Model):
    GENDER_CHOICES = (
        (0, 'male'),
        (1, 'femal')
    )
    name = models.CharField(max_length=20, verbose_name='名称')
    gender = models.SmallIntegerField(choices=GENDER_CHOICES, default=0, verbose_name='性别')
    description = models.CharField(max_length=200, null=True, verbose_name='描述信息')
    book = models.ForeignKey(BookInfo, on_delete=models.CASCADE, verbose_name='图书')  # 外键
    is_delete = models.BooleanField(default=False, verbose_name='逻辑删除')

    class Meta:
        db_table = 'peopleinfo'
        verbose_name = '人物信息'

    def __str__(self):
        return self.name

```



#### 1.2.6 修改数据库配置

`bookmanager02/settings.py`

```python
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': "192.168.50.176",
        'PORT': 3306,
        'USER': 'root',
        'PASSWORD': 'mysql',
        'NAME': 'book_02'
    }
}
```



#### 1.2.7 将`pymysql`作为`MySQLdb`

`bookmanager02/__init__.py`

```python
import pymysql

pymysql.install_as_MySQLdb()

```



#### 1.2.8 创建数据库

```mysql
create database book_02 charset = 'utf8';
```



#### 1.2.9 生成迁移文件

```bash
python manage.py makemigrations
```



#### 1.2.10 同步数据库

```bash
python manage.py migrate
```



#### 1.2.11 插入测试数据

```mysql
insert into bookinfo(name, pub_date, read_count, comment_count, is_delete)
values ('射雕英雄传', '1980-5-1', 12, 34, 0),
       ('天龙八部', '1986-7-24', 36, 40, 0),
       ('笑傲江湖', '1995-12-24', 20, 80, 0),
       ('雪山飞狐', '1987-11-11', 58, 24, 0);

insert into peopleinfo(name, gender, book_id, description, is_delete)
values ('郭靖', 1, 1, '降龙十八掌', 0),
       ('黄蓉', 0, 1, '打狗棍法', 0),
       ('黄药师', 1, 1, '弹指神通', 0),
       ('欧阳锋', 1, 1, '蛤蟆功', 0),
       ('梅超风', 0, 1, '九阴白骨爪', 0),
       ('乔峰', 1, 2, '降龙十八掌', 0),
       ('段誉', 1, 2, '六脉神剑', 0),
       ('虚竹', 1, 2, '天山六阳掌', 0),
       ('王语嫣', 0, 2, '神仙姐姐', 0),
       ('令狐冲', 1, 3, '独孤九剑', 0),
       ('任盈盈', 0, 3, '弹琴', 0),
       ('岳不群', 1, 3, '华山剑法', 0),
       ('东方不败', 0, 3, '葵花宝典', 0),
       ('胡斐', 1, 4, '胡家刀法', 0),
       ('苗若兰', 0, 4, '黄衣', 0),
       ('程灵素', 0, 4, '医术', 0),
       ('袁紫衣', 0, 4, '六合拳', 0);
```



#### 1.2.12 定义测试视图

`book/views.py`

```python
from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def index(request):
    return HttpResponse("index")

```



#### 1.2.13 项目配置`URLconf`

`bookmanager02/urls.py`

```python
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('book.urls')),
]
```



#### 1.2.14 子应用配置`URLconf`

在子应用目录下创建一个`urls.py`文件：

```python
from django.conf.urls import url
from book.views import index

urlpatterns = [
    url(r'^index/$', index, name='index'),
]
```



#### 1.2.15 启动开发服务器

```bash
python manage.py runserver
```

> 注意：
>
> 1. 如果启动服务的时候指定了IP或者域名，需要在项目配置文件中的`ALLOWED_HOSTS`中以字符串的形式添加进去，不然会无法访问
> 2. 如果项目配置文件中DEBUG=False，那么`ALLOWED_HOSTS`必须要设置允许访问的IP或者域名，不然项目无法被访问到



#### 1.2.16 浏览器访问测试

http://127.0.0.1:8000/index/

![](http://static.staryjie.com/static/images/20200912095408.png)



## 2、URLconf

- 浏览者通过在浏览器的地址栏中输入网址请求网站
- 对于Django开发的网站，由哪一个视图进行处理请求，是由url匹配找到的



**配置`URLconf`步骤一般如下：**



### 2.1 项目配置文件中指定`url`配置

`bookmanager02/settings.py`

```python
# ROOT_URLCONF = '项目.urls'
# 默认的配置如下
ROOT_URLCONF = 'bookmanager02.urls'
```



### 2.2 项目中的`urls.py`配置

其中项目`urls.py`中的配置有多种，在Django默认生成的文件中开头的注释已经给予提示了：

```python
"""bookmanager02 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
```

一共有**功能视图配置**、**类视图配置**和**包含关系配置(将不同的路由引导到对应子应用下的`urls.py`)**，其中下面两种配置是已经截胡吃过的：

* 功能视图配置

    1. 导入视图函数

        ```bash
        from book.views import index
        ```

    2. 将`url`添加到`urlpatterns`

        ```python
        from django.conf.urls import url
        from django.contrib import admin
        from book.views import index
        
        urlpatterns = [
            url(r'^admin/', admin.site.urls),
            # 将http://127.0.0.1:8000/index/ 引导到 book子应用下views.py视图中的index视图
            url(r'^index/$', index),
        ]
        ```

* 包含关系视图配置

    1. 导入include函数

        ```python
        from django.conf.urls import include
        ```

    2. 将`url`添加到`urlpatterns`

        ```python
        from django.conf.urls import url, include
        from django.contrib import admin
        
        urlpatterns = [
            url(r'^admin/', admin.site.urls),
            # 将除了以`admin/`之外的其他所有请求url转发到book子应用下的urls.py配置进行处理
            url(r'^', include('book.urls')),
        ]
        ```



### 2.3 字应该中`urls.py`配置

通过正则匹配对应的请求`url`，匹配成功后，调用子应用下对应的视图进行请求的处理。

```python
from django.conf.urls import url
from book.views import index

urlpatterns = [
    # 正则匹配到 index/ 的请求url，就调用views.py中的index视图进行处理
    url(r'^index/$', index, name='index'),
]

```



### 2.4 提示

1. 正则部分推荐使用 r，表示字符串不转义，这样在正则表达式中使用 \ 只写一个就可以（不需要进行转义）

2. 不能在开始加反斜杠，推荐在结束加反斜杠

    ```text
    正确：path/
    正确：path
    错误：/path
    错误：/path/
    ```

3. 请求的url被看做是一个普通的python字符串，进行匹配时不包括域名、get或post参数

    ```text
    如请求地址如下：http://127.0.0.1:8000/18/?a=10
    去掉域名和参数部分后，只剩下如下部分与正则匹配
    18/
    ```



**说明：**

1. 虽然路由结尾带/能带来上述好处，但是却违背了HTTP中URL表示资源位置路径的设计理念。
2. 是否结尾带/以所属公司定义风格为准。



## 3、路由命名与reverse反解析（逆向）

### 3.1 路由命名

在定义路由的时候，可以为路由命名，方便查找特定视图的具体路径信息。



#### 3.1.1 路由的命名空间

在使用include函数定义路由时，可以使用namespace参数定义路由的命名空间，如：

```python
url(r'^',include('book.urls',namespace='book'))
```

命名空间表示，凡是`book.urls`中定义的路由，均属于namespace指明的book名下。

**命名空间的作用：避免不同应用中的路由使用了相同的名字发生冲突，使用命名空间区别开。**



#### 3.1.2 路由命名

在定义普通路由时，可以使用name参数指明路由的名字，如：

```python
url(r'^index/$', index, name='index'),
```

在整个项目中路由命名可以重复，但是在某个路由命名空间中不能重复。



### 3.2 reverse反解析

很多时候，用户访问首页之后，需要登录或者注册，我们如果想要用户在注册或者登录之后也要返回首页，这时候如何才能够让我们能更快的找到对应的路由呢？这时候通过reverse函数配合路由名就可以很简单的实现。

使用reverse函数，可以根据路由名称，返回具体的路径，如：

```python
from django.shortcuts import render, redirect
from django.http import HttpResponse
# from django.core.urlresolvers import reverse
from django.urls import reverse


# Create your views here.
def index(request):
    """
    登录成功之后需要跳转到首页
    注册成功之后需要跳转到首页
    :param request:
    :return:
    """
    # 如果index这个路由名有很多个，就可以结合路由命名空间来确定 path = reverse('book:index')
    path = reverse('index')
    # 登录成功之后需要跳转到首页
    # return redirect(path)  # redirect()实现路由跳转

    # 注册成功之后需要跳转到首页
    # return redirect(path)

    return HttpResponse("index")
```

- 对于未指明namespace的，reverse(路由name)
- 对于指明namespace的，reverse(命名空间namespace:路由name)

> 说明：
>
> 1. 在整个Django项目中，如果路由名全部都是唯一的，那么直接通过path = reverse('路由名') 来确定唯一路由
> 2. 如果在不同的子应用中，路由名有冲突，那么最好在项目`urls.py`中设置不同的命名空间，然后通过 path = reverse('命名空间:路由名') 来确定唯一路由
> 3. 命名空间一般和子应该的名字一致，因为子应用名字是由开发者定义的，一般不会冲突，子应用名和命名空间名字一致也可以方便我们确定路由



## 4、使用`PostMan`对请求进行测试

`PostMan `是一款功能强大的网页调试与发送网页 HTTP 请求的 Chrome 插件，可以直接去对我们写出来的路由和视图函数进行调试，作为后端程序员是必须要知道的一个工具。

- 安装方式1：去 Chrome 商店直接搜索 PostMan 扩展程序进行安装

- 安装方式2：https://www.getpostman.com/官网下载桌面版

- 安装方式3：将已下载好的 PostMan 插件文件夹拖入到浏览器

    - 打开 Chrome 的扩展程序页面，打开`开发者模式`选项

        ![](http://static.staryjie.com/static/images/20200912112543.png)

    - 将插件文件夹拖入到浏览器(或者点击加载已解压的扩展程序选择文件夹)

        - Chrome 浏览器中会显示以下效果，可以直接点击启动

            ![](http://static.staryjie.com/static/images/20200912112630.png)

- 使用 PostMan，打开之后，会弹出注册页面，选择下方的`Skip this,go straight to the app`进行程序

    ![](http://static.staryjie.com/static/images/20200912112712.png)

    



## 5、HttpRequest对象

利用HTTP协议向服务器传参有如下几种方式：

- 提取URL的特定部分，如/weather/beijing/2018，可以在服务器端的路由中用正则表达式截取；
- 查询字符串（query string)，形如key1=value1&key2=value2；
- 请求体（body）中发送的数据，比如表单数据、json、xml；
- 在http报文的头（header）中。



### 5.1 URL路径参数

- 如果想从URL中获取值，需要在正则表达式中使用`分组`，
- 获取值分为两种方式
    - 位置参数
        - 参数的位置不能错
    - 关键字参数
        - 参数的位置可以变，跟关键字保持一致即可
- 注意：两种参数的方式不要混合使用，在一个正则表达式中只能使用一种参数方式
- 分别使用以上两种获取URL值的方式提取出18 188
    - `http://127.0.0.1:8000/18/188/`



#### 5.1.1 位置参数

* 应用中`urls.py`

    ```python
    url(r'^(\d+)/(\d+)/$', detail, name='detail'),
    ```

* 视图中函数: 参数的位置不能错

    ```python
    def detail(requset, category_id, book_id):
        # 构造上下文
        context = {'category_id': category_id, 'book_id': book_id}
        print(context)
        return HttpResponse('detail')
    ```

> 如果参数位置写错了，那么传入的参数就错了，数据也就错了。为了避免这种错误，可以使用关键字参数，这样和参数位置就没有关系了。



#### 5.1.2 关键字参数

* 应用中`urls.py`

    * 其中`?P<category_id>`部分表示为这个参数定义的名称为`category_id`

    * 可以是其它名称，起名要做到见名知意

        ```python
        url(r'^(?P<category_id>\d+)/(?P<book_id>\d+)/$', detail, name='detail'),
        ```

* 视图中函数: 参数的位置可以变，跟关键字保持一致即可

    ```python
    def detail(requset, category_id, book_id):
        # 构造上下文
        context = {'category_id': category_id, 'book_id': book_id}
        print(context)
        return HttpResponse('detail')
    ```



### 5.2 Django中的QueryDict对象

`HttpRequest`对象的属性GET、POST都是`QueryDict`类型的对象。

与python字典不同，`QueryDict`类型的对象用来处理同一个键带有多个值的情况：

* 方法get()：根据键获取值

    如果一个键同时拥有多个值将获取最后一个值。

    如果键不存在则返回None值，可以设置默认值进行后续处理。

    ```python
    QueryDict.get('键',默认值)
    ```

* 方法getlist()：根据键获取值，值以列表返回，可以获取指定键的所有值

    如果键不存在则返回空列表[]，可以设置默认值进行后续处理。

    ```python
    QueryDict.getlist('键',默认值)
    ```



### 5.3 查询字符串Query String

获取请求路径中的查询字符串参数（形如?k1=v1&k2=v2），可以通过`request.GET`属性获取，返回`QueryDict`对象。

```python
def detail(requset, category_id, book_id):
    # 请求： 127.0.0.1:8000/1/100/?username=staryjie&password=123456&hobbies=basketball&hobbies=pingpang
    # 构造上下文
    context = {'category_id': category_id, 'book_id': book_id}
    print(context)
    # {'category_id': '1', 'book_id': '100'}

    query_params = requset.GET
    print(query_params)
    # < QueryDict: {'username': ['staryjie'], 'password': ['123456']} >
    # username = query_params['username']
    username = query_params.get('username')
    password = query_params.get('password')
    print(username, password)

    # 获取一键多值，需要通过getlist()
    hobbie = query_params.get('hobbies')  # get('key')方法智能获取该key的最后一个值
    print(hobbie)
    hobbies = query_params.getlist('hobbies')
    print(hobbies)

    return HttpResponse('detail')
```

**重要：查询字符串不区分请求方式，即假使客户端进行POST方式的请求，依然可以通过request.GET获取请求中的查询字符串数据。**



### 5.4 请求体

请求体数据格式不固定，可以是表单类型字符串，可以是JSON字符串，可以是XML字符串，应区别对待。

可以发送请求体数据的请求方式有**POST**、**PUT**、**PATCH**、**DELETE**。

**Django默认开启了CSRF防护**，会对上述请求方式进行CSRF防护验证，在测试时可以关闭CSRF防护机制，方法为在settings.py文件中注释掉CSRF中间件，如：

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',  # 测试时注释该行，关闭CSRF防护机制
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```



**通过PostMan发送POST请求：**

![](http://static.staryjie.com/static/images/20200912154811.png)



#### 5.4.1 表单类型 Form Data

前端发送的表单类型的请求体数据，可以通过`request.POST`属性获取，返回`QueryDict`对象。

通过`PostMan`发送如下的POST请求：

![](http://static.staryjie.com/static/images/20200912155148.png)

```python
def detail(requset, category_id, book_id):
	# POST表单数据
    data = requset.POST
    print(data)
    # <QueryDict: {'username': ['staryjie'], 'password': ['123456']}>
    username = data.get('username')
    password = data.get('password')
    hobbies = data.getlist('hobbies')
    print(username, password, hobbies)

    return HttpResponse('detail')
```



#### 5.4.2 非表单类型 Non-Form Data

非表单类型的请求体数据，Django无法自动解析，可以通过`request.body`属性获取最原始的请求体数据，自己按照请求体格式（JSON、XML等）进行解析。**`request.body`返回bytes类型。**

**通过`PostMan`发送POST的Json数据：**

![](http://static.staryjie.com/static/images/20200912155930.png)



```python
def detail(requset, category_id, book_id):
    # POST json数据
    import json
    
    json_str = requset.body
    print(json_str)
    # b'{\n\t"username": "staryjie",\n\t"password": "123456"\n}'

    req_data = json.loads(json_str)
    print(req_data)
    # {'username': 'staryjie', 'password': '123456'}

    username = req_data.get('username')
    password = req_data.get('password')
    hobbies = req_data.get('hobbies')
    print(username, password, hobbies)

    return HttpResponse('detail')
```



### 5.5 请求头

可以通过**`request.META`**属性获取请求头headers中的数据，**`request.META`为字典类型**。

常见的请求头如：

- `CONTENT_LENGTH`– The length of the request body (as a string).
- `CONTENT_TYPE`– The MIME type of the request body.
- `HTTP_ACCEPT`– Acceptable content types for the response.
- `HTTP_ACCEPT_ENCODING`– Acceptable encodings for the response.
- `HTTP_ACCEPT_LANGUAGE`– Acceptable languages for the response.
- `HTTP_HOST`– The HTTP Host header sent by the client.
- `HTTP_REFERER`– The referring page, if any.
- `HTTP_USER_AGENT`– The client’s user-agent string.
- `QUERY_STRING`– The query string, as a single (unparsed) string.
- `REMOTE_ADDR`– The IP address of the client.
- `REMOTE_HOST`– The hostname of the client.
- `REMOTE_USER`– The user authenticated by the Web server, if any.
- `REQUEST_METHOD`– A string such as`"GET"`or`"POST"`.
- `SERVER_NAME`– The hostname of the server.
- `SERVER_PORT`– The port of the server (as a string).

具体使用如下：

```python
def detail(requset, category_id, book_id):
    # 请求头
    header_data = requset.META
    # print(header_data)

    content_type = header_data.get('CONTENT_TYPE')
    user_agent = header_data.get('User-Agent')
    print(content_type)
    print(user_agent)

    return HttpResponse('detail')
```



### 5.6 其他常用HttpRequest对象属性

- **method**：一个字符串，表示请求使用的HTTP方法，常用值包括：'GET'、'POST'。
- **user：请求的用户对象。**
- path：一个字符串，表示请求的页面的完整路径，不包含域名和参数部分。
- encoding：一个字符串，表示提交的数据的编码方式。
    - 如果为None则表示使用浏览器的默认设置，一般为utf-8。
    - 这个属性是可写的，可以通过修改它来修改访问表单数据使用的编码，接下来对属性的任何访问将使用新的encoding值。
- FILES：一个类似于字典的对象，包含所有的上传文件。



## 6、HttpResponse对象

视图在接收请求并处理后，必须返回HttpResponse对象或子对象。HttpRequest对象由Django创建，HttpResponse对象由开发人员创建。



### 6.1 HttpResponse

可以使用**django.http.HttpResponse**来构造响应对象。

```python
HttpResponse(content=响应体, content_type=响应体数据类型, status=状态码)
```

也可通过HttpResponse对象属性来设置响应体、响应体数据类型、状态码：

- content：表示返回的内容。
- status_code：返回的HTTP响应状态码。
- content_type： 是一个MIME类型。例如： text/html   application/json  text/javascript text/css image/png image/jpeg 等等

响应头可以直接将HttpResponse对象当做字典进行响应头键值对的设置：

```python
response = HttpResponse()
response['subject'] = 'Python'  # 自定义响应头subject, 值为Python
```

示例：

```python
from django.http import HttpResponse

def response(request):
    return HttpResponse('subject python', status=400)
    # 或者
    response = HttpResponse('subject python')
    response.status_code = 400
    response['subject'] = 'Python'
    return response
```



### 6.2 HttpResponse子类

Django提供了一系列HttpResponse的子类，可以快速设置状态码。

```python
HttpResponseRedirect 301
HttpResponsePermanentRedirect 302
HttpResponseNotModified 304
HttpResponseBadRequest 400
HttpResponseNotFound 404
HttpResponseForbidden 403
HttpResponseNotAllowed 405
HttpResponseGone 410
HttpResponseServerError 500
```



### 6.3 JsonResponse

若要返回json数据，可以使用JsonResponse来构造响应对象，作用：

- 帮助我们将数据转换为json字符串
- 设置响应头**Content-Type**为**application/json**

```python
from django.http import JsonResponse

def response(request):
    return JsonResponse({'city': 'hangzhou', 'subject': 'python'})
```



### 6.4 redirect重定向

```python
from django.shortcuts import redirect
from django.urls import reverse

def response(request):
    # return rediect('/index/')
    return redirect(reverse('book:index'))
```



## 7、状态保持

- 浏览器请求服务器是无状态的。
- **无状态**：指一次用户请求时，浏览器、服务器无法知道之前这个用户做过什么，每次请求都是一次新的请求。
- **无状态原因**：浏览器与服务器是使用Socket套接字进行通信的，服务器将请求结果返回给浏览器之后，会关闭当前的Socket连接，而且服务器也会在处理页面完毕之后销毁页面对象。
- 有时需要保持下来用户浏览的状态，比如用户是否登录过，浏览过哪些商品等
- 实现状态保持主要有两种方式：
    - 在客户端存储信息使用`Cookie`
    - 在服务器端存储信息使用`Session`



### 7.1 Cookie

Cookie，有时也用其复数形式Cookies，指某些网站为了辨别用户身份、进行session跟踪而储存在用户本地终端上的数据（通常经过加密）。Cookie最早是网景公司的前雇员Lou Montulli在1993年3月的发明。Cookie是由服务器端生成，发送给User-Agent（一般是浏览器），浏览器会将Cookie的key/value保存到某个目录下的文本文件内，下次请求同一网站时就发送该Cookie给服务器（前提是浏览器设置为启用cookie）。Cookie名称和值可以由服务器端开发自己定义，这样服务器可以知道该用户是否是合法用户以及是否需要重新登录等。服务器可以利用Cookies包含信息的任意性来筛选并经常性维护这些信息，以判断在HTTP传输中的状态。Cookies最典型记住用户名。

Cookie是存储在浏览器中的一段纯文本信息，建议不要存储敏感信息如密码，因为电脑上的浏览器可能被其它人使用。



```text
cookie基于客户端
cookie基于IP(域名)

Cookie流程：
    第一次请求：
        1. 客户端第一次请求服务器的时候，不会携带任何Cookie信息
        2. 服务器接收到请求后，发现请求中没有带任何Cookie信息
        3. 服务器设置一个Cookie，这个Cookie设置在响应中
        4. 客户端收到这个响应之后，发现响应中有Cookie信息，客户端会将Cookie信息保存起来

    第二次及以后的请求：
        5. 客户端第二次以及之后的请求中都会携带这个Cookie信息
        6. 服务器接收到请求之后，读取请求中的Cookie信息，就可以通过Cookie信息识别这个请求是谁发送的了

从http协议的角度深入掌握cookie的流程
    第一次请求：
        1. 第一次请求服务器，不会携带任何cookie信息，请求头中没有任何cookie信息
        2. 服务器发现请求中没有cookie信息，就会在响应中设置相关的cookie信息，响应头中会包含对应的set_cookie信息
    
    第二次及以后的请求：
        3. 第二次及以后的请求中都会携带cookie信息，在请求头中可以找到对应的cookie信息
        4.（可选）结合代码，如果代码中没有设置cookie的操作，那么响应头中就没有set_cookie的信息
```



#### 7.1.1 Cookie的特点

- Cookie以键值对的格式进行信息的存储。
- Cookie基于域名安全，不同域名的Cookie是不能互相访问的，如访问www.jd.com时向浏览器中写了Cookie信息，使用同一浏览器访问www.baidu.com时，无法访问到www.jd.com写的Cookie信息。
- 当浏览器请求某网站时，会将浏览器存储的跟网站相关的所有Cookie信息提交给网站服务器。



#### 7.1.2 设置Cookie

可以通过**HttpResponse**对象中的**set_cookie**方法来设置cookie。

```python
HttpResponse.set_cookie(cookie名, value=cookie值, max_age=cookie有效期)
```

* **max_age**单位为秒，默认为**None** 。如果是临时cookie，可将max_age设置为None。



示例：请求地址是：http://127.0.0.1:8000/set_cookie/?username=staryjie

```python
def set_cookie(request):
    # 1.先判断有没有Cookie信息
    # 假设没有Cookie

    # 2.获取用户名
    username = request.GET.get('username')
    # 3.设置Cookie
    response = HttpResponse('set_cookie')
    # response.set_cookie('username', username)  # 临时Cookie，会话结束（浏览器关闭）就失效
    response.set_cookie('username', username, max_age=3600)  # 设置时效，单位：秒

    # 4.返回响应
    return response
```

![](http://static.staryjie.com/static/images/20200912202210.png)

#### 7.1.3 读取Cookie

可以通过**HttpResponse**对象的**COOKIES**属性来读取本次请求携带的cookie值。**request.COOKIES为字典类型**。

```python
def get_cookie(request):
    # 1.获取Cookie
    cookies = request.COOKIES
    username = cookies.get('username')

    response = HttpResponse('get_cookie - ' + username)

    return response
```

![](http://static.staryjie.com/static/images/20200913110553.png)



#### 7.1.4 删除Cookie

可以通过**HttpResponse**对象中的delete_cookie方法来删除。

```python
response.delete_cookie('username')
# 或者
reponse.set_cookie('username', 'staryjie', max_age=0)  # max_age=0也是删除cookie的一种方式
```



### 7.2 Session

Session依赖于Cookie。一般情况下如果浏览器禁用Cookie，则Session无法实现。



```bash
Session流程：
    第一次请求：
        1. 客户端第一次请求的时候可以携带一些信息(比如登录时候使用的 用户名、密码等)，cokkies中没有任何信息
        2. 服务器接接收到请求中的信息之后进行验证，验证通过则在服务器上设置session信息
        3. 设置session信息的同时，服务器会自动在响应头中设置一个sessionid的的cookie信息(不需要我们自己去设置)
        4. 客户端在cookie中设置sessionid的信息
    第二次及以后的请求：
        5. 第二次及其之后的请求都会携带sessionid信息
        6. 服务器接收到请求之后，会获取到sessionid信息，然后通过sessionid进行验证，验证成功则获取相应的session信息
```



#### 7.2 1 启用Session

**Django项目默认启用Session。**

可以在settings.py文件中查看:

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',  # Django默认启用Session
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',  # 测试时注释该行，关闭CSRF防护机制
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```

如需禁用session，将上图中的session中间件注释掉即可。



#### 7.2.2 存储方式

在settings.py文件中，可以设置session数据的存储方式，可以保存在数据库、本地缓存等。

##### 7.2.2.1 数据库

存储在数据库中，如下设置可以写，也可以不写，**这是默认存储方式**。

```python
SESSION_ENGINE='django.contrib.sessions.backends.db'
```

> 默认在Django的配置文件中没有该配置，但是不写也是默认这种配置。

如果存储在数据库中，需要在项INSTALLED_APPS中安装Session应用。

```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',  # 将session存储在数据库中需要注册该项
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 注册子应用
    'book.apps.BookConfig',
]
```

在数据库的表如图所示：

![](http://static.staryjie.com/static/images/20200913134612.png)

表结构如下：

![](http://static.staryjie.com/static/images/20200913134647.png)

由表结构可知，操作Session包括三个数据：键，值，过期时间。



##### 7.2.2.2 本地缓存

存储在本机内存中，如果丢失则不能找回，比数据库的方式读写更快。

```python
SESSION_ENGINE='django.contrib.sessions.backends.cache'
```



##### 7.2.2.3 混合存储

优先从本机内存中存取，如果没有则从数据库中存取。

```python
SESSION_ENGINE='django.contrib.sessions.backends.cached_db'
```



##### 7.2.2.4 Redis

在redis中保存session，需要引入第三方扩展，我们可以使用**django-redis**来解决。

参考文档：https://django-redis-chs.readthedocs.io/zh_CN/latest/

1. 安装扩展

    ```bash
    pip install django-redis
    ```

2. 配置

    在settings.py文件中做如下设置:

    ```python
    CACHES = {
        'default': {
            'BACKEND': 'django_redis.cache.RedisCache',
            'LOCATION': 'redis://127.0.0.1:6379/1',
            'OPTIONS': {
                'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            }
        }
    }
    SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
    SESSION_CACHE_ALIAS = 'default'
    ```

**注意：**

如果redis的ip地址不是本地回环127.0.0.1，而是其他地址，访问Django时，可能出现Redis连接错误，如下：

![](http://static.staryjie.com/static/images/20200913134957.png)

**解决方法：**

修改redis的配置文件，添加特定ip地址。

```bash
bind 0.0.0.0 10.211.55.5
```

修改完之后需要重启redis。



### 7.3 session操作

通过HttpRequest对象的session属性进行会话的读写操作。

#### 7.3.1 以键值对的方式写session

```python
request.session['username'] = username
```

![](http://static.staryjie.com/static/images/20200913135702.png)

sessionid的值是加密之后的。



#### 7.3.2 根据键读取值

```python
request.session.get('键',默认值)
```



#### 7.3.3 清除所有session

清除所有session，在存储中删除值部分。

```python
request.session.clear()
```



#### 7.3.4 清除session数据

清除session数据，在存储中删除session的整条数据。

```python
request.session.flush()
```



#### 7.3.5 删除session中的指定键及值

删除session中的指定键及值，在存储中只删除某个键及对应的值。

```python
del request.session['键']
```



#### 7.3.6 设置session的有效期

```python
request.session.set_expiry(value)
```

- 如果value是一个整数，session将在value秒没有活动后过期。
- 如果value为0，那么用户session的Cookie将在用户的浏览器关闭时过期。
- 如果value为None，那么session有效期将采用系统默认值， **默认为两周**，可以通过在settings.py中设置**SESSION_COOKIE_AGE**来设置全局默认值。



## 8、类视图与中间件

### 8.1 类视图

思考：一个视图，是否可以处理两种逻辑？比如get和post请求逻辑。

如何在一个视图中处理get和post请求？

![](http://static.staryjie.com/static/images/20200913143922.png)

注册视图处理get和post请求：

以函数的方式定义的视图称为**函数视图**，函数视图便于理解。但是遇到一个视图对应的路径提供了多种不同HTTP请求方式的支持时，便需要在一个函数中编写不同的业务逻辑，代码可读性与复用性都不佳。

```python
def register(request):
    """处理注册"""

    # 获取请求方法，判断是GET/POST请求
    if request.method == 'GET':
        # 处理GET请求，返回注册页面
        return render(request, 'register.html')
    else:
        # 处理POST请求，实现注册逻辑
        return HttpResponse('这里实现注册逻辑')
```



#### 8.1.1 类视图的使用

在Django中也可以使用类来定义一个视图，称为**类视图**。

使用类视图可以将视图对应的不同请求方式以类中的不同方法来区别定义。如下所示：

```python
from django.views.generic import View

class RegisterView(View):
    """类视图：处理注册"""

    def get(self, request):
        """处理GET请求，返回注册页面"""
        return render(request, 'register.html')

    def post(self, request):
        """处理POST请求，实现注册逻辑"""
        return HttpResponse('这里实现注册逻辑')
```

类视图的好处：

- **代码可读性好**
- **类视图相对于函数视图有更高的复用性** ， 如果其他地方需要用到某个类视图的某个特定逻辑，直接继承该类视图即可

定义类视图需要继承自Django提供的父类**View**，可使用`from django.views.generic import View`或者`from django.views.generic.base import View`导入，定义方式如上所示。

**配置路由时，使用类视图的**`as_view()`**方法来添加**。

```python
urlpatterns = [
    # 视图函数：注册
    # url(r'^register/$', views.register, name='register'),
    # 类视图：注册
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
]
```



#### 8.1.2 类视图原理

```python
@classonlymethod
    def as_view(cls, **initkwargs):
        """
        Main entry point for a request-response process.
        """
        ...省略代码...

        def view(request, *args, **kwargs):
            self = cls(**initkwargs)
            if hasattr(self, 'get') and not hasattr(self, 'head'):
                self.head = self.get
            self.request = request
            self.args = args
            self.kwargs = kwargs
            # 调用dispatch方法，按照不同请求方式调用不同请求方法
            return self.dispatch(request, *args, **kwargs)

        ...省略代码...

        # 返回真正的函数视图
        return view


    def dispatch(self, request, *args, **kwargs):
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)
```

![](http://static.staryjie.com/static/images/20200913150505.png)



#### 8.1.3 类视图的多继承重写dispatch

```python
class CenterView(View):

    def get(self,request):
        return HttpResponse("OK")

    def post(self,request):
        return HttpResponse("OK")
```

使用面向对象多继承的特性和MRO查找顺序。

```python
class CenterView(LoginRequireMixin,View):

    def get(self,request):
        return HttpResponse("OK")

    def post(self,request):
        return HttpResponse("OK")
```



### 8.2 中间件

Django中的中间件是一个轻量级、底层的插件系统，可以介入Django的请求和响应处理过程，修改Django的输入或输出。中间件的设计为开发者提供了一种无侵入式的开发方式，增强了Django框架的健壮性。

我们可以使用中间件，在Django处理视图的不同阶段对输入或输出进行干预。

Django中间件文档：https://docs.djangoproject.com/en/1.11/topics/http/middleware/



#### 8.2.1 中间件的定义方法

定义一个中间件工厂函数，然后返回一个可以被调用的中间件。

中间件工厂函数需要接收一个可以调用的get_response对象。

返回的中间件也是一个可以被调用的对象，并且像视图一样需要接收一个request对象参数，返回一个response对象。

```python
def simple_middleware(get_response):
    # 此处编写的代码仅在Django第一次配置和初始化的时候执行一次。

    def middleware(request):
        # 此处编写的代码会在每个请求处理视图前被调用。

        response = get_response(request)

        # 此处编写的代码会在每个请求处理视图之后被调用。

        return response

    return middleware
```

例如，在book应用中新建一个middleware.py文件：

```python
def my_middleware(get_response):
    print('init 被调用')
    def middleware(request):
        print('before request 被调用')
        response = get_response(request)
        print('after response 被调用')
        return response
    return middleware
```

**定义好中间件后，需要在settings.py 文件中添加注册中间件**

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'book.middleware.my_middleware',  # 添加中间件
]
```

定义一个视图进行测试

```python
def middleware(request):
    print('view 视图被调用')
    return HttpResponse('OK')
```

![](http://static.staryjie.com/static/images/20200913155526.png)

**注意：Django运行在调试模式下，中间件init部分有可能被调用两次。**



#### 8.2.2 多个中间件的执行顺序

- 在请求视图被处理**前**，中间件**由上至下**依次执行
- 在请求视图被处理**后**，中间件**由下至上**依次执行

![](http://static.staryjie.com/static/images/20200913161204.png)

示例：定义两个中间件

```python
def my_middleware(get_response):
    print('init 被调用')
    def middleware(request):
        print('before request 被调用')
        response = get_response(request)
        print('after response 被调用')
        return response
    return middleware

def my_middleware2(get_response):
    print('init2 被调用')
    def middleware(request):
        print('before request 2 被调用')
        response = get_response(request)
        print('after response 2 被调用')
        return response
    return middleware
```

注册添加两个中间件

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'users.middleware.my_middleware',  # 添加
    'users.middleware.my_middleware2',  # 添加
]
```

执行结果：

```bash
init2 被调用
init 被调用
before request 被调用
before request 2 被调用
view 视图被调用
after response 2 被调用
after response 被调用
```