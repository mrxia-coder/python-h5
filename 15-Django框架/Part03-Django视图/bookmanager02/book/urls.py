from django.conf.urls import url
from book.views import index, detail, set_cookie, get_cookie, set_session, get_session, BookView, CenterView, middleware

urlpatterns = [
    # name 就是给路由起名字
    # 可以通过name找到这个路由
    url(r'^index/$', index, name='index'),
    # url(r'^(\d+)/(\d+)/$', detail, name='detail'),
    # 使用关键字参数，可以避免参数位置出错引起的其他数据错误
    url(r'^(?P<category_id>\d+)/(?P<book_id>\d+)/$', detail, name='detail'),
    url(r'^set_cookie/$', set_cookie, name='set_cookie'),
    url(r'^get_cookie/$', get_cookie, name='get_cookie'),
    url(r'^set_session/$', set_session, name='set_session'),
    url(r'^get_session/$', get_session, name='get_session'),
    # 类视图url引导
    url(r'login/$', BookView.as_view(), name='bookview'),
    url(r'^center/$', CenterView.as_view(), name='center'),
    url(r'^middleware/$', middleware, name='middleware'),
]
