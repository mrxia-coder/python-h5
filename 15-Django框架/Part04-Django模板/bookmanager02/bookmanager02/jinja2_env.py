from django.contrib.staticfiles.storage import staticfiles_storage
from django.template.defaultfilters import date, default
from django.urls import reverse

from jinja2 import Environment


def environment(**options):
    # 1.创建Environment实例
    env = Environment(**options)

    # 2.指定或者更新jinja2的函数指向django的指定过滤器
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
        'date': date,  # 指定jinja2的date过滤器执行djang模板的date过滤器
        'default': default,
    })

    # 返回Environment实例
    return env
