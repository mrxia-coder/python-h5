from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
# from django.core.urlresolvers import reverse
from django.urls import reverse
import datetime


# Create your views here.
def index(request):
    """
    登录成功之后需要跳转到首页
    注册成功之后需要跳转到首页
    :param request:
    :return:
    """
    # 如果index这个路由名有很多个，就可以结合路由命名空间来确定 path = reverse('book:index')
    # 如果路由命名是在整个项目中也是唯一的，那么可以直接通过路由名获取路由
    path = reverse('index')
    # 登录成功之后需要跳转到首页
    # return redirect(path)

    # 注册成功之后需要跳转到首页
    # return redirect(path)

    return HttpResponse("index")


def detail(requset, category_id, book_id):
    # GET请求
    """
    # 请求： 127.0.0.1:8000/1/100/?username=staryjie&password=123456&hobbies=basketball&hobbies=pingpang
    # 构造上下文
    context = {'category_id': category_id, 'book_id': book_id}
    print(context)
    # {'category_id': '1', 'book_id': '100'}

    query_params = requset.GET
    print(query_params)
    # < QueryDict: {'username': ['staryjie'], 'password': ['123456']} >
    # username = query_params['username']
    username = query_params.get('username')
    password = query_params.get('password')
    print(username, password)

    # 获取一键多值，需要通过getlist()
    hobbie = query_params.get('hobbies')  # get('key')方法智能获取该key的最后一个值
    print(hobbie)
    hobbies = query_params.getlist('hobbies')
    print(hobbies)
    """

    # POST表单数据
    """
    data = requset.POST
    print(data)
    # <QueryDict: {'username': ['staryjie'], 'password': ['123456']}>
    username = data.get('username')
    password = data.get('password')
    hobbies = data.getlist('hobbies')
    print(username, password, hobbies)
    """

    # POST json数据
    """
    import json
    json_str = requset.body
    print(json_str)
    # b'{\n\t"username": "staryjie",\n\t"password": "123456"\n}'

    req_data = json.loads(json_str)
    print(req_data)
    # {'username': 'staryjie', 'password': '123456'}

    username = req_data.get('username')
    password = req_data.get('password')
    hobbies = req_data.get('hobbies')
    print(username, password, hobbies)
    """

    # 请求头
    """
    header_data = requset.META
    # print(header_data)

    content_type = header_data.get('CONTENT_TYPE')
    user_agent = header_data.get('User-Agent')
    print(content_type)
    print(user_agent)
    """

    # HttpResponse
    """
    # 1. conetnt        传递字符串，不要传递对象、字典等其他类型的数据
    # 2. status         状态码 HTTP status code must be an integer from 100 - 599. 只能使用系统规定的状态码
    # 3. content_type   是一个MIME类型
    #                   语法： 大类/小类, 例如： text/html   application/json  text/javascript text/css image/png image/jpeg 等等

    context = {"city": "hangzhou", "subject": "Python"}
    
    return JsonResponse(data=context)
    """
    return HttpResponse('detail', status=200)


# 状态保持 Cookie和Session

# 保存在客户端的是Cookie，保存在服务器的是Session
"""
Cookie流程：
    第一次请求：
        1. 客户端第一次请求服务器的时候，不会携带任何Cookie信息
        2. 服务器接收到请求后，发现请求中没有带任何Cookie信息
        3. 服务器设置一个Cookie，这个Cookie设置在响应中
        4. 客户端收到这个响应之后，发现响应中有Cookie信息，客户端会将Cookie信息保存起来

    第二次及以后的请求：
        5. 客户端第二次以及之后的请求中都会携带这个Cookie信息
        6. 服务器接收到请求之后，读取请求中的Cookie信息，就可以通过Cookie信息识别这个请求是谁发送的了

从http协议的角度深入掌握cookie的流程
    第一次请求：
        1. 第一次请求服务器，不会携带任何cookie信息，请求头中没有任何cookie信息
        2. 服务器发现请求中没有cookie信息，就会在响应中设置相关的cookie信息，响应头中会包含对应的set_cookie信息
    
    第二次及以后的请求：
        3. 第二次及以后的请求中都会携带cookie信息，在请求头中可以找到对应的cookie信息
        4.（可选）结合代码，如果代码中没有设置cookie的操作，那么响应头中就没有set_cookie的信息
"""


def set_cookie(request):
    # 1.先判断有没有Cookie信息
    # 假设没有Cookie

    # 2.获取用户名
    username = request.GET.get('username')
    # 3.设置Cookie
    response = HttpResponse('set_cookie')
    # response.set_cookie('username', username)  # 临时Cookie，会话结束（浏览器关闭）就失效
    response.set_cookie('username', username, max_age=3600)  # 设置时效， 单位：秒

    # 4.返回响应
    return response


def get_cookie(request):
    # 1.获取Cookie
    cookies = request.COOKIES
    username = cookies.get('username')

    response = HttpResponse('get_cookie - ' + username)

    return response


# Session
"""
Session依赖于Cookie。一般情况下如果浏览器禁用Cookie，则Session无法实现。

Session流程：
    第一次请求：
        1. 客户端第一次请求的时候可以携带一些信息(比如登录时候使用的 用户名、密码等)，cokkies中没有任何信息
        2. 服务器接接收到请求中的信息之后进行验证，验证通过则在服务器上设置session信息
        3. 设置session信息的同时，服务器会自动在响应头中设置一个sessionid的的cookie信息(不需要我们自己去设置)
        4. 客户端在cookie中设置sessionid的信息
    第二次及以后的请求：
        5. 第二次及其之后的请求都会携带sessionid信息
        6. 服务器接收到请求之后，会获取到sessionid信息，然后通过sessionid进行验证，验证成功则获取相应的session信息
"""


def set_session(request):
    # 1.获取cookie中的信息
    print(request.COOKIES)

    # 2.验证session信息
    # 假设验证通过
    username = request.GET.get('username')
    passowrd = request.GET.get('password')

    print(username, passowrd)
    # 3.设置session
    # 设置session的时候session做了两件事：
    # 1. 将session保存在数据库中
    # 2. 设置了一个sessionid为key的cookie
    request.session['username'] = username

    # 4.返回响应
    # 在响应头中有包含sessionid为key的cookie信息，内如是 sessionid: xxxxxx
    return HttpResponse('set_session')


def get_session(request):
    # 1. 获取Cookie
    print(request.COOKIES)
    # 2. 验证sessionid
    # 假设验证成功，可以获取sessionid
    print(request.session)
    username = request.session.get('username')
    print(username)

    # 3. 返回响应
    return HttpResponse('get_session - ' + username)


def login(requset):
    # 通过不同的请求方法区分业务逻辑
    if request.method == 'GET':
        # GET请求获取登录页面，展示登录页面
        pass
    elif requset.method == 'POST':
        # POST请求实现用户登录验证
        pass


"""
面向对象：类视图
    
    1. 定义类视图
        1. 继承自 View （from django.views import View 或者 from django.views.generic.base import View）
        2. 不同的请求方法处理不同的业务逻辑
            类视图不同的方法 直接采用 http请求的名字作为我我们的函数名，例如： get  post ...
        3. 类视图方法的第二个参数必须是请求示例对象
        4. 类视图方法必须有一个HttpResponse或者其子类
    
    2. 类视图的url引导
"""
from django.views import View


class BookView(View):
    def get(self, requset):
        return HttpResponse('get')

    def post(self, request):
        return HttpResponse('post')

    def put(self, request):
        return HttpResponse('put')


# 个人中心页面    -- 必须登录才能展示
"""
GET 展示页面
POST 修改数据

通过类视图方法实现
"""
from django.contrib.auth.mixins import LoginRequiredMixin  # 可以检查用户是否登录


class CenterView(LoginRequiredMixin, View):
    """
    这里需求是要先登录之后才可以进行界面展示和数据修改，
    然而，在urls.py中 url(r'^center/$', CenterView.as_view(), name='center'),  这里 CenterView.as_view() 会去触发as_view()方法，找到一个叫dispatch的方法，
    但是在View和LoginRequiredMixin中都存在dispatch方法，View中的dispatch方法是判断请求是否存在并且返回一个handler()去执行我们已经定义的函数（get()、post()等），而 LoginRequiredMixin的dispatch方法是是用来检测用户是否已经登录的
    所以依照MRO顺序，需要先把LoginRequiredMixin放在左边，这样如果用户没有登录，就会跳转到登录页面，而不会进行数据展示或者修改的操作
    """

    def get(self, request):
        return HttpResponse('展示数据')

    def post(self, request):
        return HttpResponse('修改数据')


# 中间件
def middleware(request):
    print("view视图被调用")
    return HttpResponse('OK')


# 视图渲染模板
class HomeView(View):
    def get(self, request):
        # 1.获取数据
        username = request.GET.get('username')
        context = {
            'username': username,
            'age': 26,
            'birthday': datetime.datetime.now(),
            'salary': {
                '2018': 12000,
                '2019': 18000,
                '2020': 25000,
            },
            'friends': ['lucy', 'bob', 'lance'],
            'desc': "<script>alert('热情似火')</script>",
        }
        return render(request, 'index.html', context=context)
