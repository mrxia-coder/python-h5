from django.conf.urls import url
from book.views import booklist

urlpatterns = [
    # 匹配书籍列表信息的URL,调用对应的booklist视图
    url(r'^booklist/$', booklist, name='booklist'),
]
