from django.shortcuts import render
from django.http import HttpResponse
from book.models import BookInfo


# Create your views here.
def booklist(request):
    # 1.数据库中查询数据
    books = BookInfo.objects.all()
    # 2.组织数据
    context = {
        "books": books,
    }
    # 3.传递给模板
    # return HttpResponse("OK!")
    return render(request, 'book/booklist.html', context=context)



