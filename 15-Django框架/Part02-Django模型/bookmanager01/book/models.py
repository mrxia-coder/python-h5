from django.db import models

# Create your models here.
"""
1. ORM
    表   --> 字段
    字段 --> 属性
2. 模型类需要继承自 models.Model
3. 模型类会自动为我们添加(生成)一个主键
4. 属性名 = 属性类型(选项)
    属性名:
        尽量不要使用Python和MySQL的关键字
        不要使用连续的下划线(__)
    选项：
        CharField()     必须设置max_length
        null=True       允许为空
        unique          唯一
        default         默认值
        verbose_name    admin后台显示名称
"""


# 准备书籍列表信息的模型类
class BookInfo(models.Model):
    # 创建字段，字段类型...
    name = models.CharField(max_length=20, unique=True, verbose_name="名称")
    pub_date = models.DateField(null=True, verbose_name="发行日期")
    read_count = models.IntegerField(default=0, verbose_name='阅读量')
    comment_count = models.IntegerField(default=0, verbose_name='评论量')
    is_delete = models.BooleanField(default=False, verbose_name='逻辑删除')

    class Meta:
        db_table = 'bookinfo'  # 指明数据库表名
        verbose_name = '图书'  # 在admin站点中显示的名称

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.name


# 准备人物列表信息的模型类
class PeopleInfo(models.Model):
    GENDER_CHOICES = (
        (0, 'male'),
        (1, 'femal')
    )
    name = models.CharField(max_length=20, verbose_name='名称')
    gender = models.SmallIntegerField(choices=GENDER_CHOICES, default=0, verbose_name='性别')
    description = models.CharField(max_length=200, null=True, verbose_name='描述信息')
    book = models.ForeignKey(BookInfo, on_delete=models.CASCADE, verbose_name='图书')  # 外键
    is_delete = models.BooleanField(default=False, verbose_name='逻辑删除')

    class Meta:
        db_table = 'peopleinfo'
        verbose_name = '人物信息'

    def __str__(self):
        return self.name
