from django.conf.urls import url
from book.views import index

urlpatterns = [
    # 一条完整的URL包含URL匹配规则和对应的处理函数，视图和URL的关系也就是在这里绑定的
    url(r'^index/$', index, name="index"),  # 其中 r('^index/$')是url正则匹配规则， index是视图, name="index"是该url的别名
]
