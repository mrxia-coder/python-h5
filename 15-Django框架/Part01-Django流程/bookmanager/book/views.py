from django.shortcuts import render  # 导入render函数渲染模板
from django.http import HttpRequest, HttpResponse

# Create your views here.

"""
视图
    1. 每个视图都是一个Python函数
    2. 函数的第一个参数是`request`(请求),它是一个HttpRequest的实例对象
    3. 我们必须返回一个响应，响应应该是HttpResponse的实例对象或者子类对象
"""


def index(request):

    # return HttpResponse("index")
    # 准备给模板的数据
    content = {"title": "Django模板接收视图函数传入的数据并展示"}
    return render(request, 'book/index.html', content)
