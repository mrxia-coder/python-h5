"""bookmanager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # django1.x版本提供正则匹配来配置URL，这里需要将各个子应用下的urls配置进来，然后提供url总入口分发到各个子应用中的url配置
    # 这里将除了 http://127.0.0.1:8000/admin 之外的url全部分发到book子应用的urls配置中，如果有其他的子应用应该要做区分，避免url匹配不到正确的视图
    url(r'^', include('book.urls')),
]
