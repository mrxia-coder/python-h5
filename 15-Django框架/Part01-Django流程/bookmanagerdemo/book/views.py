from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from book.models import BookInfo


# Create your views here.
def index(requests):
    # 实现业务逻辑
    # 1.把所有书籍查询出来
    books = BookInfo.objects.all()
    context = {"books": books}

    return render(requests, 'book/index.html', context=context)
