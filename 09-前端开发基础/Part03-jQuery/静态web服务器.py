# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/13 23:11
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 静态web服务器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import sys
import socket
import threading


# 定义web服务器类
class HttpWebServer(object):
    def __init__(self, port):
        tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        tcp_server_socket.bind(("", port))

        tcp_server_socket.listen(128)

        self.tcp_server_socket = tcp_server_socket

    @staticmethod
    def handler_client_request(new_socket, ip_port):
        recv_client_data = new_socket.recv(4096)
        if len(recv_client_data) == 0:
            print(f"客户端{ip_port}关闭了，关闭连接！")
            new_socket.close()
            return

        recv_client_content = recv_client_data.decode('utf-8')
        print(recv_client_content)

        # 根据只读字符串截取请求资源路径
        request_list = recv_client_content.split(" ", maxsplit=2)

        # 获取请求资源路径
        request_path = request_list[1]
        print(request_path)

        # 根据不同的资源请求路径拼装不同的响应体
        if request_path == "/":
            request_path = "/index.html"

        try:
            with open("static" + request_path, "rb") as f:
                file_data = f.read()
        except FileNotFoundError as e:
            # 请求资源路径不存在，返回404
            # 响应行
            response_line = "HTTP/1.1 404 NOT FOUND\r\n"
            # 响应头
            response_header = "Server: Tegine3.2.3\r\n"
            with open("static/error.html", 'rb') as ferr:
                file_data = ferr.read()

            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
            new_socket.send(response_data)
            new_socket.close()
        else:
            # 响应行
            response_line = "HTTP/1.1 200 OK\r\n"
            # 响应头
            response_header = "Server: Tegine3.2.3\r\n"

            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
            new_socket.send(response_data)
        finally:
            new_socket.close()

    # 启动web服务器进行工作
    def start(self):
        while True:
            new_socket, ip_port = self.tcp_server_socket.accept()
            sub_thread = threading.Thread(target=self.handler_client_request, args=(new_socket, ip_port))
            sub_thread.setDaemon(True)
            sub_thread.start()


# 程序入口
def main():
    if len(sys.argv) != 2:
        print("web服务启动命令： python3 xxx.py 8000")
        return
    # 判断端口号字符串是否都是数字
    if not sys.argv[1].isdigit():
        print("web服务启动命令： python3 xxx.py 8000")
        return

    port = int(sys.argv[1])
    web_server = HttpWebServer(port)
    web_server.start()


if __name__ == '__main__':
    main()
