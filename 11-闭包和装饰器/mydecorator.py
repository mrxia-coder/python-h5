# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 16:40
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : mydecorator.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 定义装饰器
# 如果闭包函数的参数有且只有一个，并且这个参数是一个函数，那么这个闭包函数就称为装饰器
def check_login(fn):
    print("装饰器开始执行...")
    def inner():
        # 在内部函数中，对已有函数进行装饰
        print("请先登录...")
        # 调用需要装饰的函数
        fn()

    return inner


# 装饰器语法糖写法
@check_login  # 等价于 submit = check_login(submit)
def submit():
    print("提交...")
