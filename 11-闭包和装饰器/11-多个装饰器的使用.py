# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 20:39
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 11-多个装饰器的使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 定义装饰器
def make_div(fn):
    def inner(*args, **kwargs):
        result = "<div>" + fn() + "</div>"
        return result

    return inner


# 定义装饰器
def make_p(fn):
    def inner(*args, **kwargs):
        result = "<p>" + fn() + "</p>"
        return result

    return inner


@make_div
@make_p
def content():
    return "人生苦短"


result = content()
print(result)
