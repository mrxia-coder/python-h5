# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 13:55
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-闭包的简单使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
闭包的作用：可以保存外部函数的变量或者参数

闭包的形成条件:
    1. 在函数嵌套(函数里面再定义函数)的前提下
    2. 内部函数使用了外部函数的变量(还包括外部函数的参数)
    3. 外部函数返回了内部函数
"""


# 定义一个外部函数
def func_out(num1):
    # 定义一个内部函数
    def func_inner(num2):
        # 内部函数使用了外部函数的变量(num1)
        result = num1 + num2
        print(f"结果是：{result}")

    # 外部函数返回了内部函数，这里返回的内部函数就是闭包
    return func_inner


# 创建闭包实例
f = func_out(1)
f(2)
f(3)
