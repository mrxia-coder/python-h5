# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 14:08
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-闭包的实例.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
需求: 根据配置信息使用闭包实现不同人的对话信息，例如对话:
张三: 到北京了吗? 李四: 已经到了，放心吧。
"""


# 外部函数接收姓名参数
def config_name(name):
    # 内部函数使用外部函数的参数，并且完成数据的拼接显示
    def inner(msg):
        print(f"{name}：{msg}")

    # 外部函数返回内部函数
    return inner


# 创建闭包实例
zs = config_name("张三")
ls = config_name("李四")
zs("到北京了吗？")
ls("已经到了，放心吧。")
