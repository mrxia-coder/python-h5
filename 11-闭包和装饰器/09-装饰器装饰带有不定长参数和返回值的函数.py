# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 17:10
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 09-装饰器装饰带有不定长参数和返回值的函数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def logging(fn):
    # 使用装饰器装饰已有函数的时候，内部函数的类型要和要装饰的函数类型保持一致
    def inner(*args, **kwargs):
        print("-- 正在努力计算 --")
        result = fn(*args, **kwargs)
        return result

    return inner


@logging
def sum_unm(*args, **kwargs):
    result = 0

    for value in args:
        result += value

    for value in kwargs.values():
        result += value

    return result


result = sum_unm(1, 2, 3, 4, 5, a=15)
print(f"计算结果是:{result}")
