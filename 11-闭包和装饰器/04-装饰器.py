# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 16:21
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-装饰器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
装饰器就是给已有函数增加额外功能的函数，它本质上就是一个闭包函数。

装饰器的功能特点:
    1. 不修改已有函数的源代码
    2. 不修改已有函数的调用方式
    3. 给已有函数增加额外的功能
"""


# 定义装饰器
# 如果闭包函数的参数有且只有一个，并且这个参数是一个函数，那么这个闭包函数就称为装饰器
def check_login(fn):
    def inner():
        # 在内部函数中，对已有函数进行装饰
        print("请先登录...")
        # 调用需要装饰的函数
        fn()

    return inner


def comment():
    print("发表评论")


# 利用装饰器来装饰函数, comment == inner
comment = check_login(comment)
comment()


# 装饰器语法糖写法
@check_login  # 等价于 submit = check_login(submit)
def submit():
    print("提交...")


comment()
