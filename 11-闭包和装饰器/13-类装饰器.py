# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/30 10:12
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 13-类装饰器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class MyDecorator(object):
    def __init__(self, fn):
        # 初始化操作在此完成
        self.__fn = fn

    # 实现__call__方法，表示对象是一个可调用对象，可以像调用函数一样进行调用。
    def __call__(self, *args, **kwargs):
        # 添加装饰功能
        print("请先登录...")
        self.__fn()


@MyDecorator
def comment():
    print("发表评论")


comment()
