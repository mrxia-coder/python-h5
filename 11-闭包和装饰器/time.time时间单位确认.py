# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 16:49
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : time.time时间单位确认.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm
import time

since = time.time()
time.sleep(5)
time_elapsed = time.time() - since
print(f"耗时{time_elapsed}秒")
