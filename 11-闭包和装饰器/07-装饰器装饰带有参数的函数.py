# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 16:54
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 07-装饰器装饰带有参数的函数.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def logging(fn):
    # 使用装饰器装饰已有函数的时候，内部函数的类型要和要装饰的函数类型保持一致
    def inner(num1, num2):
        print("-- 正在努力计算 --")
        fn(num1, num2)

    return inner


@logging
def sum_unm(num1, num2):
    result = num1 + num2
    print(f"计算结果是：{result}")


sum_unm(1, 2)
