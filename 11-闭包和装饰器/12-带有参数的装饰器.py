# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/30 10:01
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 12-带有参数的装饰器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def logging(flag):
    def decorator(fn):
        def inner(a, b):
            if flag == "+":
                print("正在进行加法运算...")
            elif flag == "-":
                print("正在进行减法运算...")
            result = fn(a, b)
            return result

        return inner
    # 返回装饰器
    return decorator


@logging("-")
def sub(a, b):
    result = a - b
    print(result)


@logging("+")
def add(a, b):
    result = a + b
    print(result)


add(1, 2)
sub(4, 2)
