# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 14:16
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-修改闭包内使用的外部变量.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 定义一个外部函数
def func_out(num1):
    # 定义一个内部函数
    def func_inner(num2):
        nonlocal num1  # 闭包函数中修改外部变量需要使用nonlocal关键字声明，告诉解释器此处使用的是外部变量num1
        num1 = 10
        # 内部函数使用了外部函数的变量(num1)
        result = num1 + num2
        print("结果是:", result)

    print(f"修改前的外部变量：{num1}")
    func_inner(1)
    print(f"修改后的外部变量：{num1}")

    # 外部函数返回了内部函数，这里返回的内部函数就是闭包
    return func_inner


# 创建闭包实例
f = func_out(1)
# 执行闭包
f(2)
