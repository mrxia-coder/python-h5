# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 16:45
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 06-装饰器统计函数执行耗时.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm
import time


# 装饰器函数
def get_run_time(fn):
    def inner():
        start = time.time()
        fn()
        end = time.time()
        print(f"函数执行完耗时：{end - start}秒")

    return inner


@get_run_time
def func():
    for i in range(10000):
        print(i)


func()
