# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/29 16:38
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-测试装饰器的执行时机.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import mydecorator


mydecorator.submit()  # 这行代码注释的时候执行一下看看输出
