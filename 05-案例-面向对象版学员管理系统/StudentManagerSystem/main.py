# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/8 22:51
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : main.py
# @Version : v1.0.0
# @Desc    : 学员管理系统入口文件
# @ide     : PyCharm


# 1. 导入managerSystem模块
from managerSystem import *


# 2. 启动学员管理系统
if __name__ == '__main__':
    # 实例化对象
    student_manager = StudentManager()
    # 启动管理系统
    student_manager.run()
