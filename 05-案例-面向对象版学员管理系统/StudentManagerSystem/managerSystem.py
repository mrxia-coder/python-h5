# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/8 22:51
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : managerSystem.py
# @Version : v1.0.0
# @Desc    : 学员管理系统管理系统文件
# @ide     : PyCharm


# 添加学员函数内部需要创建学员对象，故先导入student模块
from student import *


class StudentManager(object):
    def __init__(self):
        # 存储学员信息的列表
        self.student_list = []

    # 一. 程序入口函数，启动程序后执行的函数
    def run(self):
        # 1.加载学员信息
        self.load_student()

        while True:
            # 2.显示功能菜单
            self.show_menu()

            # 3.用户输入功能序号
            # TODO <staryjie@163.com>  需要对用户输入的内容进行判断如果不是数字类型的需要提醒用户重新输入 []
            try:
                menu_num = int(input("请输入您需要的功能序号：").strip())
            except ValueError:
                print("请输入功能列表中对应的序号！")
                continue

            # 4.根据用输入的功能序号执行不同的功能
            if menu_num == 1:
                # 添加学员
                self.add_student()
            elif menu_num == 2:
                # 删除学员
                self.del_student()
            elif menu_num == 3:
                # 修改学员信息
                self.modify_student()
            elif menu_num == 4:
                # 查询学员信息
                self.search_student()
            elif menu_num == 5:
                # 显示所有学员信息
                self.show_student()
            elif menu_num == 6:
                # 保存学员信息
                self.save_student()
            elif menu_num == 7:
                # 退出系统
                # 退出系统前，调用保存学员信息函数，防止忘记保存
                self.save_student()
                print("退出系统...")
                break
            else:
                print("没有该功能，请检查后重新输入！")

    # 二. 定义功能函数
    def load_student(self):
        """加载学员信息"""
        # 尝试以"r"模式打开数据文件，文件不存在则提示用户；文件存在（没有异常）则读取数据
        try:
            f = open("./student.db", "r")
        except:
            f = open("./student.db", "w")
        else:
            # 1.读取数据
            data = f.read()

            # 2.文件中读取的数据都是字符串且字符串内部为字典数据，故需要转换数据类型再转换字典为对象后存储到学员列表
            new_list = eval(data)
            # print(new_list)
            # for i in range(len(new_list)):
            #     print(type(new_list[i]))
            # print(new_list)
            # print(type(new_list))

            """通过列表推导式生成self.student_list，其中每个元素都是通过Student()类实例化的一个对象，
            存在列表中的对象都是Student类的对象的内存地址,如果在程序中需要使用列表中对象的某个键值对，必须通过对象.__dict__转换成字典之后才能够获取
            """
            self.student_list = [Student(stu["stu_id"], stu["name"], stu["gender"], stu["tel"]) for stu in new_list]
            # print(self.student_list)
        finally:
            # 3.关闭文件
            f.close()

    @staticmethod
    def show_menu():
        """显示功能菜单"""
        menu_info = """请选择如下功能：
        1.添加学员信息
        2.删除学员信息
        3.修改学员信息
        4.查询学员信息
        5.显示所有学员信息
        6.保存学员信息
        7.退出系统"""
        print(menu_info)

    def add_student(self):
        """添加学员"""
        # 1.用户输入学号、姓名、性别、手机号
        # print(type(self.student_list))
        # print(self.student_list)
        # print(len(self.student_list))
        stu_id = input("请输入您的学号：").strip()
        # TODO<staryjie@163.com> 需要增加对学员信息是否已经存在的判断，不存在则创建学员对象，添加到学员列表 [OK]
        if not self.check_stu_is_exists(stu_id):
            name = input("请输入您的姓名：").strip()
            gender = input("请输入您的性别（1:男  0:女）：").strip()
            tel = input("请输入您的手机号：").strip()

            # 2.创建学员对象
            # 需要先导入Student类  from student import *
            student = Student(stu_id, name, gender, tel)

            # 3.将学员信息添加到列表
            self.student_list.append(student)

            # 4.验证
            # print(self.student_list)
            print(student)
            # 自动调用保存学员信息函数，防止录入学生信息之后未保存就退出了系统
            self.save_student()
        else:
            # 学号存在，说明该学员信息已经存在，无需添加
            print("该学员已存在，请检查后重新添加！")

    def del_student(self):
        """删除学员"""
        # 1.用户输入目标学员学号
        # print(self.student_list)
        del_stu_id = input("请输入要删除的学员的学号：").strip()
        if self.check_stu_is_exists(del_stu_id):
            # 2.判断学员是否存在，存在就删除，不存在提示学员不存在
            for stu in self.student_list:
                if stu.__dict__["stu_id"] == del_stu_id:
                    self.student_list.remove(stu)
        else:
            # 在遍历列表的时候，如果学号不是输入的目标学号，就不要打印学员不存在的信息
            # if stu.stu_id == del_stu_id:
            print("没有该学员的信息！")

        # 3.验证
        print(self.student_list)

        # 自动调用保存学员信息函数，防止删除学生信息之后未保存就退出了系统
        self.save_student()

    def modify_student(self):
        """修改学员信息"""
        # 1.用户输入学员学号
        # print(self.student_list)
        modify_stu_id = input("请输入要修改信息学员的学号：").strip()

        # 2.判断学员是否存在，存在则提示用户输入对应信息进行修改，不存在提示学员不存在
        if self.check_stu_is_exists(modify_stu_id):
            for stu in self.student_list:
                if stu.__dict__["stu_id"] == modify_stu_id:
                    stu.__dict__["name"] = input("请输入学员姓名：").strip()
                    stu.__dict__["gender"] = input("请输入您的性别（1:男  0:女）：").strip()
                    stu.__dict__["tel"] = input("请输入学员手机号：").strip()
                    print(
                        f"修改学员{stu.__dict__['name']}成功！{modify_stu_id}, {stu.__dict__['name']}, {stu.__dict__['gender']}, {stu.__dict__['tel']}")
        else:
            print("没有该学员的信息！")

        # 自动调用保存学员信息函数，防止修改学生信息之后未保存就退出了系统
        self.save_student()

    def search_student(self):
        """查询学员信息"""
        # 1.提示用户输入学员学号
        search_stu_id = input("请输入要查询学员的学号：").strip()

        # 2.判断学员是否存在，存在则输出学员信息，不存在提示学员不存在
        if self.check_stu_is_exists(search_stu_id):
            for stu in self.student_list:
                if stu.__dict__["stu_id"] == search_stu_id:
                    print(f"{stu.__dict__['stu_id']}, {stu.__dict__['name']}, {stu.__dict__['gender']}, {stu.__dict__['tel']}")
        else:
            print("没有该学员的信息！")

    def show_student(self):
        """显示所有学员信息"""
        print("学号\t姓名\t性别\t手机号")
        for stu in self.student_list:
            print(f"{stu.stu_id}\t{stu.name}\t{stu.gender}\t\t{stu.tel}")

    def save_student(self):
        """保存学员信息"""
        print("正在保存数据中...")
        # 1.打开文件
        with open("./student.db", "w") as f:
            # 2.写入学员信息
            # 注意1：文件写入的数据不能是学员对象的内存地址，需要把学员数据转换成列表字典数据再做存储
            new_list = [stu.__dict__ for stu in self.student_list]
            # [{"stu_id": "10001", "name": "Jack", "gender": "1", "tel": "1318xxxx467"}]
            # print(new_list)
            # 3.保存数据
            # 注意2：文件内数据要求为字符串类型，故需要先转换数据类型为字符串才能文件写入数据
            f.write(str(new_list))
        print("数据保存成功！")

    def check_stu_is_exists(self, stu_id):
        stu_id_list = []
        # 判断用户输入的学号是否已经存在
        for i in range(len(self.student_list)):
            # print(self.student_list[i].__dict__["stu_id"])
            # print(type(self.student_list[i].__dict__["stu_id"]))
            # self.student_list[i].__dict__ 才是列表中每个对象的字典
            had_stu_id = self.student_list[i].__dict__["stu_id"]
            stu_id_list.append(had_stu_id)

        if stu_id in stu_id_list:
            return True
        else:
            return False
