# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/8 22:51
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : student.py
# @Version : v1.0.0
# @Desc    : 学员管理系统学生文件
# @ide     : PyCharm


class Student(object):
    def __init__(self, stu_id, name, gender, tel):
        self.stu_id = stu_id
        self.name = name
        # 性别通过0和1区分，1=男 0=女
        self.gender = gender
        self.tel = tel

    def __str__(self):
        return f"{self.stu_id}, {self.name}, {self.gender}, {self.tel}"


# 测试代码
if __name__ == '__main__':
    student = Student("12", "Tom", "1", "1318xxxx467")
    print(student)
