## 1、Vue概述

Vue.js是前端三大新框架：Angular.js、React.js、Vue.js之一，Vue.js目前的使用和关注程度在三大框架中稍微胜出，并且它的热度还在递增。

Vue.js读音 /vjuː/, 类似于 view

Vue.js是一个轻巧、高性能、可组件化的MVVM库，同时拥有非常容易上手的API

Vue.js是一个构建数据驱动的Web界面的库

Vue.js是一套构建用户界面的 渐进式框架

通俗的说:

- Vue.js是一个构建数据驱动的 web 界面的渐进式框架
- Vue.js 的目标是通过尽可能简单的 API 实现响应的数据绑定和组合的视图组件
- 核心是一个响应的数据绑定系统



**vue的作者**

![](http://static.staryjie.com/static/images/20200922102154.png)

尤雨溪是Vue.js框架的作者，他认为，未来App的趋势是轻量化和细化，能解决问题的应用就是好应用。而在移动互联网时代大的背景下，个人开发者的机遇在门槛低，成本低，跨设备和多平台四个方面。



**Vue.js使用文档及下载Vue.js**

Vue.js使用文档已经写的很完备和详细了，通过以下地址可以查看：https://cn.vuejs.org/v2/guide/

vue.js如果当成一个库来使用，可以通过下面地址下载：https://cn.vuejs.org/v2/guide/installation.html

可视化学习网站:https://scrimba.com/playlist/pXKqta



## 2、第一个Vue应用

参考文档：https://cn.vuejs.org/v2/guide/#%E8%B5%B7%E6%AD%A5

### 2.1 导包

官方一共提供了两个包：

* 开发环境版本
* 生产环境版本

```html
<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<!-- 生产环境版本，优化了尺寸和速度 -->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
```



### 2.2 创建Vue实例

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        {{ message }}
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue!',
            }
        })
    </script>
</body>
</html>
```

![](http://static.staryjie.com/static/images/20200922103325.png)



### 2.3 调试工具

Vue官方提供了对应的调试工具，可以根据官方文档进行安装调试：https://github.com/vuejs/vue-devtools

![](http://static.staryjie.com/static/images/20200922105402.png)



在浏览器安装插件之后，按下F12，选择`Vue`即可进行调试：

![](http://static.staryjie.com/static/images/20200922105537.png)

谷歌浏览器插件地址：https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd



## 3、data（数据）

### 3.1 基本语法

数据绑定最常见的形式就是使用“Mustache(胡子)”语法 (双大括号) 的文本插值。

```html
<a href="#">{{ message }}</a>
```

Mustache 标签将会被替代为对应数据对象上`message`属性的值。无论何时，绑定的数据对象上`message`属性发生了改变，插值处的内容都会更新。

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <a href="#">{{ message }}</a>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                message: '百度一下，你就知道',
            }
        })
    </script>
</body>
</html>
```



### 3.2 v-bind绑定元素属性

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <a href="#">{{ message }}</a>
        <hr>
        <a v-bind:href="url">戳我有惊喜</a>
        <hr>
        <span v-bind:title='showmessage'>鼠标放在这里显示当前时间</span>
        <hr>
        <!-- 简写 -->
        <span :title='showmessage'>鼠标放在这里显示当前时间</span>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                message: '百度一下，你就知道',
                url: 'https://jd.com/',
                showmessage: '当前时间是' + new Date().toLocaleString(),
            }
        })
    </script>
</body>
</html>
```

> v-bind简写：https://cn.vuejs.org/v2/guide/syntax.html#v-bind-%E7%BC%A9%E5%86%99



## 4、if条件渲染

通过条件指令可以控制元素的创建(显示)或者销毁(隐藏)。

- v-if
- v-else-if
- v-else
- v-show



### 4.1 v-if、v-esle-if、v-esle

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <span>{{ message }}</span>
        <hr>
        <a href="#" v-if="isLogin">欢迎您，</a>
        <a href="#" v-if="level === 1">倔强青铜</a>
        <a href="#" v-else-if="level === 2">秩序白银</a>
        <a href="#" v-else-if="level === 3">荣耀黄金</a>
        <a href="#" v-else-if="level === 4">尊贵铂金</a>
        <a href="#" v-else-if="level === 5">永恒钻石</a>
        <a href="#" v-else-if="level === 6">至尊星耀</a>
        <a href="#" v-else>最强王者</a>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                message: 'Hello',
                isLogin: true,
                level: 7,
            }
        })
    </script>
</body>
</html>
```

> v-if和v-else：https://cn.vuejs.org/v2/guide/conditional.html#v-else
>
> v-if、v-else-if和v-else：https://cn.vuejs.org/v2/guide/conditional.html#v-else-if



### 4.2 v-show

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <span v-if="seen">v-if</span>
        <span v-show="seen">v-show</span>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                seen: false,
            }
        })
    </script>
</body>
</html>
```

这时候打开浏览器访问，两个元素都不显示，但是`v-show`的元素是存在的，只是属性为`sidplay: none`。

![](http://static.staryjie.com/static/images/20200922112634.png)

> v-show用法和v-if大致一样，但是它不支持v-else,它和v-if的区别是，它制作元素样式的显示和隐藏，元素一直是存在的，v-if的元素如果不显示就不存在。
>
> 注意在vue中使用v-show, 原来的css代码不能设置display属性, 会导致冲突



## 5、for列表渲染

参考文档：https://cn.vuejs.org/v2/guide/list.html

`v-for`指令可以绑定数组的数据来渲染一个项目列表

`v-for`指令需要使用`item in items`形式的特殊语法，`items`是源数据数组并且`item`是数组元素迭代的别名。

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
<div id="app">
    {{ items }}
    <hr>
    <ul>
        <li v-for="item in items">{{ item }}</li>
    </ul>
</div>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            items: ['吃饭', '睡觉', '打豆豆', '淘宝', '知乎', '微博', '抖音'],
        }
    })
</script>
</body>
</html>
```



### 5.1 index

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
<div id="app">
    {{ items }}
    <hr>
    <ul>
        <li v-for="(item, index) in items">{{ index + 1 }} - {{ item }}</li>
    </ul>
    <hr>
</div>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            items: ['吃饭', '睡觉', '打豆豆', '淘宝', '知乎', '微博', '抖音'],
        }
    })
</script>
</body>
</html>
```

> 注意：index默认从0开始，解包index和item时要注意顺序，是(item, index)而不是(index, item)这点有别与Python的解包顺序。



### 5.2 遍历对象

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
<div id="app">
    {{ items }}
    <hr>
    <ul>
        <li v-for="(item, index) in items">{{ index + 1 }} - {{ item }}</li>
    </ul>
    <hr>
    <!-- 遍历对象 -->
    <ul>
        <!-- <li v-for="value in object">{{ value }}</li>-->
        <li v-for="(value,key) in object">{{ key }} : {{ value }}</li>
    </ul>
</div>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            items: ['吃饭', '睡觉', '打豆豆', '淘宝', '知乎', '微博', '抖音'],
            object: {
                title: 'How to do list in vue?',
                author: 'StaryJie',
                publishedAt: '2020-09-22',
            }
        }
    })
</script>
</body>
</html>
```

> 参考文档：https://cn.vuejs.org/v2/guide/list.html#%E4%B8%80%E4%B8%AA%E5%AF%B9%E8%B1%A1%E7%9A%84-v-for



### 5.3 遍历对象列表

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
<div id="app">
    {{ items }}
    <hr>
    <ul>
        <li v-for="(item, index) in items">{{ index + 1 }} - {{ item }}</li>
    </ul>
    <hr>
    <!-- 遍历对象 -->
    <ul>
        <!-- <li v-for="value in object">{{ value }}</li>-->
        <li v-for="(value,key) in object">{{ key }} : {{ value }}</li>
    </ul>
    <hr>
    <!-- 遍历列表对象 -->
    <ul>
        <li v-for="todo in todos">
            {{ todo.title }} - {{ todo.author }} - {{ todo,oublishedAt }}
        </li>
    </ul>
</div>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            items: ['吃饭', '睡觉', '打豆豆', '淘宝', '知乎', '微博', '抖音'],
            object: {
                title: 'How to do list in vue?',
                author: 'StaryJie',
                publishedAt: '2020-09-22',
            },
            todos: [
                {
                    title: 'Vue',
                    author: 'Jane Doe',
                    publishedAt: '2016-04-10'
                },
                {
                    title: 'python',
                    author: 'Ricky',
                    publishedAt: '2019-04-10'
                },
                {
                    title: 'java',
                    author: 'tom Doe',
                    publishedAt: '2006-05-08'
                }
            ]
        }
    })
</script>
</body>
</html>
```



## 6、methods (事件)

参考文档：https://cn.vuejs.org/v2/guide/events.html

可以用`v-on`指令监听 DOM 事件，并在触发时运行一些 JavaScript 代码。

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <button v-on:click="login">登录</button>
        <!-- v-on:click 简写 @click -->
        <a  href="javascript:;" @click="register">注册</a>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {

            },
            methods: {
                login: function () {
                    alert("登录事件!");
                },
                register: function () {
                    alert("注册事件!");
                }
            }
        })
    </script>
</body>
</html>
```



### 6.1 事件处理方法

然而许多事件处理逻辑会更为复杂，所以直接把 JavaScript 代码写在`v-on`指令中是不可行的。因此`v-on`还可以接收一个需要调用的方法名称。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <!-- 开发环境版本 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
<div id="app">
  <button v-on:click="add">Add 1</button>
</div>
</body>
<script type="text/javascript">
var app = new Vue({
  el: '#app',
  data: {
    counter:0
  },
  methods:{
    add:function(){
      counter+=1
      alert(this.counter)
    }
  }
})
</script>
</html>
```



### 6.2 事件处理方法传递参数

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <button v-on:click="login">登录</button>
        <!-- v-on:click 简写 @click -->
        <a  href="javascript:;" @click="register">注册</a>
        <hr>
        <span>{{ counter }}</span>
        <button @click="add(counter)">点击+1</button>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                counter: 1,
            },
            methods: {
                login: function () {
                    alert("登录事件!");
                },
                register: function () {
                    alert("注册事件!");
                },
                add: function (counter) {
                    // this表示当前的vue对象，可以通过this获取data中的变量
                    this.counter = this.counter + 1;
                }
            }
        })
    </script>
</body>
</html>
```



## 7、model表单绑定 (双向绑定数据)

- 单行文本框
- 多行文本框
- 单选框
- 多选框
- 下拉框

参考文档：https://cn.vuejs.org/v2/guide/forms.html



可以用`v-model`指令在表单`<input>`、`<textarea>`及`<select>`元素上创建双向数据绑定。它会根据控件类型自动选取正确的方法来更新元素。尽管有些神奇。它负责监听用户的输入事件以更新数据，并对一些极端场景进行一些特殊处理。

> `v-model`会忽略所有表单元素的`value`、`checked`、`selected`特性的初始值而总是将 Vue 实例的数据作为数据来源。你应该通过 JavaScript 在组件的`data`选项中声明初始值

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <table>
            <tr>
                <td>用户名：</td>
                <td><input type="text" name="username" id="username" v-model="username"></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><input type="password" name="password" id="password" v-model="password"></td>
            </tr>
            <tr>
                <td>重复密码：</td>
                <td><input type="password" name="repassword" id="repassword" v-model="repassword"></td>
            </tr>
        </table>
        <button>注册</button>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                username: '',
                password: '',
                repassword: '',
            }
        })
    </script>
</body>
</html>
```

效果如下：

![](http://static.staryjie.com/static/images/20200922172104.png)



## 8、todolist案例

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <input type="text" name="todoitem" v-model="newitem"><button @click="additem">添加</button>
        <hr>
        <ol>
            <li v-for="(todo, index) in todolist">
                {{ todo }}
                <a href="javascript:;" @click="delitem(index)">删除</a>
                <a href="javascript:;" @click="up(index)">上移</a>
                <a href="javascript:;" @click="down(index)">下移</a>
            </li>
        </ol>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                todolist: ['吃饭', '睡觉', '打豆豆'],
                newitem: '',
            },
            methods: {
                additem: function () {
                    if(this.newitem.length === 0){
                        alert("添加项不能为空！");
                    }else {
                        this.todolist.push(this.newitem)
                        this.newitem = '';
                    }
                },
                delitem: function (index) {
                    alert("将要删除 ["+ this.todolist[index]+"] !");
                    this.todolist.splice(index, 1);
                },
                up: function (index) {
                    // 获取当前元素
                    current = this.todolist[index];
                    // 删除当前元素
                    this.todolist.splice(index, 1);
                    // 插入当前元素到前一个索引位置
                    if((index - 1) >= 0) {
                        this.todolist.splice(index -1 , 0 , current);
                    }else {
                        this.todolist.splice(index , 0 , current);
                        alert("无法继续上移!");
                    }
                },
                down: function (index) {
                    current = this.todolist[index];
                    if((index + 1) == this.todolist.length) {
                        alert("无法继续下移!");
                    }else {
                        this.todolist.splice(index, 1);
                        this.todolist.splice(index + 1 , 0 , current);
                    }
                },
            }
        })
    </script>
</body>
</html>
```



## 9、ES6语法

ES6标准入门：http://caibaojian.com/es6/



### 9.1 ES6语法介绍

ES6是JavaScript语言的新版本，它也可以叫做ES2015，之前学习的JavaScript属于ES5，ES6在它的基础上增加了一些语法，ES6是未来JavaScript的趋势，而且vue组件开发中会使用很多的ES6的语法，所以掌握这些常用的ES6语法是必须的。



### 9.2 变量声明

**var:**它是用来声明变量的。如果在方法中声明，则为局部变量；如果在全局中声明，则为全局变量。

```javascript
var num=10
```

> 变量一定要在声明后使用，否则报错。

![](http://static.staryjie.com/static/images/20200922221739.png)

**let:**ES6新增了`let`命令，用来声明变量。它的用法类似于`var`，但是所声明的变量，只在`let`命令所在的代码块内有效。

```javascript
{
  let a = 10;
  var b = 1;
}
```

![](http://static.staryjie.com/static/images/20200922221714.png)

上面代码在代码块之中，分别用`let`和`var`声明了两个变量。然后在代码块之外调用这两个变量，结果`let`声明的变量报错，`var`声明的变量返回了正确的值。这表明，`let`声明的变量只在它所在的代码块有效。

> `for`循环的计数器，就很合适使用`let`命令。
>
> ```javascript
> for (let i = 0; i < 10; i++) {}
> 计数器i只在for循环体内有效，在循环体外引用就会报错。
> ```

**const:**`const`声明一个只读的常量。一旦声明，常量的值就不能改变。

```javascript
const PI = 3.1415;
```

![](http://static.staryjie.com/static/images/20200922221838.png)



### 9.3 Javascript对象的写法

#### 9.3.1 ES5的写法

```javascript
var person = { 
    name:'staryjie',
    age:26,
    say:function(){
        alert('hello')
    }
}

person.say()
```

还可以写成：

```javascript
var person = {};
person.name='staryjie';
person.age=26;
person.say = function (){alert('hello')}
person.say();
```



#### 9.3.2 ES6的写法

需要注意的是, 实现简写,有一个前提,必须变量名属性名一致。

```javascript
//定义变量
var name='staryjie';
var age=26;
//创建对象
var person = {
    name,
    age,
    say:function(){
        alert('hello');
    }
};
//调用
person.say()
```



### 9.4 ES6的箭头函数

作用:

- 定义函数新的方式
- 改变this的指向

**定义函数新的方式**

```javascript
//无参数,无返回值
var say = ()=> {
    alert('我是无参数无返回值函数');
}
//有参数,无返回值
var eat = food => {
    alert('我喜欢吃'+food);
}
//有参数,有返回值
var total = (num1,num2) => {
    return num1+num2;
}
```

**改变this的指向**

如果层级比较深的时候, this的指向就变成了window, 这时候就可以通过箭头函数解决这个指向的问题。

```javascript
var person = {
    name:'staryjie',
    age:26,
    say:function(){
        alert('my name is ' + this.name);
    }
}
//调用
person.say()
```



## 10、Vue实例生命周期

![](http://static.staryjie.com/static/images/20200922223236.png)

#### 各个生命周期函数的作用

- beforeCreate
    - vm对象实例化之前
- created
    - vm对象实例化之后
- beforeMount
    - vm将作用标签之前
- mounted(重要时机初始化数据使用)
    - vm将作用标签之后
- beforeUpdate
    - 数据或者属性更新之前
- updated
    - 数据或者属性更新之后

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <span>{{ message }}</span>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue',
            },
            // vue生命周期钩子函数（不在methods内）
            beforeCreate: function () {
                alert("beforeCreate");
                console.log("beforeCreate");
            },
            created: function () {
                alert("created");
                console.log("created");
            },
            beforeMount: function () {
                alert("beforeMount");
                console.log("beforeMount");
            },
            mounted: function () {
                alert("mounted");
                console.log("mounted");
            },
            beforeDestroy: function () {
                alert("beforeDestroy");
                console.log("beforeDestroy");
            },
            destroyed: function () {
                alert("destroyed");
                console.log("destroyed");
            },
        })
    </script>
</body>
</html>
```

![](http://static.staryjie.com/static/images/20200922225744.png)



## 11、axios发送ajax请求

axios官方文档：https://github.com/axios/axios



### 11.1 准备后台

```bash
# 创建项目
django-admin startproject bookmanager04
cd bookmanager04

# 创建子应用
python mamage.py startapp book
```



**项目配置**

```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'book.apps.BookConfig',  # 注册子应用
]
```

创建`templates`模板目录并配置：

```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```

本地化：

```python
LANGUAGE_CODE = 'zh-hans'

TIME_ZONE = 'Asia/Shanghai'
```

配置url：

`bookmanager04/urls.py`

```python
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('book.urls')),
]
```

`book/urls.py`

```python
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login/$', views.LoginView.as_view(), name="login"),
]
```



编写LoginView视图：

```python
from django.shortcuts import render, HttpResponse
from django.views import View


# Create your views here.


class LoginView(View):
    def get(self, request):
        return render(request, 'login.html')

    def post(self, request):
        pass

```



创建登录模板`templates/login.html`:

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>用户登录</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="app">
        <!-- Vue的大胡子语法和Django的模板语法冲突，需要修改Vue的模板语法 -->
        {# <span>{{ message }}</span>#}
        <span>[[ message ]]</span>
    </div>
    <script>
        var vm = new Vue({
            el: '#app',
            delimiters: ['[[', ']]'],
            data: {
                message: 'Hello Vue',
            }
        })
    </script>
</body>
</html>
```



启动django开发服务器，测试访问：

```bash
python manage.py runserver
```

![](http://static.staryjie.com/static/images/20200922233500.png)



### 11.2导入axios发送ajax请求

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>用户登录</title>
    <!-- 开发环境版本，包含了有帮助的命令行警告 -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- 导入axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>
<body>
<div id="app">
    <!-- Vue的大胡子语法和Django的模板语法冲突，需要修改Vue的模板语法 -->
    {# <span>{{ message }}</span>#}
    <span>[[ message ]]</span> <br>
    <button @click="login">登录(GET)</button>
    <hr>
    <button @click="login2">登录(POST)</button>
    <hr>
    <span v-if="username">欢迎您，[[ username ]]!</span>
</div>
<script>
    var vm = new Vue({
        el: '#app',
        delimiters: ['[[', ']]'], // 修改默认的{{}}为[[]]，避免和Django模板冲突
        data: {
            message: 'Hello Vue',
            username: '',
        },
        methods: {
            login: function () {
                // 这里通过axios发送ajax的GET请求
                var url = "http://127.0.0.1:8000/rece/?username=staryjie&password=123456"
                axios.get(url).then((response) => {
                    this.username = response.data.data.username;
                    console.log(this.username);
                }).catch((error) => {
                    console.log(error);
                })
            },
            login2: function () {
                // 这里通过axios发送ajax的POST请求
                var url = "http://127.0.0.1:8000/rece/?username=staryjie&password=123456"
                axios.post(url, {username: 'tony', password: '123456'}).then((response) => {
                    this.username = response.data.data.username;
                    console.log(this.username);
                }).catch((error) => {
                    console.log(error);
                })
            },
        },
    })
</script>
</body>
</html>
```

编写一个类视图处理ajax请求：

```python
class ReceiveView(View):
    def get(self, request):
        # 1.接收参数
        data = request.GET
        username = data.get('username')
        password = data.get('password')

        return JsonResponse({'data': {'username': username}})

    def post(self, request):
        data = json.loads(request.body.decode())
        username = data.get('username')
        password = data.get('password')

        return JsonResponse({'data': {'username': username}})
```

![](http://static.staryjie.com/static/images/20200923000805.gif)

> Django要注释CSRF中间件，不然发送POST请求会返回403。



