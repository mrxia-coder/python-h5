## 1、Python操作Redis数据库

### 1.1 安装python中redis包

安装Redis的有3种方式https://github.com/andymccurdy/redis-py

- 第一种：进⼊虚拟环境，联⽹安装包redis

    ```bash
    pip install redis
    ```

- 第二种：进⼊虚拟环境，联⽹安装包redis

    ```bash
    easy_install redis
    ```

- 第三种：到中⽂官⽹-客户端下载redis包的源码，使⽤源码安装

    ```bash
    wget https://github.com/andymccurdy/redis-py/archive/master.zip
    unzip master.zip
    cd redis-py-master
    sudo python setup.py install
    ```

    

### 1.2 调用模块

```python
from redis import StrictRedis
# 在redis模块的client.py中有这么一行 StrictRedis = Redis
# 所以也可以写成下面这样
from redis import Redis
```

> 这个模块中提供了`StrictRedis对象`，⽤于连接redis服务器，并按照不同类型提供 了不同⽅法，进⾏交互操作。



### 1.3 StrictRedis(Redis)对象⽅法

通过init创建对象，指定参数host、port与指定的服务器和端⼝连接，host默认为localhost，port默认为6379，db默认为0。

```python
sr = StrictRedis(host='localhost', port=6379, db=0)
# 或者
rs = Redis(host="192.168.50.176", port=6379)

# 简写
sr = StrictRedis()
# 或者
rs = Redis()
```

根据不同的类型，拥有不同的实例⽅法可以调⽤，与前⾯学的redis命令对应，⽅法需要的参数与命令的参数⼀致。

**说明：**

redis 2.x模块中的StrictRedis在redis 3.x模块中已经更名为 Reids，可以参考官方文档：https://hub.fastgit.org/andymccurdy/redis-py

![](http://static.staryjie.com/static/images/20200904131000.png)



#### 1.3.1 string

- set
- setex
- mset
- append
- get
- mget
- key



#### 1.3.2 keys

- exists
- type
- delete
- expire
- getrange
- ttl



#### 1.3.3 hash

- hset
- hmset
- hkeys
- hget
- hmget
- hvals
- hdel



#### 1.3.4 list

- lpush
- rpush
- linsert
- lrange
- lset
- lrem



#### 1.3.5 set

- sadd
- smembers
- srem



#### 1.3.6 zset

- zadd
- zrange
- zrangebyscore
- zscore
- zrem
- zremrangebyscore



## 2、Redis主从搭建

### 2.1 主从概念

- ⼀个master可以拥有多个slave，⼀个slave⼜可以拥有多个slave，如此下去，形成了强⼤的多级服务器集群架构

- master用来写数据，slave用来读数据，经统计：网站的读写比率是10:1

- 通过主从配置可以实现读写分离

    ![](http://static.staryjie.com/static/images/20200904131631.png)

- master和slave都是一个redis实例(redis服务)

![](http://static.staryjie.com/static/images/20200904132051.png)

### 2.2 主从环境搭建

#### 2.2.1 查看服务器IP

![](http://static.staryjie.com/static/images/20200904141556.png)

#### 2.2.2 修改主Redis配置文件

```bash
vim /usr/local/redis/etc/redis.conf

# 只需要修改绑定IP这一行即可
bind 192.168.50.176
```

#### 2.2.3 重启redis-server

```bash
# 停止原有redis-server
kill -9 `lsof -i:6379|awk 'NR==2{print $2}'`

# 重新启动redis-server
/usr/local/redis/bin/redis-server /usr/local/redis/etc/redis.conf

# 检查是否正常启动
lsof -i:6379
# 或者
ps -aux|grep -v grep|grep redis-server
```



#### 2.2.4 修改从Redis配置

```bash
cp /usr/local/redis/etc/redis.conf /usr/local/redis/etc/slave.conf

# 修改slave.conf配置
vim /usr/local/redis/etc/slave.conf

# 修改以下三个地方
bind 192.168.50.176
port 6378  # slave的端口不能和主Redis一样
slaveof 192.168.50.176 6379
```

#### 2.2.5 启动slave

```bash
/usr/local/redis/bin/redis-server /usr/local/redis/etc/slave.conf
```

#### 2.2.6 检查主从同步信息

```bash
/usr/local/redis/bin/redis-cli -h 192.168.50.176 info Replication
```

![](http://static.staryjie.com/static/images/20200904142557.png)



### 2.3 数据操作

#### 2.3.1 主Redis写数据

```bash
/usr/local/redis/bin/redis-cli -h 192.168.50.176 -p 6379

set aa bb
```

![](http://static.staryjie.com/static/images/20200904143308.png)

#### 2.3.2 slave端获取数据

```bash
/usr/local/redis/bin/redis-cli -h 192.168.50.176 -p 6378

get aa
```

![](http://static.staryjie.com/static/images/20200904143358.png)



## 3、Redis集群搭建

### 3.1 为什么要有集群？

* redis可以做主从复制，一主可以多从，如果同时的访问量过大(1000w),主服务肯定就会挂掉，数据服务就挂掉了或者发生自然灾难只要主Redis挂掉了，就无法进行写入数据。
* 大公司都会有很多的服务器(华东地区、华南地区、华中地区、华北地区、西北地区、西南地区、东北地区、台港澳地区机房)
* 为了防止单点故障，搭建Redis集群，通过slb或者热备的方式，可以有效的防止单点故障的发生，提高业务稳定性。



### 3.2 集群的概念

* 集群是一组相互独立的、通过高速网络互联的计算机，它们构成了一个组，并以单一系统的模式加以管理。一个客户与集群相互作用时，集群像是一个独立的服务器。集群配置是用于提高可用性和可缩放性。

    ![](http://static.staryjie.com/static/images/20200904143807.png)

    当请求到来首先由负载均衡服务器处理，把请求转发到另外的一台服务器上。



### 3.3 Redis集群

参考文档：http://doc.redisfans.com/topic/cluster-tutorial.html

#### 3.3.1 分类

* 软件层面

    * 只有一台服务器，在这一台电脑上启动了多个redis服务。

        ![](http://static.staryjie.com/static/images/20200904143954.png)

* 硬件层面

    * 在多台实体的服务器，每台服务器上都启动了一个redis或者多个redis服务。

        ![](http://static.staryjie.com/static/images/20200904144009.png)



#### 3.3.2 配置Redis集群

1. 下载Redis源码包

    ```bash
    wget http://download.redis.io/releases/redis-6.0.7.tar.gz
    ```

2. 解压源码包冰女安装

    ```bash
    tar xf redis-6.0.7.tar.gz
    cd ./redis-6.0.7
    make && make install PREFIX=/usr/local/redis
    ```

3. 生成集群需要redis服务数目的配置文件

    ```bash
    cd /usr/local/redis/conf
    cp redis.conf ./{7000, 7001, 7002, 7003, 7004, 7005}.conf
    ```

4. 根据不同的配置文件修改成不同的配置文件

    ```bash
    # 监听端口号
    port 7000
    # 绑定IP
    bind 192.168.50.176
    # 是否以守护进程的方式运行
    daemonize yes
    # pid文件
    pidfile 7000.pid
    # 是否使用集群
    cluster-enabled yes
    # 集群的文件（启动后悔自动创建）
    cluster-config-file 7000_node.conf
    # 集群超时时间
    cluster-node-timeout 15000
    # 备份相关配置
    appendonly yes
    ```

    ```bash
    port 7001
    bind 192.168.50.176
    daemonize yes
    pidfile 7001.pid
    cluster-enabled yes
    cluster-config-file 7001_node.conf
    cluster-node-timeout 15000
    appendonly yes
    ```

    其他的配置文件根据上面的配置进行相应的修改，配置区别在port、pidfile、cluster-config-file三项。

5. 根据不同的配置文件启动不同的redis-serve

    ```bash
    /usr/local/redis/bin/redis-server /usr/local/redis/conf/7000.conf
    /usr/local/redis/bin/redis-server /usr/local/redis/conf/7001.conf
    /usr/local/redis/bin/redis-server /usr/local/redis/conf/7002.conf
    /usr/local/redis/bin/redis-server /usr/local/redis/conf/7003.conf
    /usr/local/redis/bin/redis-server /usr/local/redis/conf/7004.conf
    /usr/local/redis/bin/redis-server /usr/local/redis/conf/7005.conf
    ```

6. 检查是否正常启动即可

    ```bash
    ps -ef|grep redis-server
    ```



#### 2.3.3 创建集群

在redis源码包中包含了`redis-trib.rb`命令，用于创建集群。

1. 将redis-trib.rb命令加入环境变量

    ```bash
    cp ./redis-6.0.7/src/redis-trib.rb /usr/local/bin/
    ```

2. 安装ruby环境

    ```bash
    yum install -y ruby
    ```

3. 创建集群

    ```bash
    # 创建一注意从三个节点，一共6个Redis服务，redis-trib.rb会自动分配三个master和三个slave
    redis-trib.rb create --replicas 1 192.168.50.176:7000 192.168.50.176:7001 192.168.50.176:7002 192.168.50.176:7003 192.168.50.176:7004 192.168.50.176:7005
    ```

    > 说明：
    >
    > 1. 如果在不同的服务器上，那么只需要修改成对应的服务器IP和端口号即可。
    >
    > 2. 上述命令可能会报错，导致无法创建集群，可能是因为ruby不是最新最新版本的，需要通过修改gem源来获取最新版
    >
    >     ```bash
    >     # 先查看⾃⼰的 gem 源是什么地址
    >     gem source -l # 如果是https://rubygems.org/ 就需要更换
    >     
    >     # 更换指令为
    >     gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/
    >     
    >     # 通过 gem 安装 redis 的相关依赖
    >     gem install redis
    >     
    >     # 然后重新执⾏上述创建集群的指令
    >     ```
    >
    > 3. 如果提示`Can I set the above configuration?`输入yes即可。
    >
    > 4. 给定 `redis-trib.rb` 程序的命令是 `create` ， 这表示我们希望创建一个新的集群。
    >
    > 5. 选项 `--replicas 1` 表示我们希望为集群中的每个主节点创建一个从节点。
    >
    > 6. 之后跟着的其他参数则是实例的地址列表， 我们希望程序使用这些地址所指示的实例来创建新集群。

4. 集群搭建完成



#### 2.3.4 数据验证

1. 在随便一个redis中写入数据

    ```bash
    /usr/local/redis/bin/redis-cli -h 192.168.50.176 -c -p 7001
    
    set age 40
    ```

2. 然后从另一个redis中获取数据

    ```bash
    /usr/local/redis/bin/redis-cli -h 192.168.50.176 -c -p 7004
    
    get age
    ```

    

#### 3.4 在哪个服务器上写数据？（Redis的CRC16算法）

* edis cluster在设计的时候，就考虑到了去中⼼化，去中间件，也就是说，集群中 的每个节点都是平等的关系，都是对等的，每个节点都保存各⾃的数据和整个集 群的状态。每个节点都和其他所有节点连接，⽽且这些连接保持活跃，这样就保 证了我们只需要连接集群中的任意⼀个节点，就可以获取到其他节点的数据。
* Redis集群没有并使⽤传统的⼀致性哈希来分配数据，⽽是采⽤另外⼀种叫做哈希 槽 (hash slot)的⽅式来分配的。redis cluster 默认分配了 16384 个slot，当我们 set⼀个key 时，会⽤CRC16算法来取模得到所属的slot，然后将这个key 分到哈 希槽区间的节点上，具体算法就是：CRC16(key) % 16384。当我们在某一个节点设置key value的时候，设置成功之后会有提示信息告诉你这个数据存放在了哪个节点上。
* Redis 集群会把数据存在⼀个 master 节点，然后在这个 master 和其对应的salve 之间进⾏数据同步。当读取数据时，也根据⼀致性哈希算法到对应的 master 节 点获取数据。只有当⼀个master 挂掉之后，才会启动⼀个对应的 salve 节点，充 当 master。
* 需要注意的是：必须要3个或以上的主节点，否则在创建集群时会失败，并且当存 活的主节点数⼩于总节点数的⼀半时，整个集群就⽆法提供服务了。



## 4、Python操作Redis集群

### 4.1 安装Python操作Redis集群的模块

```bash
pip install redis-py-cluster
```

> redis-py-cluster源码地址https://github.com/Grokzen/redis-py-cluster



### 4.2 Python代码操作Redis集群

```bash
from rediscluster import *

if __name__ == '__main__':
    try:
        # 构建所有的节点，Redis会使⽤CRC16算法，将键和值写到某个节点上
        startup_nodes = [
            {'host': '192.168.50.176', 'port': '7000'},
            {'host': '192.168.50.176', 'port': '7003'},
            {'host': '192.168.50.176', 'port': '7001'},
        ]
        # 构建StrictRedisCluster对象
        src = RedisCluster(startup_nodes=startup_nodes, decode_responses=True)
        # 设置键为name、值为staryjie的数据
        result = src.set('name', 'staryjie')
        print(result)
        # 获取键为name
        name = src.get('name')
        print(name)
    except Exception as e:
        print(e)
```

