## 1、NoSQL和Redis

### 1.1 nosql介绍

#### NoSQL：一类新出现的数据库(not only sql)

- 泛指非关系型的数据库
- 不支持SQL语法
- 存储结构跟传统关系型数据库中的那种关系表完全不同，nosql中存储的数据都是KV形式
- NoSQL的世界中没有一种通用的语言，每种nosql数据库都有自己的api和语法，以及擅长的业务场景
- NoSQL中的产品种类相当多：
    - Redis
    - Mongodb
    - Hbase hadoop
    - Cassandra hadoop



#### NoSQL和SQL数据库的比较：

- 适用场景不同：sql数据库适合用于关系特别复杂的数据查询场景，nosql反之
- **事务** 特性的支持：sql对事务的支持非常完善，而nosql基本不支持事务
- 两者在不断地取长补短，呈现融合趋势



### 1.2 Redis简介

- Redis是一个开源的使用ANSI C语言编写、支持网络、可基于内存亦可持久化的日志型、Key-Value数据库，并提供多种语言的API。从2010年3月15日起，Redis的开发工作由VMware主持。从2013年5月开始，Redis的开发由Pivotal赞助。
- Redis是 NoSQL技术阵营中的一员，它通过多种键值数据类型来适应不同场景下的存储需求，借助一些高层级的接口使用其可以胜任，如缓存、队列系统的不同角色



### 1.3 Redis特性

- Redis 与其他 key - value 缓存产品有以下三个特点：
    - Redis支持数据的持久化，可以将内存中的数据保存在磁盘中，重启的时候可以再次加载进行使用。
    - Redis不仅仅支持简单的key-value类型的数据，同时还提供list，set，zset，hash等数据结构的存储。
    - Redis支持数据的备份，即master-slave模式的数据备份。



### 1.4 Redis 优势

- 性能极高 – Redis能读的速度是110000次/s,写的速度是81000次/s 。
- 丰富的数据类型 – Redis支持二进制案例的 Strings, Lists, Hashes, Sets 及 Ordered Sets 数据类型操作。
- 原子 – Redis的所有操作都是原子性的。
- 丰富的特性 – Redis还支持 publish/subscribe, 通知, key 过期等等特性。



### 1.5 Redis应用场景

- 用来做缓存(ehcache/memcached)——redis的所有数据是放在内存中的（内存数据库）
- 可以在某些特定应用场景下替代传统数据库——比如社交类的应用
- 在一些大型系统中，巧妙地实现一些特定的功能：session共享、购物车
- 只要你有丰富的想象力，redis可以用在可以给你无限的惊喜…….



### 1.6 推荐阅读

- [redis官方网站](https://redis.io/)
- [redis中文官网](http://redis.cn/)



## 2、Redis安装

- 

- 当前redis最新稳定版本是6/0/7

- 当前ubuntu虚拟机中已经安装好了redis，以下步骤可以跳过 最新稳定版本下载链接： http://download.redis.io/releases/redis-6.0.7.tar.gz

- step1:下载

    ```bash
    wget http://download.redis.io/releases/redis-6.0.7.tar.gz
    ```

- step2:解压

    ```bash
    tar xf redis-6.0.7.tar.gz
    ```

- step3:进入源码目录

    ```bash
    cd ./redis-6.0.7
    ```

- step4:查看文件

    ```bash
    ls -l
    ```

- step5:make

    ```bash
    make
    ```

- step6: 测试

    ```bash
    make test
    ```

- step7:安装

    ```bash
    make install PREFIX=/usr/local/redis
    
    # 其实这里和前面的几个步骤可以合并一次性执行
    make && make install PREFIX=/usr/local/redis
    ```

- step8:安装完成之后查看

    ```bash
    ls -l /usr/local/redis/
    ```

    - redis-server redis服务器
    - redis-cli redis命令行客户端
    - redis-benchmark redis性能测试工具
    - redis-check-aof AOF文件修复工具
    - redis-check-rdb RDB文件检索工具

- step9:配置?件，移动到`/etc/`?录下

    - 配置?件?录为`/usr/local/redis/redis.conf`

    ```bash
    mkdir -p /etc/redis/
    cp ./redis.conf /etc/redis/
    ```
    



**其他系统安装：**可以根据官网文档或者百度查找相关资料即可，Redis的安装还是很方便的。



## 3、配置Redis

#### 配置

- Redis的配置信息在`/etc/redis/redis.conf`下。

- 查看

    > sudo vi /etc/redis/redis.conf

#### 核心配置选项

- 绑定ip：如果需要远程访问，可将此?注释，或绑定?个真实ip

    > bind 127.0.0.1

- 端?，默认为6379

    > port 6379

- 是否以守护进程运?

    - 如果以守护进程运?，则不会在命令?阻塞，类似于服务
    - 如果以?守护进程运?，则当前终端被阻塞
    - 设置为yes表示守护进程，设置为no表示?守护进程
    - 推荐设置为yes

    > daemonize yes

- 数据?件

    > dbfilename dump.rdb

- 数据?件存储路径

    > dir /var/lib/redis

- ?志?件

    > logfile "/var/log/redis/redis-server.log"

- 数据库，默认有16个

    > database 16

- 主从复制，类似于双机备份。

    > slaveof



## 4、服务端和客户端命令

### 4.1 服务器端

- 服务器端的命令为redis-server

- 可以使?help查看帮助?档

    > redis-server --help

- 个人习惯

    > ps aux | grep redis 查看redis服务器进程
    > sudo kill -9 pid 杀死redis服务器
    > sudo redis-server /etc/redis/redis.conf 指定加载的配置文件



### 4.2 客户端

- 客户端的命令为redis-cli

- 可以使?help查看帮助?档

    > redis-cli --help

- 连接redis

    > redis-cli

- 运?测试命令

    > ping

- 切换数据库

- 数据库没有名称，默认有16个，通过0-15来标识，连接redis默认选择第一个数据库

    > select 10



## 5、数据操作

### 5.1 参考文档

- [Redis 参考命令](http://doc.redisfans.com/)
- [Redis 官方文档](https://redis-py.readthedocs.io/en/latest/#indices-and-tables)



### 5.2 数据结构

- redis是key-value的数据结构，每条数据都是?个键值对

- 键的类型是字符串

- 注意：键不能重复

    ![](http://static.staryjie.com/static/images/20200902165515.png)

- 值的类型分为五种：

    - 字符串string
    - 哈希hash
    - 列表list
    - 集合set
    - 有序集合zset



### 5.3 数据操作行为

- 保存
- 修改
- 获取
- 删除

点击中?官?查看命令?档http://redis.cn/commands.html



## 6、Redis通用命令

#### 6.1 keys

遍历所有的key。

keys命令一般不在生产环境中使用。因为redis是单线程的而且生产环境中key会很多，该命令效率很低。可以在热备从节点使用，或者使用scan命令代替。

```bash
keys *
```

![](http://static.staryjie.com/static/images/20200817170551.png)

![](http://static.staryjie.com/static/images/20200817171306.png)

#### 6.2 dbsize

计算数据库的大小。

![](http://static.staryjie.com/static/images/20200817170702.png)

#### 6.3 exists key

判断某个key是否存在。

```bash
# exists [key名称]，如果存在返回1，不存在返回0
exists hello
```

![](http://static.staryjie.com/static/images/20200817170901.png)

#### 6.4 del key

通过制定key值删除一个或者多个键值对。

```bash
del key
del key1 key2 key3 ...
```

![](http://static.staryjie.com/static/images/20200817171836.png)

#### 6.5 expire key seconds

设置key的过期时间，单位是秒。

```bash
# 设置key在seconds秒后过期
expire key seconds

# 查询key剩余的过期时间
ttl key

# 去掉key的过期时间
persist key
```

![](http://static.staryjie.com/static/images/20200817172630.png)

![](http://static.staryjie.com/static/images/20200817172807.png)

#### 6.6 type key

查询数据类型。

```bash
type key
```

![](http://static.staryjie.com/static/images/20200817172841.png)

#### 6.7 常用命令的时间复杂度

![](http://static.staryjie.com/static/images/20200817172926.png)





## 7、数据类型

![](http://static.staryjie.com/static/images/20200902165711.png)

### 7.1 string

字符串类型是 Redis 中最为基础的数据存储类型，它在 Redis 中是二进制安全的，这便意味着该类型可以接受任何格式的数据，如JPEG图像数据或Json对象描述信息等。在Redis中字符串类型的Value最多可以容纳的数据长度是512M。

#### 7.1.1 设置键值对

如果设置的键不存在则为添加，如果设置的键已经存在则修改。

* 设置键值

    ```bash
    set key value
    ```

    ![](http://static.staryjie.com/static/images/20200902170105.png)

* 设置键值及过期时间，单位是秒

    ```bash
    setex key seconds value
    ```

    ![](http://static.staryjie.com/static/images/20200902170921.png)

* 设置多个键值

    ```bash
    set key1 value1 key2 value2 key3 value3 ...
    ```

    ![](http://static.staryjie.com/static/images/20200902171055.png)

* 追加值

    ```bash
    append key value
    ```

    ![](http://static.staryjie.com/static/images/20200902171155.png)

#### 7.1.2 获取值

* 获取：根据键获取值，如果不存在此键则返回`nil`

    ```bash
    get key
    ```

* 根据多个键获取多个值

    ```bash
    mget a1 a2 a3 a4 a5
    ```



#### 7.1.3 删除键值对

* 直接通过del key删除对应的键值对

    ```bash
    del name
    ```

    ![](http://static.staryjie.com/static/images/20200902171514.png)



### 7.2 keys

* 查找键，参数?持正则表达式

    ```bash
    keys na*
    keys a*
keys *
    ```
    
    ![](http://static.staryjie.com/static/images/20200902172851.png)



* 判断键是否存在，如果存在返回`1`，不存在返回`0`

    ```bash
    exists key key_name
    # 或者
    exists key_name
    ```

    ![](http://static.staryjie.com/static/images/20200902173144.png)

* 查看键对应的`value`的类型

    ```bash
    type key_name
    ```

    ![](http://static.staryjie.com/static/images/20200902173348.png)

* 设置过期时间，以秒为单位

* 如果没有指定过期时间则?直存在，直到使?`DEL`移除

    ```bash
    expire key_name expire_seconds
    ```

    ![](http://static.staryjie.com/static/images/20200902173530.png)
    
    > 说明：这里是通过key名设置已经存在的键值对的过期时间，前面是在设置键值对的时候指定过期时间。
    
* 查看有效时间，以秒为单位

    ```bash
    ttl key_name
    ```

    ![](http://static.staryjie.com/static/images/20200902173858.png)





### 7.3 hash

#### 7.3.1 hash类型

- **hash**?于存储对象，对象的结构为属性、值
- **值**的类型为**string**



#### 7.3.2 增加、修改

* 设置单个属性

    ```bash
    hset key field value
    ```

    ![](http://static.staryjie.com/static/images/20200902175511.png)

* 设置多个属性

    ```bash
    hmset key field1 value1 field2 value2
    ```

    ![](http://static.staryjie.com/static/images/20200902175622.png)

* 修改属性(跟设置hash类型一样，如果field存在就是修改值)

    ```bash
    hset key field value
    ```
    
    ![](http://static.staryjie.com/static/images/20200902175815.png)



#### 7.3.3 获取

* 获取指定键所有的属性

    ```bash
    hkeys key
    ```

    ![](http://static.staryjie.com/static/images/20200902175923.png)

* 获取一个属性的值

    ```bash
    hget key field
    ```

    ![](http://static.staryjie.com/static/images/20200902180008.png)

* 获取多个属性的值

    ```bash
    hmget key field1 field2 field3 ...
    ```

    ![](http://static.staryjie.com/static/images/20200902180052.png)

* 获取所有属性的值

    ```bash
    hvals key
    ```

    ![](http://static.staryjie.com/static/images/20200902180149.png)

#### 7.3.4 删除

- **删除整个hash键及值，使用del命令**

    ```bash 
    del key
    ```

- 删除某个属性，属性对应的值会被一起删除

    ```bash
    hdel key field
    ```

    ![](http://static.staryjie.com/static/images/20200902180339.png)

- 可能出现的错误

    > MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on disk. Commands that may modify the data set are disabled. Please check Redis logs for details about the error.
    >
    > Redis被配置为保存数据库快照，但它目前不能持久化到硬盘。用来修改集合数据的命令不能用

    - 原因：
        - 强制关闭Redis快照导致不能持久化。
    - 解决方案:
        - 运行config set stop-writes-on-bgsave-error no　命令后，关闭配置项stop-writes-on-bgsave-error解决该问题。



### 7.4 list

- 列表的元素类型为string
- 按照插入顺序排序

#### 7.4.1 增加

* 在左侧插入数据

    ```bash
    lpush key value1 value2 ...
    ```

    ![](http://static.staryjie.com/static/images/20200902222942.png)

* 在右侧插入数据

    ```bash
    rpush value1 value2 ...
    ```

    ![](http://static.staryjie.com/static/images/20200902223047.png)

* 在指定元素的前或后插入新元素

    ```bash
    linsert key before或after 现有元素 新元素
    ```
    
    ![](http://static.staryjie.com/static/images/20200902224339.png)




#### 7.4.2 获取

* 返回列表里指定范围内的元素
    * `start`、`stop`为元素的下标索引
    * 索引从左侧开始，第一个元素为0
    * 索引可以是负数，表示从尾部开始计数，如`-1`表示最后一个元素

    ```bash
    lrange 0 -1
    ```

    ![](http://static.staryjie.com/static/images/20200902223156.png)
    



#### 7.4.3 设置指定索引位置的元素值

- 索引从左侧开始，第一个元素为0

- 索引可以是负数，表示尾部开始计数，如`-1`表示最后一个元素

    ```bash
    lset key index value
    ```

    ![](http://static.staryjie.com/static/images/20200902223406.png)
    
    ![](http://static.staryjie.com/static/images/20200902223425.png)
    
    



#### 7.4.4 删除

* 删除指定元素
    * 将列表中前`count`次出现的值为`value`的元素移除
    * count > 0: 从头往尾移除
    * count < 0: 从尾往头移除
    * count = 0: 移除所有和value值相等的

    ```bash
    lrem key count value
    ```

    ![](http://static.staryjie.com/static/images/20200902223641.png)



### 7.5 set

- 无序集合
- 元素为string类型
- 元素具有唯?性，不重复
- 说明：对于集合没有修改操作



#### 7.5.1 增加

* 添加元素

    ```bash
    sadd key member1 member2 ...
    ```
    
    ![](http://static.staryjie.com/static/images/20200902224905.png)



#### 7.5.2 获取

* 返回所有的元素

    ```bash
    smembers key
    ```

    ![](http://static.staryjie.com/static/images/20200902224928.png)
    
    

#### 7.5.3 删除

* 删除指定元素

    ```bash
    srem key member
    ```

    ![](http://static.staryjie.com/static/images/20200902225034.png)



### 7.6 zset

- sorted set，有序集合
- 元素为string类型
- 元素具有唯一性，不重复
- 每个元素都会关联一个double类型的score，表示权重，通过权重将元素从小到大排序
- 说明：没有修改操作



#### 7.6.1 增加

* 添加

    ```bash
    zadd key score1 member1 score2 member2 ...
    ```
    
    ![](http://static.staryjie.com/static/images/20200902225519.png)



#### 7.6.2 获取

* 返回指定范围内的元素

* start、stop为元素的下标索引

* 索引从左侧开始，第一个元素为0

* 索引可以是负数，表示从尾部开始计数，如`-1`表示最后一个元素

    ```bash
    zrange key start stop
    ```

    ![](http://static.staryjie.com/static/images/20200902225602.png)
    
* 返回`score`值在`min`和`max`之间的成员

    ```bash
    zrangebyscore key min max
    ```

    ![](http://static.staryjie.com/static/images/20200902225658.png)

* 返回成员`member`的`score`值

    ```bash
    zscore key member
    ```

    ![](http://static.staryjie.com/static/images/20200902225740.png)

    

#### 7.6.3 删除

* 删除指定元素

    ```bash
    zrem key member1 member2 ...
    ```

    ![](http://static.staryjie.com/static/images/20200902225838.png)

* 删除权重在指定范围的元素

    ```bash
    zremrangebyscore key min max
    ```

    ![](http://static.staryjie.com/static/images/20200902225924.png)

    



**Redis指令参考文档：http://doc.redisfans.com/**