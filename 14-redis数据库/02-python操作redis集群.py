# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/4 15:39
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-python操作redis集群.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


from rediscluster import *

if __name__ == '__main__':
    try:
        # 构建所有的节点，Redis会使⽤CRC16算法，将键和值写到某个节点上
        startup_nodes = [
            {'host': '192.168.50.176', 'port': '7000'},
            {'host': '192.168.50.176', 'port': '7003'},
            {'host': '192.168.50.176', 'port': '7001'},
        ]
        # 构建StrictRedisCluster对象
        src = RedisCluster(startup_nodes=startup_nodes, decode_responses=True)
        # 设置键为name、值为staryjie的数据
        result = src.set('name', 'staryjie')
        print(result)
        # 获取键为name
        name = src.get('name')
        print(name)
    except Exception as e:
        print(e)
