# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/4 12:42
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-python连接Redis数据库.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 1.导入包
import redis

if __name__ == '__main__':
    # 2.创建客户端连接实例
    try:
        rs = redis.Redis(host="192.168.50.176", port=6379)
    except Exception as e:
        print(e)

    # 3.操作string
    # 添加  set key value
    # result = rs.set("name", "StaryJie")
    # print(result)

    # 获取
    # name = rs.get("name")
    # print(name)
    # name = name.decode("utf-8")
    # print(name)

    # 修改
    # result = rs.set("name", "Tom")
    # print(result)
    # name = rs.get("name")
    # print(name)

    # 删除
    # result = rs.delete("name")
    # print(result)
    # name = rs.get("name")
    # print(name)

    # 获取键
    result = rs.keys()
    print(result)
