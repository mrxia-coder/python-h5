# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:21
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 09-异常的finally.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


try:
    f = open('test.txt', 'r')
except Exception as result:
    f = open('test.txt', 'w')
else:
    print('没有异常，真开心')
finally:
    f.close()  # 不论是否有异常发生，这里的代码都会执行
