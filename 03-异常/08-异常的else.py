# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:21
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 08-异常的else.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


try:
    print(1)
    # print(num)
except Exception as result:
    print(result)
else:
    print('我是else，是没有异常的时候执行的代码')
