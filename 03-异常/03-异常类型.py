# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:05
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-异常类型.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


print(num)  # NameError

print(10/0)  # ZeroDivisionError