# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:26
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 10-异常的传递.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
需求：
​	1. 尝试只读方式打开test.txt文件，如果文件存在则读取文件内容，文件不存在则提示用户即可。
 	2. 读取内容要求：尝试循环读取内容，读取过程中如果检测到用户意外终止程序，则`except`捕获异常并提示用户。
"""
import time

try:
    f = open('test.txt', 'r')
    try:
        while True:
            conetnt = f.readline()
            if len(conetnt) == 0:
                break
            time.sleep(2)
            print(conetnt, end='')
    except:
        # 命令行中Ctrl + C取消程序执行
        print('意外终止了读取数据')
    finally:
        f.close()
        print('关闭文件')
except:
    print('文件不存在!')
