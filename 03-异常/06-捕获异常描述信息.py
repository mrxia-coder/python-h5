# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:12
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 06-捕获异常描述信息.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


try:
    print(num/0)
except (NameError, ZeroDivisionError) as result:
    print(f"异常捕获成功!异常信息是：{result}")
