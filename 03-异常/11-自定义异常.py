# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:34
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 11-自定义异常.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
需求：密码长度不足，则报异常（用户输入密码，如果输入的长度不足3位，则报错，即抛出自定义异常，并捕获该异常）。
"""


# 自定义异常类，继承Exception
class ShortInputError(Exception):
    def __init__(self, length, min_len):
        self.length = length
        self.min_len = min_len

    # 设置异常提示信息
    def __str__(self):
        return f"您输入的密码长度是{self.length}，不能少于最少长度{self.min_len}!"


def main():
    try:
        password = input("请输入你的密码：").strip()
        if len(password) < 3:
            raise ShortInputError(len(password), 3)
    except Exception as result:
        print(result)
    else:
        print(f"您输入的密码是{password}")


main()
