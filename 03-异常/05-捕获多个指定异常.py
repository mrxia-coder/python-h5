# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:11
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-捕获多个指定异常.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


try:
    print(num/0)
except (NameError, ZeroDivisionError):
    print("异常捕获成功!")
