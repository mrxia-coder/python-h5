# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 22:01
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-体验异常.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


try:
    open('test.txt', 'r')
except:
    open('test.txt', 'w')
