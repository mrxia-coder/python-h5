## 1、了解异常

当检测到一个错误时，解释器就无法继续执行了，反而出现了一些错误的提示，这就是所谓的"异常"。

例如：以`r`模式打开一个不存在的文件：

```python
open('test.txt', 'r')
```

![](http://static.staryjie.com/static/images/20200805214910.png)



## 2、异常的写法

### 2.1 语法

```python
try:
    可能会发生错误的代码
except:
    如果发现异常要执行的代码
```

### 2.2 快速体验

```python
try:
    f = open('test.txt', 'r')  # 可能发开一个不存在的文件
except:
    f = open('test.txt', 'w')  # 如果'r'模式打开异常了，则用'w'模式打开
```

### 2.3 捕获指定异常

![](http://static.staryjie.com/static/images/20200805220613.png)

#### 2.3.1 语法

```python
try:
    可能发生错误的代码
except 异常类型:
    如果捕获到该异常类型执行的代码
```

#### 2.3.2 快速体验

```python
try:
    print(num)
except NameError:
    print("异常捕获成功!")
```

> 注意：
>
> 1. 如果尝试执行的代码的异常类型和要捕获的异常类型不一致，则无法捕获异常。
> 2. 一般try下方只放一行尝试执行的代码。

#### 2.3.3 捕获多个指定异常

```python
try:
    print(num/0)
except (NameError, ZeroDivisionError):
    print("异常捕获成功!")
```

#### 2.3.4 捕获异常描述信息

```python
try:
    print(num/0)
except (NameError, ZeroDivisionError) as result:  # result就是异常的详细信息
    print(f"异常捕获成功!异常信息是：{result}")
```

#### 2.3.5 捕获所有异常

Exception是所有程序异常类的父类。如果实在不知道代码会发生的异常会是什么，可以使用该异常进行捕捉，但是一般不推荐使用。

```python
try:
    print(num/0)
except Exception as result:
    print(f"异常捕获成功!异常信息是：{result}")
```

### 2.4 异常的else

else表示的是如果没有异常要执行的代码。

```python
try:
    print(1)  # 1 - 如果这里没有异常
    # print(num)
except Exception as result:
    print(result)  # 2 - 这里不执行
else:
    print('我是else，是没有异常的时候执行的代码')  # 3 - 这里执行
```

### 2.5 异常的finally

finally表示的是无论是否异常都要执行的代码，例如关闭文件。

```python
try:
    f = open('test.txt', 'r')
except Exception as result:
    f = open('test.txt', 'w')
else:
    print('没有异常，真开心')
finally:
    f.close()
```



## 3、异常的传递

体验异常传递

需求：

​	1. 尝试只读方式打开test.txt文件，如果文件存在则读取文件内容，文件不存在则提示用户即可。

	2. 读取内容要求：尝试循环读取内容，读取过程中如果检测到用户意外终止程序，则`except`捕获异常并提示用户。

```python
import time

try:
    f = open('test.txt', 'r')
    try:
        while True:
            conetnt = f.readline()
            if len(conetnt) == 0:
                break
            time.sleep(2)
            print(conetnt, end='')
    except:
        print('意外终止了读取数据')
    finally:
        f.close()
        print('关闭文件')
except:
    print('文件不存在!')
```

![](http://static.staryjie.com/static/images/20200805223307.png)



## 4、自定义异常

在Python中，抛出自定义异常的语法为` raise 异常类对象`。

需求：密码长度不足，则报异常（用户输入密码，如果输入的长度不足3位，则报错，即抛出自定义异常，并捕获该异常）。

```python
# 自定义异常类，继承Exception
class ShortInputError(Exception):
    def __init__(self, length, min_len):
        self.length = length
        self.min_len = min_len

    # 设置异常提示信息
    def __str__(self):
        return f"您输入的密码长度是{self.length}，不能少于最少长度{self.min_len}!"


def main():
    try:
        password = input("请输入你的密码：").strip()
        if len(password) < 3:
            raise ShortInputError(len(password), 3)
    except Exception as result:
        print(result)
    else:
        print(f"您输入的密码是{password}")


main()
```

