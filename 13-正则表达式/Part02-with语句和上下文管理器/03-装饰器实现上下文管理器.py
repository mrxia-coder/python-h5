# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/31 16:22
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-装饰器实现上下文管理器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 导入装饰器
from contextlib import contextmanager


# 装饰器装饰函数，让其成为一个上下文管理器
@contextmanager
def my_open(path, mode):
    try:
        file = open(path, mode)

        # yield关键字之前的代码可以认为是上文方法，负责返回操作对象资源
        yield file
    except Exception as e:
        print(e)
    finally:
        # yield关键字之后的代码可以认为是下文方法，负责释放操作对象资源
        file.close()


with my_open("1.txt", "w") as f:
    f.write("Hello contextmanager")
