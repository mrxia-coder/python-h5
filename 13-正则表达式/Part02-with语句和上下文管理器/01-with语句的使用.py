# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/31 9:17
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-with语句的使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


try:
    # 1、以读的方式打开文件
    f = open("1.txt", "r")
    # 2、读取文件内容
    f.write("xxxxx")

except IOError as e:
    print("文件操作出错", e)

finally:
    # 3、关闭文件
    f.close()

# 为了简化读取和写入文件的操作，python提供了with语句这种写法，既简单又安全
# 当with语句执行完成，会自动关闭文件句柄，即使有报错，也会自动关闭

# 1、以写的方式打开文件
with open("1.txt", "w") as f:
    # 2、读取文件内容
    f.write("hello world")
