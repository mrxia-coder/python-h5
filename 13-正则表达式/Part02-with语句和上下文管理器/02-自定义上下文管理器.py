# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/31 9:24
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-自定义上下文管理器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 一个类只要实现了`__enter__()和__exit__()`这个两个方法，通过该类创建的对象我们就称之为上下文管理器。

class File(object):
    def __init__(self, file_name, file_mode):
        self.file_name = file_name
        self.file_mode = file_mode

    def __enter__(self):
        # 上文方法，负责返回操作对象资源，比如：文件对象，数据库连接对象等
        print("上文方法执行...")
        self.file = open(self.file_name, self.file_mode)

        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("下文方法执行...")
        self.file.close()


# with语句结合上下文管理器使用
with File("1.txt", "r") as file:
    file_data = file.read()
    print(file_data)
