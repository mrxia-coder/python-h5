# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/1 20:49
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-生成器的创建方式-生成器推导式.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 根据程序员制定的规则循环生成数据，当条件不成立时则生成数据结束。数据不是一次性全部生成处理，而是使用一个，再生成一个，可以节约大量的内存。

# 与列表推导式类似，只不过生成器推导式使用小括号
my_generator = (i * 2 for i in range(5))
print(my_generator)

# value = next(my_generator)
# print(value)
# value = next(my_generator)
# print(value)
# value = next(my_generator)
# print(value)
# value = next(my_generator)
# print(value)
# value = next(my_generator)
# print(value)

# 当生成器没有值时，会抛出StopIteration异常，表示生成器生成数据完毕
# value = next(my_generator)
# print(value)

for value in my_generator:
    print(value)
