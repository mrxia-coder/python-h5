# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/1 21:14
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-浅拷贝-不可变类型.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# copy函数是浅拷贝，只对可变类型的第一层对象进行拷贝，对拷贝的对象开辟新的内存空间进行存储，不会拷贝对象内部的子对象。
import copy  # 使用浅拷贝需要导入copy模块

# 不可变类型有: 数字、字符串、元组
a1 = 123123
b1 = copy.copy(a1)  # 使用copy模块里的copy()函数就是浅拷贝了
# 查看内存地址
print(id(a1))
print(id(b1))

print("-" * 15)
a2 = "abc"
b2 = copy.copy(a2)
# 查看内存地址
print(id(a2))
print(id(b2))

print("-" * 15)
a3 = (1, 2, ["hello", "world"])
b3 = copy.copy(a3)
# 查看内存地址
print(id(a3))
print(id(b3))

# 不可变类型进行浅拷贝不会给拷贝的对象开辟新的内存空间，而只是拷贝了这个对象的引用。
# 对于不可变类型来说，浅拷贝实际上是对引用的拷贝，浅拷贝前后两个变量指向同一个内存地址


