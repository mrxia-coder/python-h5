# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/1 20:57
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-生成器创建方式-yield关键字.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def my_generator(n):
    for i in range(n):
        print(f"开始生成第{i + 1}个数据...")
        # 当程序执行到yield关键字的时候，代码会暂停，并把结果返回，再次获取生成器数据的时候会从暂停的地方继续往下执行
        yield i
        print(f"第{i + 1}个数据生成完毕。")


result = my_generator(3)
print(result)

# value = next(result)
# print(value)
# value = next(result)
# print(value)

# while True:
#     try:
#         value = next(result)
#         print(value)
#     except StopIteration as e:
#         break

for value in result:
    print(value)
