# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/1 21:05
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-生成器的应用-斐波那契数列.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def fibonacci(num):
    a = 0
    b = 1

    current_index = 0

    while current_index < num:
        result = a
        a, b = b, a + b
        current_index += 1

        yield result


# fib = fibonacci(3)
fib = fibonacci(5)
# 遍历生成的数据
for value in fib:
    print(value)