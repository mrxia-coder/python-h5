## 1、property属性



### 1.1 property属性的介绍

property属性就是负责把一个方法当做属性进行使用，这样做可以简化代码使用。

**定义property属性有两种方式**

1. 装饰器方式
2. 类属性方式



### 1.2 装饰器方式

```python
class Student(object):
    def __init__(self):
        # 设置私有属性
        self.__age = 20

    @property  # 当对象调用age属性的时候会执行下面的方法
    def age(self):
        return self.__age

    @age.setter  # 对象调用age属性设置值的时候会执行下面的方法，这里的函数名需要和上面的@property装饰的函数名一致
    def age(self, new_age):
        if new_age >= 0 and new_age <= 130:
            self.__age = new_age
        else:
            print("年龄超出正常范围！")


student = Student()
# age = student.age()
print(student.age)
student.age = 120
print(student.age)
```

**运行结果：**

```python
20
120
```

**代码说明:**

- @property 表示把方法当做属性使用, 表示当获取属性时会执行下面修饰的方法
- @方法名.setter 表示把方法当做属性使用,表示当设置属性时会执行下面修饰的方法
- 装饰器方式的property属性修饰的方法名一定要一样。



### 1.3 类属性方式

```python
class Student(object):
    def __init__(self):
        # 设置私有属性
        self.__age = 20

    def get_age(self):
        return self.__age

    def set_age(self, new_age):
        if new_age >= 0 and new_age <= 130:
            self.__age = new_age
        else:
            print("年龄超出正常范围！")

    # 1.get_age 表示获取age属性时执行的方法
    # 2.set_age 表示设置age属性时执行的方法
    age = property(get_age, set_age)


student = Student()
print(student.age)
student.age = 120
print(student.age)
```

**运行结果：**

```python
20
120
```

**代码说明:**

- property的参数说明:
    - 第一个参数是获取属性时要执行的方法
    - 第二个参数是设置属性时要执行的方法

### 1.4 小结

- 定义property属性有两种方式:
    1. 装饰器方式
    2. 类属性方式
- 装饰器方式:
    1. @property 修饰获取值的方法
    2. @方法名.setter 修饰设置值的方法
- 类属性方式:
    1. 类属性 = property(获取值方法, 设置值方法)



## 2、with语句

**之前向文件中写入数据的示例代码:**

```python
 # 1、以写的方式打开文件
 f = open("1.txt", "w")
 # 2、写入文件内容
 f.write("hello world")
 # 3、关闭文件
 f.close()
```

**代码说明:**

- 文件使用完后必须关闭，因为文件对象会占用操作系统的资源，并且操作系统同一时间能打开的文件数量也是有限的

**这种写法可能出现一定的安全隐患，错误代码如下:**

```python
 # 1、以读的方式打开文件
 f = open("1.txt", "r")
 # 2、读取文件内容
 f.write("hello world")
 # 3、关闭文件
 f.close()
```

**运行结果:**

```python
Traceback (most recent call last):
  File "/home/python/Desktop/test/xxf.py", line 4, in <module>
    f.write("hello world")
io.UnsupportedOperation: not writable
```

**代码说明:**

- 由于文件读写时都有可能产生IOError，一旦出错，后面的f.close()就不会调用。
- 为了保证无论是否出错都能正确地关闭文件，我们可以使用try ... finally来解决

**安全写法, 代码如下:**

```python
try:
    # 1、以读的方式打开文件
    f = open("1.txt", "r")
    # 2、读取文件内容
    f.write("xxxxx")

except IOError as e:
    print("文件操作出错", e)

finally:
    # 3、关闭文件
    f.close()
```

这种方法虽然代码运行良好,但是缺点就是代码过于冗长,并且需要添加try-except-finally语句,不是很方便,也容易忘记.

在这种情况下,**Python提供了 with 语句的这种写法，既简单又安全，并且 with 语句执行完成以后自动调用关闭文件操作，即使出现异常也会自动调用关闭文件操作**。

**with 语句的示例代码:**

```python
# 1、以写的方式打开文件
with open("1.txt", "w") as f:
    # 2、读取文件内容
    f.write("hello world")
```



## 3、上下文管理器

一个类只要实现了`__enter__()和__exit__()`这个两个方法，通过该类创建的对象我们就称之为上下文管理器。

上下文管理器可以使用 with 语句，**with语句之所以这么强大，背后是由上下文管理器做支撑的**，也就是说刚才使用 open 函数创建的文件对象就是就是一个上下文管理器对象。



### 3.1 自定义上下文管理器类

自定义上下文管理器类,模拟文件操作:

定义一个File类，实现 `__enter__() 和 __exit__()`方法，然后使用 with 语句来完成操作文件， 示例代码:

```python
class File(object):
    def __init__(self, file_name, file_mode):
        self.file_name = file_name
        self.file_mode = file_mode

    def __enter__(self):
        # 上文方法，负责返回操作对象资源，比如：文件对象，数据库连接对象等
        print("上文方法执行...")
        self.file = open(self.file_name, self.file_mode)

        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("下文方法执行...")
        self.file.close()


# with语句结合上下文管理器使用
with File("1.txt", "r") as file:
    file_data = file.read()
    print(file_data)
```

**运行结果：**

```python
上文方法执行...
hello
下文方法执行...
```

**代码说明:**

- `__enter__`表示上文方法，需要返回一个操作文件对象
- `__exit__`表示下文方法，with语句执行完成会自动执行，即使出现异常也会执行该方法。



### 3.2 上下文管理器的另外一种实现方式

假如想要让一个函数成为上下文管理器，Python 还提供了一个 @contextmanager 的装饰器，更进一步简化了上下文管理器的实现方式。通过 yield 将函数分割成两部分，yield 上面的语句在 `__enter__` 方法中执行，yield 下面的语句在 `__exit__` 方法中执行，紧跟在 yield 后面的参数是函数的返回值。

```python
# 导入装饰器
from contextlib import contextmanager


# 装饰器装饰函数，让其成为一个上下文管理器
@contextmanager
def my_open(path, mode):
    try:
        file = open(path, mode)

        # yield关键字之前的代码可以认为是上文方法，负责返回操作对象资源
        yield file
    except Exception as e:
        print(e)
    finally:
        # yield关键字之后的代码可以认为是下文方法，负责释放操作对象资源
        file.close()


with my_open("1.txt", "w") as f:
    f.write("Hello contextmanager")
```

