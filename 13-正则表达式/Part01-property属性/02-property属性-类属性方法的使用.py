# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/31 9:11
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-property属性-类属性方法的使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Student(object):
    def __init__(self):
        # 设置私有属性
        self.__age = 20

    def get_age(self):
        return self.__age

    def set_age(self, new_age):
        if new_age >= 0 and new_age <= 130:
            self.__age = new_age
        else:
            print("年龄超出正常范围！")

    # 1.get_age 表示获取age属性时执行的方法
    # 2.set_age 表示设置age属性时执行的方法
    age = property(get_age, set_age)


student = Student()
print(student.age)
student.age = 120
print(student.age)
