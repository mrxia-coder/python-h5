# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/31 9:00
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-property属性-装饰器方法的使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Student(object):
    def __init__(self):
        # 设置私有属性
        self.__age = 20

    @property  # 当对象调用age属性的时候会执行下面的方法
    def age(self):
        return self.__age

    @age.setter  # 对象调用age属性设置值的时候会执行下面的方法，这里的函数名需要和上面的@property装饰的函数名一致
    def age(self, new_age):
        if new_age >= 0 and new_age <= 130:
            self.__age = new_age
        else:
            print("年龄超出正常范围！")


student = Student()
# age = student.age()
print(student.age)
student.age = 120
print(student.age)
