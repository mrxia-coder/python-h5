# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/2 10:36
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-匹配分组.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import re

fruit_list = ["apple", "banana", "orange", "pear", "peach"]

for value in fruit_list:
    match_obj = re.match("apple|pear", value)
    if match_obj:
        print(f"{match_obj.group()}是我要的")
    else:
        print(f"{value}不是我要的。")

# 匹配邮箱
# () 表示分组，从1开始
# \. 表示转义，原本.表示任意一个字符，转义之后\.就只代表字符 '.'
match_obj = re.match("([a-zA-Z0-9_]{4,20})@(163|126|qq|sina|google|yahoo|139|189)\.com", "staryjie@163.com")
if match_obj:
    print(match_obj.group())  # => print(match_obj.group(0))
    print(match_obj.group(1))
    print(match_obj.group(2))
else:
    print("不支持该邮箱！")

print("-" * 30)

match_obj = re.match("(qq):([1-9]\d{4,11})", "qq:877930128")
if match_obj:
    print(match_obj.group())
    print(match_obj.group(1))
    print(match_obj.group(2))
else:
    print("未匹配到")

# match_obj = re.match("<[a-zA-Z1-6]+>.*</[a-zA-Z1-6]+>", "<html>hh</div>")
# match_obj = re.match("<[a-zA-Z1-6]+>.*</[a-zA-Z1-6]+>", "<html>hh</html>")
# match_obj = re.match("<([a-zA-Z1-6]+)>.*</\\1>", "<html>hh</div>")
match_obj = re.match("<([a-zA-Z1-6]+)>.*</\\1>", "<html>hh</html>")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到")

match_obj = re.match("<(?P<nam1>[a-zA-Z1-6]+)><(?P<name2>[a-zA-Z1-6]+)>.*</(?P=name2)></(?P=nam1)>", "<html><h1>www.cnblogs.com/jie-fang/</h1></html>")

if match_obj:
    print(match_obj.group())
else:
    print("匹配失败")