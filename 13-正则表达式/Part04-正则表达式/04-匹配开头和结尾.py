# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/2 10:22
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-匹配开头和结尾.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import re

# match_obj = re.match("^\d", "1st")
match_obj = re.match("^\d.*", "1st")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# match_obj = re.match(".*\d$", "wa1st2")
match_obj = re.match("^\d.*\d$", "2wa1st2")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# [^指定字符] 表示出了指定字符都匹配
# ^ 表示以指定字符开头
# [^] 表示出了指定字符串，其他的都匹配
# match_obj = re.match("^\d.*[^47]$", "2wa1st2")
# match_obj = re.match("^\d.*[^47]$", "2wa1st24")
match_obj = re.match("^\d.*[^47]$", "2wa1st27")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")
