# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/2 10:08
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-匹配多个字符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import re

# * 匹配前一个字符出现0次或者无限次，即可有可无
# match_obj = re.match("tw*o", "twwwwwwwo")
match_obj = re.match("t.*o", "tsasjko")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# + 匹配前一个字符出现1次或者无限次，即至少有1次
# match_obj = re.match("tw+o", "twwo")
match_obj = re.match("t.+o", "twwo")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# ? 匹配前一个字符出现1次或者0次，即要么有1次，要么没有
# match_obj = re.match("https?", "http")
match_obj = re.match("https?", "https")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# {m}匹配前一个字符出现m次
match_obj = re.match("ht{2}ps?", "http")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# {m,n}匹配前一个字符出现从m到n次
# match_obj = re.match("ht{1,3}ps?", "http")
match_obj = re.match("ht{1,3}ps?", "htttttp")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# 拓展: {m,} ： 至少出现m次
match_obj = re.match("ht{3,}ps?", "htttttttttttttttp")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")