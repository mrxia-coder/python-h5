# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/2 9:44
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-re模块的使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import re

# 根据正则表达式匹配数据
# 参数1：正则表达式
# 参数2： 要匹配的字符串
# 返回：匹配对象

match_obj = re.match("he", "hello")
print(match_obj)
result = match_obj.group()
print(result)
