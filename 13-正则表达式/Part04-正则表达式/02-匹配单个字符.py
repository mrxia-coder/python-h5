# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/9/2 9:49
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-匹配单个字符.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
.: 匹配任意一个字符（除了\n）
[]: 匹配[]中列举的字符
\d: 匹配数字，0-9
\D: 匹配非数字，即不是数字
\s: 匹配空白，即 空格 和 tab键
\S: 匹配非空白，即a-z、A-Z、0-9、汉字以及各种符号
\w: 匹配非特殊字符，即a-z、A-Z、0-9、 `__` 、汉字
\W: 匹配特殊字符，即非字母、非数字、非汉字、非下划线
"""

import re

# .: 匹配任意一个字符（除了\n）
# match_obj = re.match("t.o", "two")
match_obj = re.match("t.o", "t\no")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# []: 匹配[]中列举的字符
match_obj = re.match("蜡笔小新第[1-6]季", "蜡笔小新第2季")
# match_obj = re.match("蜡笔小新第[123456]季", "蜡笔小新第2季")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# \d: 匹配数字，0-9
# match_obj = re.match("[0-9]", "7")
match_obj = re.match("\d", "7")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# \D: 匹配非数字，即不是数字
match_obj = re.match("\D", "a")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# \s: 匹配空白，即 空格 和 tab键
match_obj = re.match("蜡笔小新第\s[1-6]\s季", "蜡笔小新第 2 季")
# match_obj = re.match("蜡笔小新第[123456]季", "蜡笔小新第2季")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# \w: 匹配非特殊字符，即a-z、A-Z、0-9、 `__` 、汉字
# match_obj = re.match("[a-zA-Z0-9_]", "_")
match_obj = re.match("\w", "_")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")

# \W: 匹配特殊字符，即非字母、非数字、非汉字、非下划线
match_obj = re.match("\W", "%")
if match_obj:
    print(match_obj.group())
else:
    print("未匹配到！")
