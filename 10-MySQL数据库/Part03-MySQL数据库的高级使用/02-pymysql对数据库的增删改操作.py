# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/28 17:02
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-pymysql对数据库的增删改操作.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/28 16:35
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-pymysql查询语句操作.py
# @Version : v1.0.0
# @Desc    :
# @ide     : PyCharm


import pymysql

# 创建连接对象
conn = pymysql.connect(
    host="192.168.50.176",
    port=3306,
    user="root",
    password="Vu39hbnx",
    db="python",
    charset='utf8mb4'
)

# 获取游标对象
cursor = conn.cursor()

# 查询SQL语句
# sql = "insert into classes(name) values('高三五班');"
# sql = "update classes set id = 5 where name = '高三五班';"
sql = "delete from classes where id = 5;"

try:
    cursor.execute(sql)
    # 没报错，提交数据
    conn.commit()
except Exception as e:
    # 报错，对数据进行回滚
    conn.rollback()
finally:
    cursor.close()
    conn.close()
