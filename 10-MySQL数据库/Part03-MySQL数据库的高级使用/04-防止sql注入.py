# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/28 17:14
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-防止sql注入.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import pymysql

conn = pymysql.connect(
    host="192.168.50.176",
    port=3306,
    user="root",
    password="Vu39hbnx",
    db="python",
    charset='utf8mb4'
)

cursor = conn.cursor()

# sql = "select * from students where name = '%s';" % "乔洋' or 1 = 1 or'"  # sql注入
# 准备sql， 使用防止sql注入的sql语句， %s没有引号包含，是一个参数占位符，不是之前的字符串占位符
sql = "select * from students where name = %s;"

# row_count = cursor.execute(sql, args=("乔洋' or 1 = 1 or'", ))  # 无法获取任何数据
row_count = cursor.execute(sql, args=("乔洋", ))
print(f"SQL执行影响了{row_count}行。")
for line in cursor.fetchall():
    print(line)

cursor.close()
conn.close()
