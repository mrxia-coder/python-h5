# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/28 17:20
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-防止sql注入多参数使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import pymysql

conn = pymysql.connect(
    host="192.168.50.176",
    port=3306,
    user="root",
    password="Vu39hbnx",
    db="python",
    charset='utf8mb4'
)

cursor = conn.cursor()

sql = "insert into students(name, age, height, gender, birth, is_delete, c_id) values(%s, %s, %s, %s, %s, %s, %s)"
# datetime.datetime(1998, 12, 2, 0, 57, 58), 0, 2)
# row_count = cursor.execute(sql, args=("乔洋' or 1 = 1 or'", ))  # 无法获取任何数据
row_count = cursor.execute(sql, args=["李四", 27, 176.50, "男", "1995-02-13 07:22:24", 0, 2])
print(f"SQL执行影响了{row_count}行。")
conn.commit()

cursor.close()
conn.close()
