# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/28 17:10
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-sql注入.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import pymysql

conn = pymysql.connect(
    host="192.168.50.176",
    port=3306,
    user="root",
    password="Vu39hbnx",
    db="python",
    charset='utf8mb4'
)

cursor = conn.cursor()

sql = "select * from students where name = '%s';" % "乔洋' or 1 = 1 or'"  # sql注入
row_count = cursor.execute(sql)
print(f"SQL执行影响了{row_count}行。")
for line in cursor.fetchall():
    print(line)

cursor.close()
conn.close()
