# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/28 21:25
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 06-测试索引的查询性能.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import pymysql

if __name__ == '__main__':
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="python",
        charset='utf8mb4'
    )

    cursor = conn.cursor()

    sql = "insert into my_test_index(name) values(%s);"

    try:
        for i in range(100000):
            cursor.execute(sql, ["test-" + str(i)])
        conn.commit()
    except Exception as e:
        conn.rollback()
    finally:
        cursor.close()
        conn.close()
