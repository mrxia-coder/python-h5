# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/28 16:35
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-pymysql查询语句操作.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import pymysql

# 创建连接对象
conn = pymysql.connect(
    host="192.168.50.176",
    port=3306,
    user="root",
    password="Vu39hbnx",
    db="python",
    charset='utf8mb4'
)

# 获取游标对象
cursor = conn.cursor()

# 查询SQL语句
sql = "select * from students;"
row_count = cursor.execute(sql)
print(f"SQL执行影响了{row_count}行。")

# 获取一条记录
# print(cursor.fetchone())

# 获取所有结果
for line in cursor.fetchall():
    print(line)

# 关闭游标
cursor.close()

# 关闭连接
conn.close()
