# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/24 16:39
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : get-mock-data.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

"""
EasyMock地址：http://mock.mengxuegu.com/
用户名：staryjie
密码：Vu39hbnx
项目名：students

"""

import json

import pymysql
import requests


def mysql_conn():
    """
    获取连接
    :return: conn
    """
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="python",
        charset='utf8mb4'
    )

    return conn


def truncate_table(conn):
    """
    清空表中所有的模拟数据
    :param conn:
    :return:
    """
    print("正在清除旧数据....")
    try:
        # 拿到游标
        cursor = conn.cursor()
        conn.ping(reconnect=True)  # 防止连接建立时间较长后自动断开连接，如果已经断开连接则会自动重新建立连接
        sql = "TRUNCATE TABLE python.students;"
        cursor.execute(sql)
    except Exception as e:
        print(e)

    conn.commit()
    cursor.close()
    conn.close()
    print("旧数据已清空！")


def insert_infos(conn, sql):
    """
    插入新的模拟数据
    :param conn:
    :param sql:
    :return:
    """
    try:
        # 拿到游标
        cursor = conn.cursor()
        conn.ping(reconnect=True)  # 防止连接建立时间较长后自动断开连接，如果已经断开连接则会自动重新建立连接
        cursor.execute(sql)
    except Exception as e:
        print(e)

    conn.commit()
    cursor.close()
    conn.close()


def get_students_info_list(rul):
    print("开始获取新的模拟数据...")
    return json.loads(requests.get(url).text)["students"]


if __name__ == '__main__':
    # url = "http://mengxuegu.com:7300/mock/5f437c65fd0fa244c4c5651a/students/infos"
    url = "https://mock.staryjie.com/mock/5f4391426b4d626d371d60ea/students"
    conn = mysql_conn()
    truncate_table(conn)
    students_info_list = get_students_info_list(url)
    # print(students_info_list)
    print("开始插入新的模拟数据...")
    for sinf in students_info_list:
        sql = "INSERT INTO python.students(id, name, age, height, gender, birth, is_delete, c_id) VALUES (%d, '%s', %d, %s, '%s', '%s', %d, %d);" % (
            sinf["id"], sinf["name"], sinf["age"], sinf["height"], sinf["gender"], sinf["birth"], sinf["is_delete"], sinf["c_id"])
        # print(sql)
        insert_infos(conn, sql)
    print("新的模拟数据插入完成！")
