# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/13 21:47
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-返回固定页面数据的静态web服务器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
**实现步骤:**

1. 编写一个TCP服务端程序
2. 获取浏览器发送的http请求报文数据
3. 读取固定页面数据，把页面数据组装成HTTP响应报文数据发送给浏览器。
4. HTTP响应报文数据发送完成以后，关闭服务于客户端的套接字。
"""


import socket


if __name__ == '__main__':
    # 创建TCP套接字
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 设置端口复用
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

    # 绑定端口号
    tcp_server_socket.bind(("", 9000))

    # 设置监听
    tcp_server_socket.listen(128)
    while True:
        new_socket, ip_port = tcp_server_socket.accept()
        # 自此，连接建立成功

        # 接收数据
        recv_client_data = new_socket.recv(4096)
        if recv_client_data:
            recv_client_content = recv_client_data.decode('utf-8')
            print(recv_client_content)

            with open("./index.html", 'rb') as f:
                file_data = f.read()

            # 拼接响应数据
            # 响应行
            response_line = "HTTP/1.1 200 OK\r\n"
            # 响应头
            response_header = "Server: Tengine3.2.3\r\n"
            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body

            # 发送数据
            new_socket.send(response_data)
            print("数据发送完成，关闭连接！")
            new_socket.close()
        else:
            print(f"客户端{ip_port}已下线，关闭连接！")
            new_socket.close()
