### 1、搭建Python自带静态Web服务器

#### 1.1 静态Web服务器是什么？

可以**为发出请求的浏览器提供静态文档的程序**。

平时我们浏览百度新闻数据的时候，**每天的新闻数据都会发生变化，那访问的这个页面就是动态的**，而我们开发的是**静态的，页面的数据不会发生变化**。



#### 1.2 如何搭建Python自带的静态Web服务器

搭建Python自带的静态Web服务器使用 **python3 -m http.server 端口号**, 效果图如下:

![](http://static.staryjie.com/static/images/20200813214311.png)

#### 1.4 浏览器和搭建的静态Web服务器的通信过程

![](http://static.staryjie.com/static/images/20200813214521.png)

#### 1.5 小结

- 静态Web服务器是为发出请求的浏览器提供静态文档的程序，
- 搭建Python自带的Web服务器使用`python3 –m http.server` 端口号 这个命令即可，端口号不指定默认是8000。



### 2、静态Web服务器-返回固定页面数据

#### 2.1 开发自己的静态Web服务器

**实现步骤:**

1. 编写一个TCP服务端程序
2. 获取浏览器发送的http请求报文数据
3. 读取固定页面数据，把页面数据组装成HTTP响应报文数据发送给浏览器。
4. HTTP响应报文数据发送完成以后，关闭服务于客户端的套接字。

#### 2.2 示例代码

```python
import socket


if __name__ == '__main__':
    # 创建TCP套接字
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 设置端口复用
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

    # 绑定端口号
    tcp_server_socket.bind(("", 9000))

    # 设置监听
    tcp_server_socket.listen(128)
    while True:
        new_socket, ip_port = tcp_server_socket.accept()
        # 自此，连接建立成功

        # 接收数据
        recv_client_data = new_socket.recv(4096)
        if recv_client_data:
            recv_client_content = recv_client_data.decode('utf-8')
            print(recv_client_content)

            with open("./index.html", 'rb') as f:
                file_data = f.read()

            # 拼接响应数据
            # 响应行
            response_line = "HTTP/1.1 200 OK\r\n"
            # 响应头
            response_header = "Server: Tengine3.2.3\r\n"
            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body

            # 发送数据
            new_socket.send(response_data)
            print("数据发送完成，关闭连接！")
            new_socket.close()
        else:
            print(f"客户端{ip_port}已下线，关闭连接！")
            new_socket.close()
```

**index.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>返回固定页面数据</title>
</head>
<body>
    <h3>测试页面，恭喜您，访问成功！</h3>
</body>
</html>
```



#### 2.3 浏览器访问查看效果

![](http://static.staryjie.com/static/images/20200813220004.png)

#### 2.4 查看客户端给服务器发送的数据

```text
GET / HTTP/1.1
Host: 127.0.0.1:9000
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Accept-Encoding: gzip, deflate, br
Accept-Language: zh-CN,zh;q=0.9
Cookie: csrftoken=982w1Wynn3CGK5xgSPxpzDA2Z87ZtuRRtq6PSZObgmRFUzVfyjub5HkBz6emCqEr
```

![](http://static.staryjie.com/static/images/20200813220137.png)

#### 2.5 小结

1. 编写一个TCP服务端程序

    ```python
    # 创建TCP套接字
        tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
        # 设置端口复用
        tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    
        # 绑定端口号
        tcp_server_socket.bind(("", 9000))
    
        # 设置监听
        tcp_server_socket.listen(128)
        while True:
            new_socket, ip_port = tcp_server_socket.accept()
    ```

2. 获取浏览器发送的http请求报文数据

    ```python
    recv_client_data = new_socket.recv(4096)
    recv_client_content = recv_client_data.decode('utf-8')
    print(recv_client_content)
    ```

3. 读取固定页面数据，把页面数据组装成HTTP响应报文数据发送给浏览器。

    ```python
    with open("./index.html", 'rb') as f:
        file_data = f.read()
    
        # 拼接响应数据
        # 响应行
        response_line = "HTTP/1.1 200 OK\r\n"
        # 响应头
        response_header = "Server: Tengine3.2.3\r\n"
        # 响应体
        response_body = file_data
    
        # 拼接响应报文
        response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
        # 发送数据
        new_socket.send(response_data)
    ```

4. HTTP响应报文数据发送完成以后，关闭服务于客户端的套接字。

    ```python
    print("数据发送完成，关闭连接！")
    new_socket.close()
    ```

    

### 3、静态Web服务器-返回指定页面数据

#### 3.1 静态Web服务器的问题

目前的Web服务器，不管用户访问什么页面，返回的都是固定页面的数据，接下来需要根据用户的请求返回指定页面的数据。

![](http://static.staryjie.com/static/images/20200813220658.png)

![](http://static.staryjie.com/static/images/20200813220723.png)

#### 3.2 返回指定页面数据的实现步骤

1. 获取用户请求资源的路径
2. 根据请求资源的路径，读取指定文件的数据
3. 组装指定文件数据的响应报文，发送给浏览器
4. 判断请求的文件在服务端不存在，组装404状态的响应报文，发送给浏览器



#### 3.3 示例代码

```python
import socket


if __name__ == '__main__':
    tcp_server_socket =socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    tcp_server_socket.bind(("", 9000))
    tcp_server_socket.listen(128)

    while True:
        new_socket, ip_port = tcp_server_socket.accept()
        recv_client_data = new_socket.recv(4096)

        if recv_client_data:
            recv_client_content = recv_client_data.decode('utf-8')
            print(recv_client_content)

            # 根据指定字符串对url进行分割， 最大分割次数指定2
            request_list = recv_client_content.split(" ", maxsplit=2)

            # 获取请求资源路径
            request_path = request_list[1]
            print("request_path ------ > ", request_path)

            # 判断请求资源路径
            if request_path == '/':
                request_path = '/index.html'

            try:
                with open("static" + request_path, 'rb') as f:
                    # 读取文件内容
                    file_data = f.read()
            except FileNotFoundError as e:
                # 请求资源不存在，返回404
                # 响应行
                response_line = "HTTP/1.1 404 NOT FOUND\r\n"
                # 响应头
                response_header = "Server: Tegine3.2.3\r\n"
                with open("static/error.html", 'rb') as ferr:
                    file_data = ferr.read()

                # 响应体
                response_body = file_data

                # 拼接响应报文
                response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
                new_socket.send(response_data)
                print("数据发送完成，关闭连接！")
                new_socket.close()
            else:
                # 响应行
                response_line = "HTTP/1.1 200 OK\r\n"
                # 响应头
                response_header = "Server: Tegine3.2.3\r\n"

                # 响应体
                response_body = file_data

                # 拼接响应报文
                response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
                new_socket.send(response_data)
            finally:
                print("数据发送完成，关闭连接！")
                new_socket.close()
        else:
            print(f"客户端{ip_port}已下线，关闭连接！")
            new_socket.close()
```

**static/error.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error</title>
</head>
<body>
    <h3>Page Not Found! 404</h3>
</body>
</html>
```

**static/pay.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pay</title>
</head>
<body>
    <h3>支付页面！</h3>
</body>
</html>
```

**static/shop_cart.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>购物车</title>
</head>
<body>
    <h3>购物车页面</h3>
</body>
</html>
```

**static/index.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>返回固定页面数据</title>
</head>
<body>
    <h3>测试页面，恭喜您，访问成功！</h3>
</body>
</html>
```

#### 3.4 浏览器访问查看效果

##### 3.4.1 http://127.0.0.1:9000/

![](http://static.staryjie.com/static/images/20200813223515.png)

##### 3.4.2 http://127.0.0.1:9000/pay.html

![](http://static.staryjie.com/static/images/20200813223624.png)

##### 3.4.3 http://127.0.0.1:9000/shop_cart.html

![](http://static.staryjie.com/static/images/20200813223703.png)

##### 3.4.5 其他页面返回404

![](http://static.staryjie.com/static/images/20200813223749.png)



#### 3.5 小结

1. 获取用户请求资源的路径

    ```python
    request_list = client_request_conent.split(” ”,  maxsplit=2)
    request_path = request_list[1]
    ```

2. 根据请求资源的路径，读取请求指定文件的数据

    ```python
    with open("static" + request_path, "rb") as file:
    	file_data = file.read()
    ```

3. 组装指定文件数据的响应报文，发送给浏览器

    ```python
    response_data = (response_line + response_header + "\r\n").encode("utf-8") + response_body
    conn_socket.send(response_data)
    ```

4. 判断请求的文件在服务端不存在，组装404状态的响应报文，发送给浏览器

    ```python
    try:
        # 打开指定文件,代码省略...
        except FileNotFoundError as e:
            conn_socket.send(404响应报文数据)
    ```

    

### 4、静态Web服务器-多任务版

#### 4.1 静态Web服务器的问题

目前的Web服务器，不能支持多用户同时访问，只能一个一个的处理客户端的请求，那么如何开发多任务版的web服务器同时处理 多个客户端的请求?

可以使用**多线程**，比进程更加节省内存资源。



#### 4.2 多任务版web服务器程序的实现步骤

* 当客户端和服务端建立连接成功，创建子线程，使用子线程专门处理客户端的请求，防止主线程阻塞。
* 把创建的子线程设置成为守护主线程，防止主线程无法退出。



#### 4.3 示例代码

```python
import socket
import threading


def handler_client_request(new_socket, ip_port):
    recv_client_data = new_socket.recv(4096)
    if len(recv_client_data) == 0:
        print(f"客户端{ip_port}关闭了，关闭连接！")
        new_socket.close()
        return

    recv_client_content = recv_client_data.decode('utf-8')
    print(recv_client_content)

    # 根据只读字符串截取请求资源路径
    request_list = recv_client_content.split(" ", maxsplit=2)

    # 获取请求资源路径
    request_path = request_list[1]
    print(request_path)

    # 根据不同的资源请求路径拼装不同的响应体
    if request_path == "/":
        request_path = "/index.html"

    try:
        with open("static" + request_path, "rb") as f:
            file_data = f.read()
    except FileNotFoundError as e:
        # 请求资源路径不存在，返回404
        # 响应行
        response_line = "HTTP/1.1 404 NOT FOUND\r\n"
        # 响应头
        response_header = "Server: Tegine3.2.3\r\n"
        with open("static/error.html", 'rb') as ferr:
            file_data = ferr.read()

        # 响应体
        response_body = file_data

        # 拼接响应报文
        response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
        new_socket.send(response_data)
        new_socket.close()
    else:
        # 响应行
        response_line = "HTTP/1.1 200 OK\r\n"
        # 响应头
        response_header = "Server: Tegine3.2.3\r\n"

        # 响应体
        response_body = file_data

        # 拼接响应报文
        response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
        new_socket.send(response_data)
    finally:
        new_socket.close()


def main():
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    tcp_server_socket.bind(("", 9000))

    tcp_server_socket.listen(128)

    while True:
        new_socket, ip_port = tcp_server_socket.accept()
        print(f"新的客户端{ip_port}连接成功！")

        # 建立子线程处理与客户端的通信
        sub_thread = threading.Thread(target=handler_client_request, args=(new_socket, ip_port))
        # 设置子线程守护主线程
        sub_thread.setDaemon(True)
        # 启动子线程
        sub_thread.start()


if __name__ == '__main__':
    main()
```

#### 4.4 小结

1. 当客户端和服务端建立连接成功，创建子线程，使用子线程专门处理客户端的请求，防止主线程阻塞。

    ```python
    while True:
        new_socket, ip_port = tcp_server_socket.accept()
        print(f"新的客户端{ip_port}连接成功！")
        # 建立子线程处理与客户端的通信
        sub_thread = threading.Thread(target=handler_client_request, args=(new_socket, ip_port))
    ```

2. 把创建的子线程设置成为守护主线程，防止主线程无法退出。

    ```python
    # 建立子线程处理与客户端的通信
    sub_thread = threading.Thread(target=handler_client_request, args=(new_socket, ip_port))
    # 设置子线程守护主线程
    sub_thread.setDaemon(True)
    # 启动子线程
    sub_thread.start()
    ```



### 5、静态Web服务器-面向对象开发

#### 5.1 面向对象的方式开发静态Web服务器步骤

1. 把提供服务的Web服务器抽象成一个类(HTTPWebServer)
2. 提供Web服务器的初始化方法，在初始化方法里面创建socket对象
3. 提供一个开启Web服务器的方法，让Web服务器处理客户端请求操作。



#### 5.2 示例代码

```python
import socket
import threading


# 定义web服务器类
class HttpWebServer(object):
    def __init__(self):
        tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        tcp_server_socket.bind(("", 9000))

        tcp_server_socket.listen(128)

        self.tcp_server_socket = tcp_server_socket

    @staticmethod
    def handler_client_request(new_socket, ip_port):
        recv_client_data = new_socket.recv(4096)
        if len(recv_client_data) == 0:
            print(f"客户端{ip_port}关闭了，关闭连接！")
            new_socket.close()
            return

        recv_client_content = recv_client_data.decode('utf-8')
        print(recv_client_content)

        # 根据只读字符串截取请求资源路径
        request_list = recv_client_content.split(" ", maxsplit=2)

        # 获取请求资源路径
        request_path = request_list[1]
        print(request_path)

        # 根据不同的资源请求路径拼装不同的响应体
        if request_path == "/":
            request_path = "/index.html"

        try:
            with open("static" + request_path, "rb") as f:
                file_data = f.read()
        except FileNotFoundError as e:
            # 请求资源路径不存在，返回404
            # 响应行
            response_line = "HTTP/1.1 404 NOT FOUND\r\n"
            # 响应头
            response_header = "Server: Tegine3.2.3\r\n"
            with open("static/error.html", 'rb') as ferr:
                file_data = ferr.read()

            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
            new_socket.send(response_data)
            new_socket.close()
        else:
            # 响应行
            response_line = "HTTP/1.1 200 OK\r\n"
            # 响应头
            response_header = "Server: Tegine3.2.3\r\n"

            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
            new_socket.send(response_data)
        finally:
            new_socket.close()

    # 启动web服务器进行工作
    def start(self):
        while True:
            new_socket, ip_port = self.tcp_server_socket.accept()
            sub_thread = threading.Thread(target=self.handler_client_request, args=(new_socket, ip_port))
            sub_thread.setDaemon(True)
            sub_thread.start()


# 程序入口
def main():
    web_server = HttpWebServer()
    web_server.start()


if __name__ == '__main__':
    main()
```



### 6、静态Web服务器-命令行启动动态绑定端口号

#### 6.1 命令行启动动态绑定端口号的静态web服务器实现步骤

1. 获取执行python程序的终端命令行参数
2. 判断参数的类型，设置端口号必须是整型
3. 给Web服务器类的初始化方法添加一个端口号参数，用于绑定端口号



#### 6.2 示例代码

```python
import sys
import socket
import threading


# 定义web服务器类
class HttpWebServer(object):
    def __init__(self, port):
        tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        tcp_server_socket.bind(("", port))

        tcp_server_socket.listen(128)

        self.tcp_server_socket = tcp_server_socket

    @staticmethod
    def handler_client_request(new_socket, ip_port):
        recv_client_data = new_socket.recv(4096)
        if len(recv_client_data) == 0:
            print(f"客户端{ip_port}关闭了，关闭连接！")
            new_socket.close()
            return

        recv_client_content = recv_client_data.decode('utf-8')
        print(recv_client_content)

        # 根据只读字符串截取请求资源路径
        request_list = recv_client_content.split(" ", maxsplit=2)

        # 获取请求资源路径
        request_path = request_list[1]
        print(request_path)

        # 根据不同的资源请求路径拼装不同的响应体
        if request_path == "/":
            request_path = "/index.html"

        try:
            with open("static" + request_path, "rb") as f:
                file_data = f.read()
        except FileNotFoundError as e:
            # 请求资源路径不存在，返回404
            # 响应行
            response_line = "HTTP/1.1 404 NOT FOUND\r\n"
            # 响应头
            response_header = "Server: Tegine3.2.3\r\n"
            with open("static/error.html", 'rb') as ferr:
                file_data = ferr.read()

            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
            new_socket.send(response_data)
            new_socket.close()
        else:
            # 响应行
            response_line = "HTTP/1.1 200 OK\r\n"
            # 响应头
            response_header = "Server: Tegine3.2.3\r\n"

            # 响应体
            response_body = file_data

            # 拼接响应报文
            response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
            new_socket.send(response_data)
        finally:
            new_socket.close()

    # 启动web服务器进行工作
    def start(self):
        while True:
            new_socket, ip_port = self.tcp_server_socket.accept()
            sub_thread = threading.Thread(target=self.handler_client_request, args=(new_socket, ip_port))
            sub_thread.setDaemon(True)
            sub_thread.start()


# 程序入口
def main():
    if len(sys.argv) != 2:
        print("web服务启动命令： python3 xxx.py 8000")
        return
    # 判断端口号字符串是否都是数字
    if not sys.argv[1].isdigit():
        print("web服务启动命令： python3 xxx.py 8000")
        return

    port = int(sys.argv[1])
    web_server = HttpWebServer(port)
    web_server.start()


if __name__ == '__main__':
    main()
```

