# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/13 22:46
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-静态web服务器多任务版.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import socket
import threading


def handler_client_request(new_socket, ip_port):
    recv_client_data = new_socket.recv(4096)
    if len(recv_client_data) == 0:
        print(f"客户端{ip_port}关闭了，关闭连接！")
        new_socket.close()
        return

    recv_client_content = recv_client_data.decode('utf-8')
    print(recv_client_content)

    # 根据只读字符串截取请求资源路径
    request_list = recv_client_content.split(" ", maxsplit=2)

    # 获取请求资源路径
    request_path = request_list[1]
    print(request_path)

    # 根据不同的资源请求路径拼装不同的响应体
    if request_path == "/":
        request_path = "/index.html"

    try:
        with open("static" + request_path, "rb") as f:
            file_data = f.read()
    except FileNotFoundError as e:
        # 请求资源路径不存在，返回404
        # 响应行
        response_line = "HTTP/1.1 404 NOT FOUND\r\n"
        # 响应头
        response_header = "Server: Tegine3.2.3\r\n"
        with open("static/error.html", 'rb') as ferr:
            file_data = ferr.read()

        # 响应体
        response_body = file_data

        # 拼接响应报文
        response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
        new_socket.send(response_data)
        new_socket.close()
    else:
        # 响应行
        response_line = "HTTP/1.1 200 OK\r\n"
        # 响应头
        response_header = "Server: Tegine3.2.3\r\n"

        # 响应体
        response_body = file_data

        # 拼接响应报文
        response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
        new_socket.send(response_data)
    finally:
        new_socket.close()


def main():
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    tcp_server_socket.bind(("", 9000))

    tcp_server_socket.listen(128)

    while True:
        new_socket, ip_port = tcp_server_socket.accept()
        print(f"新的客户端{ip_port}连接成功！")
        # 建立子线程处理与客户端的通信
        sub_thread = threading.Thread(target=handler_client_request, args=(new_socket, ip_port))
        # 设置子线程守护主线程
        sub_thread.setDaemon(True)
        # 启动子线程
        sub_thread.start()


if __name__ == '__main__':
    main()
