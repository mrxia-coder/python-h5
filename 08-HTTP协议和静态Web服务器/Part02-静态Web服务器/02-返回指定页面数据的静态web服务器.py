# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/13 22:08
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-返回指定页面数据的静态web服务器.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
返回指定页面数据的实现步骤:

1. 获取用户请求资源的路径
2. 根据请求资源的路径，读取指定文件的数据
3. 组装指定文件数据的响应报文，发送给浏览器
4. 判断请求的文件在服务端不存在，组装404状态的响应报文，发送给浏览器
"""


import socket


if __name__ == '__main__':
    tcp_server_socket =socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
    tcp_server_socket.bind(("", 9000))
    tcp_server_socket.listen(128)

    while True:
        new_socket, ip_port = tcp_server_socket.accept()
        recv_client_data = new_socket.recv(4096)

        if recv_client_data:
            recv_client_content = recv_client_data.decode('utf-8')
            print(recv_client_content)

            # 根据指定字符串对url进行分割， 最大分割次数指定2
            request_list = recv_client_content.split(" ", maxsplit=2)

            # 获取请求资源路径
            request_path = request_list[1]
            print("request_path ------ > ", request_path)

            # 判断请求资源路径
            if request_path == '/':
                request_path = '/index.html'

            try:
                with open("static" + request_path, 'rb') as f:
                    # 读取文件内容
                    file_data = f.read()
            except FileNotFoundError as e:
                # 请求资源不存在，返回404
                # 响应行
                response_line = "HTTP/1.1 404 NOT FOUND\r\n"
                # 响应头
                response_header = "Server: Tegine3.2.3\r\n"
                with open("static/error.html", 'rb') as ferr:
                    file_data = ferr.read()

                # 响应体
                response_body = file_data

                # 拼接响应报文
                response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
                new_socket.send(response_data)
                print("数据发送完成，关闭连接！")
                new_socket.close()
            else:
                # 响应行
                response_line = "HTTP/1.1 200 OK\r\n"
                # 响应头
                response_header = "Server: Tegine3.2.3\r\n"

                # 响应体
                response_body = file_data

                # 拼接响应报文
                response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
                new_socket.send(response_data)
            finally:
                print("数据发送完成，关闭连接！")
                new_socket.close()
        else:
            print(f"客户端{ip_port}已下线，关闭连接！")
            new_socket.close()
