## 1、web框架概述

### 1.1 web框架和web服务器的关系介绍

前面已经学习过web服务器, 我们知道web服务器主要是接收用户的http请求,根据用户的请求返回不同的资源数据，但是之前我们开发的是静态web服务器，返回的都是静态资源数据，假如我们想要web服务器返回动态资源那么该如何进行处理呢？

这里我们给大家介绍一个web框架，**使用web框架专门负责处理用户的动态资源请求，这个web框架其实就是一个为web服务器提供服务的应用程序**，简称web框架。

![](http://static.staryjie.com/static/images/20200830105838.png)

**关系说明:**

- web服务器接收浏览器发起的请求，如果是动态资源请求找web框架来处理
- web框架负责处理浏览器的动态资源请求，把处理的结果发生给web服务器
- web服务器再把响应结果发生给浏览器



### 1.2 静态资源

不需要经常变化的资源，这种资源web服务器可以提前准备好，比如: png/jpg/css/js等文件。



### 1.3 动态资源

和静态资源相反, 这种资源会经常变化，比如: 我们在京东浏览商品时经常会根据条件进行筛选，选择不同条件, 浏览的商品就不同，这种资源web服务器无法提前准备好，需要web框架来帮web服务器进行准备，在这里web服务器可以把.html的资源请求认为是动态资源请求交由web框架进行处理。



### 1.4 WSGI协议

它是web服务器和web框架之间进行协同工作的一个规则，WSGI协议规定web服务器把动态资源的请求信息传给web框架处理，web框架把处理好的结果返回给web服务器。



### 1.5 小结

- web框架是专门为web服务器处理动态资源请求的一个应用程序
- web框架和web服务器的关系是web框架专门服务于web服务器，给web服务器提供处理动态资源请求的服务。



## 2、框架程序开发

### 2.1 框架职责介绍

接收web服务器的动态资源请求，给web服务器提供处理动态资源请求的服务。



### 2.2 动态资源判断

- 根据请求资源路径的后缀名进行判断
    - 如果请求资源路径的后缀名是.html则是动态资源请求, 让web框架程序进行处理。
    - 否则是静态资源请求，让web服务器程序进行处理。

**web服务器程序(web.py)代码:**

```python
import sys
import os
import socket
import threading
import framework


# 定义web服务器类
class HttpWebServer(object):
    def __init__(self, port):
        tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        tcp_server_socket.bind(("", port))

        tcp_server_socket.listen(128)

        self.tcp_server_socket = tcp_server_socket

    @staticmethod
    def handler_client_request(new_socket, ip_port):
        recv_client_data = new_socket.recv(4096)
        if len(recv_client_data) == 0:
            print(f"客户端{ip_port}关闭了，关闭连接！")
            new_socket.close()
            return

        recv_client_content = recv_client_data.decode('utf-8')
        print(recv_client_content)

        # 根据只读字符串截取请求资源路径
        request_list = recv_client_content.split(" ", maxsplit=2)

        # 获取请求资源路径
        request_path = request_list[1]
        print(request_path)

        # 根据不同的资源请求路径拼装不同的响应体
        if request_path == "/":
            request_path = "/index.html"

        # 判断是否是动态资源请求，以后把后缀是.html的请求任务是动态资源请求
        if request_path.endswith(".html"):
            """动态资源请求"""
            # 动态资源请求找web框架处理，需要把请求参数给web框架
            # 准备给web框架的参数信息
            env = {
                "request_path": request_path,
                # 其他参数信息可以在该字典中添加即可
            }
            # 使用web框架处理动态资源请求
            # 1. web框架需要把处理结果返回给web服务器
            # 2. web服务器负责把返回的结果封装成响应报文返回给浏览器
            status, headers, response_body = framework.handler_request(env)
            print(status, headers, response_body)
            # 把返回的结果封装成响应报文返回给浏览器
            # 1.响应行
            response_line = "HTTP/1.1 %s\r\n" % status
            # 2.响应头
            response_header = ""
            for header in headers:
                response_header += "%s: %s\r\n" % header
            # 3.空行 "\r\n"
            # 4.响应体 response_body

            # 拼接整个响应报文
            response_data = (
                response_line +
                response_header +
                "\r\n" +
                response_body
            ).encode("utf-8")

            # 发送响应报文数据给浏览器
            new_socket.send(response_data)
            new_socket.close()
        else:
            """静态资源请求"""
            try:
                with open("./static" + request_path, "rb") as f:
                    file_data = f.read()
            except FileNotFoundError as e:
                # 请求资源路径不存在，返回404
                # 响应行
                response_line = "HTTP/1.1 404 NOT FOUND\r\n"
                # 响应头
                response_header = "Server: PWS/1.1\r\n"
                with open("static/error.html", 'rb') as ferr:
                    file_data = ferr.read()

                # 响应体
                response_body = file_data

                # 拼接响应报文
                response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
                new_socket.send(response_data)
            else:
                # 响应行
                response_line = "HTTP/1.1 200 OK\r\n"
                # 响应头
                response_header = "Server: PWS/1.1\r\n"

                # 响应体
                response_body = file_data

                # 拼接响应报文
                response_data = (response_line + response_header + "\r\n").encode('utf-8') + response_body
                new_socket.send(response_data)
            finally:
                new_socket.close()

    # 启动web服务器进行工作
    def start(self):
        while True:
            new_socket, ip_port = self.tcp_server_socket.accept()
            sub_thread = threading.Thread(target=self.handler_client_request, args=(new_socket, ip_port))
            sub_thread.setDaemon(True)
            sub_thread.start()


# 程序入口
def main():
    if len(sys.argv) != 2:
        print("web服务启动命令： python3 xxx.py 8000")
        return
    # 判断端口号字符串是否都是数字
    if not sys.argv[1].isdigit():
        print("web服务启动命令： python3 xxx.py 8000")
        return

    port = int(sys.argv[1])
    web_server = HttpWebServer(port)
    web_server.start()


if __name__ == '__main__':
    main()
```



### 2.3 处理客户端的动态资源请求

1. 创建web框架程序
2. 接收web服务器的动态资源请求
3. 处理web服务器的动态资源请求并把处理结果返回给web服务器
4. web服务器把处理结果组装成响应报文发送给浏览器

**web框架程序(framework.py)代码:**

```python
import time


def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # web框架处理后的数据
    data = time.ctime()

    return status, response_header, data


def not_found():
    # 状态信息
    status = "404 NOT Found"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # web框架处理后的数据
    data = "Data Not Found"

    return status, response_header, data


# 处理动态资源请求
def handler_request(env):
    # 获取动态资源请求路径
    request_path = env["request_path"]
    print(f"动态资源请求路径：{request_path}")

    # 判断请求的动态资源路径，选择指定的函数处理对应的动态资源请求
    if request_path == "/index.html":
        # 获取首页数据
        result = index()

        # 处理后的数据返回给web服务器
        return result
    else:
        # 动态资源请求没找到数据,返回404
        result = not_found()

        return result
```



### 2.4 小结

- 动态资源的判断通过请求资源路径的后缀是.html来完成，否则是静态资源
- 处理客户端的动态资源请求
    1. 接收web服务器的动态资源请求
    2. 处理动态资源请求并把处理结果返回给web服务器
    3. web服务器把处理结果组装成响应报文发送给浏览器



## 3、模板替换功能开发

### 3.1 读取股票信息模板文件

**framework.py示例代码:**

```python
def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()
```



### 3.2 使用模拟数据替换模板变量

**framework.py示例代码:**

```python
def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟数据库查询内容
    data = time.ctime()
    response_body = file_data.replace("{%content%}", data)

    return status, response_header, response_body
```



### 3.3 小结

- 模板替换功能
    1. 打开template目录下的index.html模板文件，读取模板文件数据
    2. 把模板文件中的模板变量进行替换



## 4、路由列表功能开发

### 4.1 路由的介绍

接着上面程序的判断场景，假如咱们再处理一个个人中心的动态资源请求非常简单，再添加一个函数和更加一个分支判断就可以实现了。

**framework.py 示例代码:**

```python
import time


def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟数据库查询内容
    data = time.ctime()
    response_body = file_data.replace("{%content%}", data)

    return status, response_header, response_body


def center():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/center.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟数据库查询内容
    data = time.ctime()
    response_body = file_data.replace("{%content%}", data)

    return status, response_header, response_body


def not_found():
    # 状态信息
    status = "404 NOT Found"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # web框架处理后的数据
    data = "Data Not Found"

    return status, response_header, data


# 处理动态资源请求
def handler_request(env):
    # 获取动态资源请求路径
    request_path = env["request_path"]
    print(f"动态资源请求路径：{request_path}")

    # 判断请求的动态资源路径，选择指定的函数处理对应的动态资源请求
    if request_path == "/index.html":
        # 获取首页数据
        result = index()

        # 处理后的数据返回给web服务器
        return result
    elif request_path == "/center.html":
        # 获取首页数据
        result = center()

        # 处理后的数据返回给web服务器
        return result
    else:
        # 动态资源请求没找到数据,返回404
        result = not_found()

        return result
```

那如果咱们的框架处理的页面请求路径再多一些，比如:5个路径判断，大家可能感觉条件分支完全可以胜任，如果是40个甚至更多呢? 如果这是还是用普通的条件分支简直无法忍受。

解决办法: **可以使用路由**

**什么是路由？**

路由就是请求的URL到处理函数的映射，也就是说提前把请求的URL和处理函数关联好。

**路由列表**

这么多的路由如何管理呢， 可以使用一个路由列表进行管理，通过路由列表保存每一个路由。

| 请求路径     | 处理函数   |
| ------------ | ---------- |
| /login.html  | login函数  |
| /index.html  | index函数  |
| /center.html | center函数 |



### 4.2 在路由列表添加路由

**framework.py 示例代码:**

```python
# 定义路由列表
route_list = [
    ("/index.html", index),
    ("/center.html", center)
]
```



### 4.3 根据用户请求遍历路由列表处理用户请求

**framework.py 示例代码:**

```python
import time


def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟数据库查询内容
    data = time.ctime()
    response_body = file_data.replace("{%content%}", data)

    return status, response_header, response_body


def center():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/center.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟数据库查询内容
    data = time.ctime()
    response_body = file_data.replace("{%content%}", data)

    return status, response_header, response_body


def not_found():
    # 状态信息
    status = "404 NOT Found"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # web框架处理后的数据
    data = "Data Not Found"

    return status, response_header, data


# 路由列表，列表里的每一条记录都是一个路由
route_list = [
    ("/index.html", index),
    ("/center.html", center),
]


# 处理动态资源请求
def handler_request(env):
    # 获取动态资源请求路径
    request_path = env["request_path"]
    print(f"动态资源请求路径：{request_path}")

    for path, func in route_list:
        if request_path == path:
            # 找到对应的路由，执行对应的处理函数
            result = func()

            return result
    else:
        # 动态资源请求没找到数据,返回404
        result = not_found()

        return result
```



### 4.4 小结

- 路由是请求的URL到处理函数的映射
- 路由列表是用来保存每一个设置好的路由
- 用户的动态资源请求通过遍历路由列表找到对应的处理函数来完成。



## 5、装饰器方式的添加路由

### 5.1 使用带有参数的装饰器添加路由

前面我们已经实现了路由列表，但是每次添加路由都需要手动添加来完成，接下来我们想要完成路由的自动添加，可以通过装饰器来实现，在使用装饰器对处理函数进行装饰的时候我们需要知道装饰的函数和那个请求路径进行关联，也就是说装饰器需要接收一个url参数，这样我们定义的装饰器是一个带有参数的装饰器。

**示例代码:**

```python
import time

# 路由列表，列表里的每一条记录都是一个路由
route_list = []


# 定义一个带有参数的装饰器
def route(path):
    # 装饰器
    def decorator(func):
        # 装饰器执行的时候，把请求路径和对应的处理函数加入到路由列表
        # 当装饰函数的时候只添加一次路由
        route_list.append((path, func))

        def inner():
            result = func()
            return result

        return inner

    return decorator


@route("/index.html")
def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟生成数据库数据
	data = time.ctime()
    response_body = file_data.replace("{%content%}", data)

    # 这里返回的是元组
    return status, response_header, response_body


@route("/center.html")
def center():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/center.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟数据库查询内容
    data = time.ctime()
    response_body = file_data.replace("{%content%}", data)

    return status, response_header, response_body


def not_found():
    # 状态信息
    status = "404 NOT Found"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # web框架处理后的数据
    data = "Data Not Found"

    return status, response_header, data


# 处理动态资源请求
def handler_request(env):
    # 获取动态资源请求路径
    request_path = env["request_path"]
    print(f"动态资源请求路径：{request_path}")

    for path, func in route_list:
        if request_path == path:
            # 找到对应的路由，执行对应的处理函数
            result = func()

            return result
    else:
        # 动态资源请求没找到数据,返回404
        result = not_found()

        return result
```



### 5.2 小结

使用带有参数的装饰器对处理函数进行装饰，并完成路由的添加功能。



## 6、显示股票信息页面的开发

### 6.1 数据准备

```mysql
-- 创建数据库
create database stock_db charset=utf8;
-- 切换数据库
use stock_db;
-- 执行sql文件
source stock_db.sql;
```



### 6.2 根据sql语句查询股票信息

**示例代码：**

```python
@route("/index.html")
def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 连接数据库，查询数据
    # 创建连接
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="stock_db",
        charset='utf8'
    )
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select * from info;"
    # 执行sql
    cursor.execute(sql)
    # 获取数据
    result = cursor.fetchall()
    # print(result)
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()
```



### 6.3 使用查询数据替换模板变量

**示例代码:**

```python
@route("/index.html")
def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 连接数据库，查询数据
    # 创建连接
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="stock_db",
        charset='utf8'
    )
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select * from info;"
    # 执行sql
    cursor.execute(sql)
    # 获取数据
    result = cursor.fetchall()
    # print(result)
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()

    # 遍历每一条数据，完成数据的tr封装
    data = ""
    for row in result:
        data += """<tr>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td><input type="button" value="添加" id="toAdd" name="toAdd" systemidvaule="000007"></td>
               </tr>""" % row

    response_body = file_data.replace("{%content%}", data)

    # 这里返回的是元组
    return status, response_header, response_body
```



## 7、个人中心数据接口的开发



### 7.1 根据sql语句查询个人中心数据

```python
@route("/center_data.html")
def center_data():
    # 从数据库把数据查询出来，然后把数据转成json格式
    # 创建连接
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="stock_db",
        charset='utf8'
    )
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select i.code, i.short, i.chg, i.turnover, i.price, i.highs, f.note_info from info as i inner join focus f on i.id = f.info_id;"
    # 执行sql
    cursor.execute(sql)
    # 获取数据
    result = cursor.fetchall()
    # print(result)
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()
```



### 7.2 将个人中心数据转成json字符串并返回

```python
# 个人中心数据接口
@route("/center_data.html")
def center_data():
    # 从数据库把数据查询出来，然后把数据转成json格式
    # 创建连接
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="stock_db",
        charset='utf8'
    )
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select i.code, i.short, i.chg, i.turnover, i.price, i.highs, f.note_info from info as i inner join focus f on i.id = f.info_id;"
    # 执行sql
    cursor.execute(sql)
    # 获取数据
    result = cursor.fetchall()
    # print(result)
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()

    # 把元祖转换成列表字典
    center_data_list = [
        {
            "code": row[0],
            "short": row[1],
            "chg": row[2],
            "turnover": row[3],
            "price": str(row[4]),
            "highs": str(row[5]),
            "note_info": row[6],
        } for row in result
    ]
    # print(center_data_list)
    # 把列表转出json字符串
    # ensure_ascii=False，在控制台能够显示中文
    json_str = json.dumps(center_data_list, ensure_ascii=False)
    print(json_str)
    print(type(json_str))


    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [
        ("Server", "PWS/1.1"),
        # 因为没有使用模板文件，需要通过在响应头中指定编码格式才能让中文正常显示
        ("Content-Type", "text/html;charset=utf-8")
    ]

    return status, response_header, json_str
```

**代码说明:**

- json.dumps函数把字典转成json字符串
    1. 函数的第一个参数表示要把指定对象转成json字符串
    2. 参数的第二个参数ensure_ascii=False表示不使用ascii编码，可以在控制台显示中文。
- 响应头添加Content-Type表示指定数据的编码格式



### 7.3 小结

- web框架程序还可以开发数据接口，为客户端程序提供数据服务。
    - 1. 根据sql语句查询数据库
    - 1. 把数据转成json字符串返回
    - 1. 浏览器通过指定接口地址获取web框架提供的数据。



## 8、ajax请求数据渲染个人中心页面

### 8.1 根据用户请求返回个人中心空模板文件数据

```python
# 获取个人中心数据
@route("/center.html")
def center():
    # 响应状态
    status = "200 OK"
    # 响应头
    response_header = [("Server", "PWS2.0")]

    # 打开模板文件，读取数据
    with open("template/center.html", "r") as file:
        file_data = file.read()

    # 替换模板文件中的模板遍历
    result = file_data.replace("{%content%}", "")

    return status, response_header, result
```



### 8.2 在个人中心模板文件添加ajax请求获取个人中心数据

```js
// 发送ajax请求获取个人中心页面数据
// 路径写成 center_data.html，发送ajax的时候路径其实是http://ip地址:端口号/center.data.html
$.get("center_data.html", function (data) {
        alert(data);
    }
}, "json");
```



### 8.3 将个人中心数据在页面完成展示

```js
// 发送ajax请求，获取个人中心数据
$.get("center_data.html",function (data) {
                // ajax 成功回调函数
                // 获取table标签
                var $table = $(".table");
                // 如果指定了返回数据的解析方式是json，那么data就是一个js对象
                for(var i = 0; i < data.length; i++){
                    // 根据下标获取每一个个人中心数据js对象
                    var oCenterData = data[i];

                    // 封装后的每一个tr标签
                    var oTr = '<tr>' +
                                '<td>'+ oCenterData.code +'</td>' +
                                '<td>'+ oCenterData.short +'</td>' +
                                '<td>'+ oCenterData.chg +'</td>' +
                                '<td>'+ oCenterData.turnover +'</td>' +
                                '<td>'+ oCenterData.price +'</td>' +
                                '<td>'+ oCenterData.highs +'</td>' +
                                '<td>'+ oCenterData.note_info +'</td>' +
                                '<td><a type="button" class="btn btn-default btn-xs" href="/update/000007.html"> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> 修改 </a></td>' +
                                '<td><input type="button" value="删除" id="toDel" name="toDel" systemidvaule="000007"></td>' +
                                '</tr>';
                    // 给table标签追加每一行tr标签
                    $table.append(oTr);

                }
            }, "json");
```



## 9、logging日志

### 9.1 logging日志的介绍

在现实生活中，记录日志非常重要，比如:银行转账时会有转账记录；飞机飞行过程中，会有个黑盒子（飞行数据记录器）记录着飞机的飞行过程，那在咱们python程序中想要记录程序在运行时所产生的日志信息，怎么做呢?

可以使用 **logging** 这个包来完成

**记录程序日志信息的目的是:**

1. 可以很方便的了解程序的运行情况
2. 可以分析用户的操作行为、喜好等信息
3. 方便开发人员检查bug



### 9.2 logging日志级别介绍

日志等级可以分为5个，从低到高分别是:

1. DEBUG
2. INFO
3. WARNING
4. ERROR
5. CRITICAL

**日志等级说明:**

- DEBUG：程序调试bug时使用
- INFO：程序正常运行时使用
- WARNING：程序未按预期运行时使用，但并不是错误，如:用户登录密码错误
- ERROR：程序出错误时使用，如:IO操作失败
- CRITICAL：特别严重的问题，导致程序不能再继续运行时使用，如:磁盘空间为空，一般很少使用
- 默认的是WARNING等级，当在WARNING或WARNING之上等级的才记录日志信息。
- 日志等级从低到高的顺序是: DEBUG < INFO < WARNING < ERROR < CRITICAL



### 9.3 logging日志的使用

在 logging 包中记录日志的方式有两种:

1. 输出到控制台
2. 保存到日志文件

**日志信息输出到控制台的示例代码:**

```python
import logging

logging.debug('这是一个debug级别的日志信息')
logging.info('这是一个info级别的日志信息')
logging.warning('这是一个warning级别的日志信息')
logging.error('这是一个error级别的日志信息')
logging.critical('这是一个critical级别的日志信息')
```

**运行结果:**

```python
WARNING:root:这是一个warning级别的日志信息
ERROR:root:这是一个error级别的日志信息
CRITICAL:root:这是一个critical级别的日志信息
```

**说明:**

- 日志信息只显示了大于等于WARNING级别的日志，这说明默认的日志级别设置为WARNING

**logging日志等级和输出格式的设置:**

```python
import logging

# 设置日志等级和输出日志格式
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')

logging.debug('这是一个debug级别的日志信息')
logging.info('这是一个info级别的日志信息')
logging.warning('这是一个warning级别的日志信息')
logging.error('这是一个error级别的日志信息')
logging.critical('这是一个critical级别的日志信息')
```

**运行结果:**

```python
2019-02-13 20:41:33,080 - hello.py[line:6] - DEBUG: 这是一个debug级别的日志信息
2019-02-13 20:41:33,080 - hello.py[line:7] - INFO: 这是一个info级别的日志信息
2019-02-13 20:41:33,080 - hello.py[line:8] - WARNING: 这是一个warning级别的日志信息
2019-02-13 20:41:33,080 - hello.py[line:9] - ERROR: 这是一个error级别的日志信息
2019-02-13 20:41:33,080 - hello.py[line:10] - CRITICAL: 这是一个critical级别的日志信息
```

**代码说明:**

- level 表示设置的日志等级
- format 表示日志的输出格式, 参数说明:
    - %(levelname)s: 打印日志级别名称
    - %(filename)s: 打印当前执行程序名
    - %(lineno)d: 打印日志的当前行号
    - %(asctime)s: 打印日志的时间
    - %(message)s: 打印日志信息

**日志信息保存到日志文件的示例代码:**

```python
# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/30 18:39
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-logging日志.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import logging


"""设置日志等级和输出日志格式
    level 表示设置级别
    format 设置日志输出格式
        %(asctime)s：获取当前时间
        %(filename)s：获取文件名
        %(lineno)d：获取产生日志的代码行数
        %(levelname)s：产生日志的级别
        %(message)s：产生的日志内容
    filename="myLog.log"：将日志写入"myLog.log"文件中
    filemode="w"：每次都只写入最新的日志，之前的日志会被清空。
        w: 写入最新
        a: 追加
"""
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s',
                    filename="myLog.log",
                    filemode="w")


logging.debug("dBug级别的日志")
logging.info("INFO级别的日志")
logging.warning("Warning级别的日志")  # 在设置日志等级和输出日志格式之前，日志信息只显示了大于等于WARNING级别的日志，这说明默认的日志级别设置为WARNING
logging.error("error级别的日志")
logging.critical("Critical级别的日志")

```

**运行结果:**

![](http://static.staryjie.com/static/images/20200830185009.png)



### 9.4 logging日志在mini-web项目中应用

#### 9.4.1 程序入口模块设置logging日志的设置

```python
import sys
import os
import socket
import threading
import logging
import framework


# 在程序入口模块设置logging日志的配置信息，只需要配置一次就可以，好比单例
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s',
    filename="web.log",
    filemode="a"
)
```



#### 9.4.2 程序入口模块设置INFO级别的日志输出

**示例代码:**

```python
if request_path.endswith(".html"):
    """动态资源请求"""
    logging.info(f"动态资源请求:{request_path}")
	.....
else:
    """静态资源请求"""
    logging.info(f"静态资源请求:{request_path}")
```



#### 9.4.3 程序入口模块设置WARNING级别的日志输出

**示例代码：**

```python
if len(sys.argv) != 2:
    print("web服务启动命令： python3 xxx.py 8000")
    logging.warning("终端启动程序参数个数不正确(参数个数不是2)！")
    return
# 判断端口号字符串是否都是数字
if not sys.argv[1].isdigit():
    print("web服务启动命令： python3 xxx.py 8000")
    logging.warning("终端启动程序参数类型不正确(不是纯数字)！")
    return
```



#### 9.4.4 web框架中ERROR级别的日志输出

```python
# 处理动态资源请求
def handler_request(env):
    # 获取动态资源请求路径
    request_path = env["request_path"]
    # print(f"动态资源请求路径：{request_path}")

    for path, func in route_list:
        if request_path == path:
            # 找到对应的路由，执行对应的处理函数
            result = func()

            return result
    else:
        # 动态资源请求没找到数据,返回404
        result = not_found()
        logging.error(f"没有设置相关的路由信息:{request_path}")

        return result
```

**说明:**

- logging日志配置信息在程序入口模块设置一次，整个程序都可以生效。
    - logging.basicConfig 表示 logging 日志配置操作



### 9.4 小结

- 记录python程序中日志信息使用 logging 包来完成
- logging日志等级有5个:
    1. DEBUG
    2. INFO
    3. WARNING
    4. ERROR
    5. CRITICAL
- 打印(记录)日志的函数有5个:
    1. logging.debug函数, 表示: 打印(记录)DEBUG级别的日志信息
    2. logging.info函数, 表示: 打印(记录)INFO级别的日志信息
    3. logging.warning函数, 表示: 打印(记录)WARNING级别的日志信息
    4. logging.error函数, 表示: 打印(记录)ERROR级别的日志信息
    5. logging.critical函数, 表示: 打印(记录)CRITICAL级别的日志信息