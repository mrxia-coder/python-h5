# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/30 18:39
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-logging日志.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import logging


"""设置日志等级和输出日志格式
    level 表示设置级别
    format 设置日志输出格式
        %(asctime)s：获取当前时间
        %(filename)s：获取文件名
        %(lineno)d：获取产生日志的代码行数
        %(levelname)s：产生日志的级别
        %(message)s：产生的日志内容
    filename="myLog.log"：将日志写入"myLog.log"文件中
    filemode="w"：每次都只写入最新的日志，之前的日志会被清空。
        w: 写入最新
        a: 追加
"""
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s',
                    filename="myLog.log",
                    filemode="w")


logging.debug("dBug级别的日志")
logging.info("INFO级别的日志")
logging.warning("Warning级别的日志")  # 在设置日志等级和输出日志格式之前，日志信息只显示了大于等于WARNING级别的日志，这说明默认的日志级别设置为WARNING
logging.error("error级别的日志")
logging.critical("Critical级别的日志")
