# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/30 11:35
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : framework.py
# @Version : v1.0.0
# @Desc    : 负责处理动态资源请求
# @ide     : PyCharm
import time
import pymysql
import json
import logging

# 路由列表，列表里的每一条记录都是一个路由
route_list = []


# 定义一个带有参数的装饰器
def route(path):
    # 装饰器
    def decorator(func):
        # 装饰器执行的时候，把请求路径和对应的处理函数加入到路由列表
        # 当装饰函数的时候只添加一次路由
        route_list.append((path, func))

        def inner():
            result = func()
            return result

        return inner

    return decorator


@route("/index.html")
def index():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/index.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 连接数据库，查询数据
    # 创建连接
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="stock_db",
        charset='utf8'
    )
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select * from info;"
    # 执行sql
    cursor.execute(sql)
    # 获取数据
    result = cursor.fetchall()
    # print(result)
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()

    # 遍历每一条数据，完成数据的tr封装
    data = ""
    for row in result:
        data += """<tr>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td><input type="button" value="添加" id="toAdd" name="toAdd" systemidvaule="000007"></td>
               </tr>""" % row

    response_body = file_data.replace("{%content%}", data)

    # 这里返回的是元组
    return status, response_header, response_body


# 个人中心数据接口
@route("/center_data.html")
def center_data():
    # 从数据库把数据查询出来，然后把数据转成json格式
    # 创建连接
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="stock_db",
        charset='utf8'
    )
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select i.code, i.short, i.chg, i.turnover, i.price, i.highs, f.note_info from info as i inner join focus f on i.id = f.info_id;"
    # 执行sql
    cursor.execute(sql)
    # 获取数据
    result = cursor.fetchall()
    # print(result)
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()

    # 把元祖转换成列表字典
    center_data_list = [
        {
            "code": row[0],
            "short": row[1],
            "chg": row[2],
            "turnover": row[3],
            "price": str(row[4]),
            "highs": str(row[5]),
            "note_info": row[6],
        } for row in result
    ]
    # print(center_data_list)
    # 把列表转出json字符串
    # ensure_ascii=False，在控制台能够显示中文
    json_str = json.dumps(center_data_list, ensure_ascii=False)
    # print(json_str)
    # print(type(json_str))


    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [
        ("Server", "PWS/1.1"),
        # 因为没有使用模板文件，需要通过在响应头中指定编码格式才能让中文正常显示
        ("Content-Type", "text/html;charset=utf-8")
    ]

    return status, response_header, json_str


@route("/center.html")
def center():
    # 状态信息
    status = "200 OK"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # 1. 打开指定的模板文件，读取模板文件中的数据
    with open("template/center.html", "r", encoding="utf-8") as f:
        file_data = f.read()

    # 2.查询数据库，把模板里面的模板变量{%content%}替换成以后从数据库查询的数据
    # web框架处理后的数据
    # 模拟数据库查询内容
    response_body = file_data.replace("{%content%}", "")

    return status, response_header, response_body


def not_found():
    # 状态信息
    status = "404 NOT Found"
    # 响应头信息
    response_header = [("Server", "PWS/1.1")]
    # web框架处理后的数据
    data = "Data Not Found"

    return status, response_header, data


# 处理动态资源请求
def handler_request(env):
    # 获取动态资源请求路径
    request_path = env["request_path"]
    # print(f"动态资源请求路径：{request_path}")

    for path, func in route_list:
        if request_path == path:
            # 找到对应的路由，执行对应的处理函数
            result = func()

            return result
    else:
        # 动态资源请求没找到数据,返回404
        result = not_found()
        logging.error(f"没有设置相关的路由信息:{request_path}")

        return result
