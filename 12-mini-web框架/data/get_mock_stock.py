# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/30 16:51
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : get_mock_stock.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


"""
EasyMock地址：http://mock.mengxuegu.com/
用户名：staryjie
密码：Vu39hbnx
项目名：students

"""

import json

import pymysql
import requests


def mysql_conn():
    """
    获取连接
    :return: conn
    """
    conn = pymysql.connect(
        host="192.168.50.176",
        port=3306,
        user="root",
        password="Vu39hbnx",
        db="stock_db",
        charset='utf8mb4'
    )

    return conn


def truncate_table(conn):
    """
    清空表中所有的模拟数据
    :param conn:
    :return:
    """
    print("正在清除旧数据....")
    try:
        # 拿到游标
        cursor = conn.cursor()
        conn.ping(reconnect=True)  # 防止连接建立时间较长后自动断开连接，如果已经断开连接则会自动重新建立连接
        sql = "TRUNCATE TABLE stock_db.info;"
        cursor.execute(sql)
    except Exception as e:
        print(e)

    conn.commit()
    cursor.close()
    conn.close()
    print("旧数据已清空！")


def insert_infos(conn, sql, args):
    """
    插入新的模拟数据
    :param conn:
    :param sql:
    :return:
    """
    try:
        # 拿到游标
        cursor = conn.cursor()
        conn.ping(reconnect=True)  # 防止连接建立时间较长后自动断开连接，如果已经断开连接则会自动重新建立连接
        cursor.execute(sql, args=args)
    except Exception as e:
        print(e)

    conn.commit()
    cursor.close()
    conn.close()


def get_students_info_list(rul):
    print("开始获取新的模拟数据...")
    return json.loads(requests.get(url).text)["stockList"]


if __name__ == '__main__':
    url = "https://mock.staryjie.com/mock/5f4b646c0ddb530e485ff0b1/stock/stock"
    conn = mysql_conn()
    truncate_table(conn)
    students_info_list = get_students_info_list(url)
    # print(students_info_list)
    print("开始插入新的模拟数据...")
    for sinf in students_info_list:
        sql = "INSERT INTO stock_db.info(id, code, short, chg, turnover, price, highs, time) VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"
        # print(sql)
        arg = [i for i in sinf.values()]
        print(arg)
        insert_infos(conn, sql, args=arg)
    print("新的模拟数据插入完成！")
