/*
 Navicat Premium Data Transfer

 Source Server         : dell-mysql
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 192.168.50.176:3306
 Source Schema         : stock_db

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 30/08/2020 18:17:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Table structure for info
-- ----------------------------
DROP TABLE IF EXISTS `info`;
CREATE TABLE `info`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '股票代码',
  `short` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '股票简称',
  `chg` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '涨跌幅',
  `turnover` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '换手率',
  `price` decimal(10, 2) NOT NULL COMMENT '最新价格',
  `highs` decimal(10, 2) NOT NULL COMMENT '前期高点',
  `time` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of info
-- ----------------------------
INSERT INTO `info` VALUES (1, '545687', '具号展形', '13.18', '2.35', 25.67, 66.64, '1991-09-15');
INSERT INTO `info` VALUES (2, '954838', '代然金子', '8.17', '2.35', 41.67, 50.64, '2005-04-25');
INSERT INTO `info` VALUES (3, '691240', '它问社其', '2.18', '4.35', 51.65, 10.64, '2009-03-10');
INSERT INTO `info` VALUES (4, '820219', '增等运别', '2.11', '1.35', 78.63, 29.64, '1971-08-10');
INSERT INTO `info` VALUES (5, '354319', '委变手将', '14.15', '9.35', 34.61, 82.64, '1981-09-11');
INSERT INTO `info` VALUES (6, '354880', '金越成集', '2.17', '2.35', 89.67, 24.64, '1970-11-09');
INSERT INTO `info` VALUES (7, '378485', '才型今深', '1.14', '8.35', 29.61, 85.64, '1982-10-02');
INSERT INTO `info` VALUES (8, '730286', '两与建达', '3.16', '6.35', 31.64, 40.64, '1999-06-11');
INSERT INTO `info` VALUES (9, '811793', '造完样问', '10.11', '9.35', 52.67, 87.64, '1980-01-16');
INSERT INTO `info` VALUES (10, '064273', '理万根程', '0.14', '3.35', 84.67, 69.64, '1970-09-14');
INSERT INTO `info` VALUES (11, '374979', '议根因团', '1.18', '4.35', 33.62, 41.64, '2008-05-03');
INSERT INTO `info` VALUES (12, '343958', '公决飞活', '9.18', '7.35', 84.65, 14.64, '2002-08-20');
INSERT INTO `info` VALUES (13, '930465', '积了发认', '9.14', '7.35', 37.66, 29.64, '1987-04-27');
INSERT INTO `info` VALUES (14, '489146', '自高传领', '15.16', '6.35', 20.61, 47.64, '1975-04-15');
INSERT INTO `info` VALUES (15, '580515', '强却力度', '15.15', '2.35', 46.67, 18.64, '1977-12-29');
INSERT INTO `info` VALUES (16, '645010', '毛劳并什', '11.13', '6.35', 47.69, 59.64, '1972-02-26');
INSERT INTO `info` VALUES (17, '548463', '话断离目', '7.18', '2.35', 17.66, 64.64, '2004-06-25');
INSERT INTO `info` VALUES (18, '188211', '农设打属', '2.17', '6.35', 77.61, 20.64, '2018-05-30');
INSERT INTO `info` VALUES (19, '920830', '两青现音', '13.16', '3.35', 50.67, 77.64, '1976-07-29');
INSERT INTO `info` VALUES (20, '783689', '群加至龙', '13.11', '2.35', 13.63, 44.64, '1992-07-08');
INSERT INTO `info` VALUES (21, '323274', '外面也安', '17.15', '3.35', 36.65, 65.64, '2000-03-29');
INSERT INTO `info` VALUES (22, '461426', '细半六知', '18.15', '10.35', 39.63, 36.64, '1979-05-08');
INSERT INTO `info` VALUES (23, '619794', '志作取矿', '12.11', '2.35', 51.67, 89.64, '1991-10-20');
INSERT INTO `info` VALUES (24, '634884', '精保通五', '14.15', '9.35', 23.68, 77.64, '1993-01-22');
INSERT INTO `info` VALUES (25, '128543', '次接家过', '5.12', '1.35', 31.67, 54.64, '1990-04-25');
INSERT INTO `info` VALUES (26, '375216', '派更消名', '13.19', '8.35', 53.65, 84.64, '2015-11-07');
INSERT INTO `info` VALUES (27, '842759', '区时军王', '10.11', '5.35', 62.69, 43.64, '2002-03-27');
INSERT INTO `info` VALUES (28, '567543', '想查导发', '15.17', '5.35', 58.65, 57.64, '1988-08-19');
INSERT INTO `info` VALUES (29, '104485', '者团段广', '5.14', '4.35', 81.64, 82.64, '1986-05-15');
INSERT INTO `info` VALUES (30, '389103', '白确名理', '6.15', '7.35', 32.61, 69.64, '1998-12-19');
INSERT INTO `info` VALUES (31, '138144', '然层做热', '16.15', '6.35', 63.66, 57.64, '2017-02-10');
INSERT INTO `info` VALUES (32, '292944', '西表如酸', '15.14', '6.35', 45.63, 12.64, '1991-03-27');
INSERT INTO `info` VALUES (33, '691581', '领都采西', '10.14', '7.35', 11.66, 15.64, '1987-12-06');
INSERT INTO `info` VALUES (34, '842438', '资又照热', '13.16', '6.35', 53.67, 73.64, '2011-06-05');
INSERT INTO `info` VALUES (35, '327782', '给般意难', '19.14', '7.35', 67.68, 14.64, '1978-08-02');
INSERT INTO `info` VALUES (36, '865328', '己一低象', '-10.50', '9.35', 15.64, 75.64, '2016-07-10');
INSERT INTO `info` VALUES (37, '255576', '都者王设', '11.17', '8.35', 19.62, 44.64, '2003-04-15');
INSERT INTO `info` VALUES (38, '246485', '斗不七接', '17.16', '8.35', 31.63, 49.64, '1996-06-22');
INSERT INTO `info` VALUES (39, '423853', '型类把细', '7.13', '4.35', 89.69, 84.64, '2015-05-30');
INSERT INTO `info` VALUES (40, '712755', '争及传点', '17.13', '5.35', 39.65, 49.64, '2018-12-22');
INSERT INTO `info` VALUES (41, '415050', '消方广名', '19.16', '6.35', 15.62, 65.64, '2017-12-20');
INSERT INTO `info` VALUES (42, '466844', '论名象消', '4.12', '6.35', 36.64, 41.64, '2017-12-24');
INSERT INTO `info` VALUES (43, '245888', '问先气研', '19.16', '2.35', 77.65, 33.64, '2001-08-24');
INSERT INTO `info` VALUES (44, '215341', '江表和年', '19.14', '5.35', 37.65, 49.64, '1973-02-01');
INSERT INTO `info` VALUES (45, '140901', '多点拉率', '17.18', '7.35', 76.61, 75.64, '2018-07-16');
INSERT INTO `info` VALUES (46, '837730', '无候织国', '6.14', '8.35', 20.63, 72.64, '2019-06-05');
INSERT INTO `info` VALUES (47, '557880', '观片之江', '1.12', '9.35', 38.65, 64.64, '1974-09-17');
INSERT INTO `info` VALUES (48, '274594', '方场平今', '12.12', '4.35', 61.66, 35.64, '1988-01-07');
INSERT INTO `info` VALUES (49, '646631', '要太周知', '0.18', '8.35', 47.61, 61.64, '1972-01-27');
INSERT INTO `info` VALUES (50, '358439', '来他事观', '20.15', '7.35', 54.63, 79.64, '2008-02-13');
INSERT INTO `info` VALUES (51, '477816', '务到土几', '7.17', '5.35', 73.64, 69.64, '1981-08-13');
INSERT INTO `info` VALUES (52, '265932', '期们社际', '9.14', '3.35', 19.69, 32.64, '1999-09-06');
INSERT INTO `info` VALUES (53, '783842', '实如相安', '2.18', '10.35', 67.64, 79.64, '1976-04-06');
INSERT INTO `info` VALUES (54, '554360', '约决标军', '18.17', '8.35', 26.67, 67.64, '1988-02-27');
INSERT INTO `info` VALUES (55, '268495', '无圆细还', '1.14', '3.35', 88.66, 39.64, '1993-06-10');
INSERT INTO `info` VALUES (56, '763257', '即义矿适', '11.15', '8.35', 12.64, 32.64, '2007-09-17');
INSERT INTO `info` VALUES (57, '505475', '业本名流', '4.14', '6.35', 61.67, 89.64, '1972-11-22');
INSERT INTO `info` VALUES (58, '323870', '志油去例', '3.17', '2.35', 29.62, 46.64, '1980-02-01');
INSERT INTO `info` VALUES (59, '753842', '习界是华', '19.15', '3.35', 66.61, 77.64, '2017-03-10');
INSERT INTO `info` VALUES (60, '163821', '证加为马', '17.12', '5.35', 77.65, 57.64, '1985-10-30');
INSERT INTO `info` VALUES (61, '045098', '几第建王', '15.13', '6.35', 60.69, 18.64, '1996-01-28');
INSERT INTO `info` VALUES (62, '985524', '出西低克', '17.18', '2.35', 56.62, 22.64, '1976-03-16');
INSERT INTO `info` VALUES (63, '973149', '传际二广', '19.16', '5.35', 54.68, 49.64, '2009-08-13');
INSERT INTO `info` VALUES (64, '226890', '格格层别', '18.14', '4.35', 18.61, 14.64, '1992-06-25');
INSERT INTO `info` VALUES (65, '683527', '达被象象', '15.12', '3.35', 12.66, 45.64, '1989-10-17');
INSERT INTO `info` VALUES (66, '331454', '家取重时', '15.18', '7.35', 78.62, 26.64, '2011-08-09');
INSERT INTO `info` VALUES (67, '125789', '众提素体', '15.18', '1.35', 49.67, 49.64, '1979-05-18');
INSERT INTO `info` VALUES (68, '949365', '号地八流', '10.15', '3.35', 27.68, 71.64, '1976-08-03');
INSERT INTO `info` VALUES (69, '846862', '期专段相', '2.19', '8.35', 68.68, 72.64, '2000-04-27');
INSERT INTO `info` VALUES (70, '185871', '手同达她', '4.17', '10.35', 50.68, 28.64, '1979-09-12');
INSERT INTO `info` VALUES (71, '800832', '力节争常', '4.11', '9.35', 76.61, 19.64, '1990-03-08');
INSERT INTO `info` VALUES (72, '875044', '走取干们', '12.15', '6.35', 28.67, 13.64, '1995-10-30');
INSERT INTO `info` VALUES (73, '544794', '参由动联', '9.17', '4.35', 27.63, 16.64, '1972-02-03');
INSERT INTO `info` VALUES (74, '287472', '劳风万意', '3.12', '9.35', 36.68, 45.64, '1973-01-29');
INSERT INTO `info` VALUES (75, '705274', '相交给江', '15.11', '5.35', 85.65, 24.64, '2003-02-13');
INSERT INTO `info` VALUES (76, '947781', '没维话科', '4.12', '3.35', 12.69, 44.64, '1979-01-31');
INSERT INTO `info` VALUES (77, '261986', '根边们少', '3.12', '7.35', 43.67, 16.64, '1980-01-21');
INSERT INTO `info` VALUES (78, '653378', '受确同体', '0.13', '3.35', 10.64, 12.64, '1993-03-09');
INSERT INTO `info` VALUES (79, '524032', '完严重满', '5.16', '5.35', 39.66, 74.64, '1990-07-20');
INSERT INTO `info` VALUES (80, '281044', '照局方社', '18.14', '1.35', 22.64, 40.64, '1980-09-22');
INSERT INTO `info` VALUES (81, '557244', '增议文压', '19.11', '6.35', 13.66, 82.64, '2013-12-09');
INSERT INTO `info` VALUES (82, '332669', '没间每式', '12.17', '2.35', 71.64, 14.64, '2008-10-21');
INSERT INTO `info` VALUES (83, '840117', '确文算明', '11.14', '4.35', 57.69, 52.64, '2002-07-14');
INSERT INTO `info` VALUES (84, '923028', '例在传点', '2.12', '5.35', 76.63, 63.64, '1986-06-02');
INSERT INTO `info` VALUES (85, '646261', '向千信始', '12.17', '7.35', 33.68, 63.64, '1980-10-24');
INSERT INTO `info` VALUES (86, '395417', '本复最最', '14.12', '2.35', 43.62, 70.64, '2003-01-04');
INSERT INTO `info` VALUES (87, '255049', '议观问广', '12.16', '10.35', 20.68, 76.64, '2008-12-24');
INSERT INTO `info` VALUES (88, '871695', '导列变入', '17.12', '7.35', 37.65, 83.64, '1971-08-09');
INSERT INTO `info` VALUES (89, '424214', '养格业风', '2.15', '8.35', 67.66, 46.64, '2015-02-05');
INSERT INTO `info` VALUES (90, '903166', '及率车完', '5.15', '7.35', 67.62, 45.64, '2013-12-25');
INSERT INTO `info` VALUES (91, '301315', '己于在教', '9.12', '4.35', 57.68, 25.64, '2009-01-07');
INSERT INTO `info` VALUES (92, '837833', '往多回基', '17.12', '3.35', 31.63, 30.64, '1999-11-23');
INSERT INTO `info` VALUES (93, '184227', '机称题带', '0.15', '9.35', 87.66, 23.64, '1972-04-09');
INSERT INTO `info` VALUES (94, '415882', '开意别接', '11.14', '2.35', 37.65, 28.64, '1997-01-20');
INSERT INTO `info` VALUES (95, '163237', '元加状族', '3.13', '3.35', 41.68, 88.64, '1992-06-12');
INSERT INTO `info` VALUES (96, '476303', '里引根布', '4.16', '7.35', 71.61, 86.64, '2019-11-29');
INSERT INTO `info` VALUES (97, '421193', '何产近生', '16.18', '4.35', 54.68, 37.64, '2004-07-27');
INSERT INTO `info` VALUES (98, '076643', '区属学所', '1.17', '3.35', 66.68, 27.64, '1970-11-18');
INSERT INTO `info` VALUES (99, '627686', '关设大调', '19.11', '9.35', 75.63, 62.64, '1984-06-05');
INSERT INTO `info` VALUES (100, '835625', '回专院质', '19.16', '7.35', 58.65, 52.64, '2007-02-05');


-- ----------------------------
-- Table structure for focus
-- ----------------------------
DROP TABLE IF EXISTS `focus`;
CREATE TABLE `focus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `note_info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `info_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `info_id`(`info_id`) USING BTREE,
  CONSTRAINT `focus_ibfk_1` FOREIGN KEY (`info_id`) REFERENCES `info` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of focus
-- ----------------------------
INSERT INTO `focus` VALUES (2, '你确定买这个？', 36);
INSERT INTO `focus` VALUES (3, '利好', 37);
INSERT INTO `focus` VALUES (9, '', 88);
INSERT INTO `focus` VALUES (10, '', 89);
INSERT INTO `focus` VALUES (13, '', 1);

SET FOREIGN_KEY_CHECKS = 1;
