# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 21:34
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 06-进程之间不共享全局变量.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import time

# 定义全局变量
G_LIST = [6, 7]


# 添加数据的任务
def add_data():
    for i in range(5):
        # 因为列表是可变数据类型，可以在原有内存的基础上修改数据，并且修改后的内存地址不变
        # 所以不需要加global关键字
        # 加上global表示要修改全局变量的内存地址
        G_LIST.append(i)
        print(f"added {i}")
        time.sleep(1)
    else:
        print("data add ended.")

    print("add_data: ", G_LIST)


# 读取数据任务
def read_data():
    print(f"read data {G_LIST}")


if __name__ == '__main__':
    """
    提示：
        对于Linux和Mac系统的主进程执行的代码不会进程拷贝，但是对于windows系统来说，主进程执行的代码也会进程拷贝
        对于windows来说，创建子进程的代码如果进行拷贝执行，相当于递归无限制进行创建子进程，会报错
    解决方法：
        通过判断是否是主模块来区分
        将住进新执行的代码以及创建子进程的代码都放在 if __name__ == 'main': 下面即可。
        windows不会拷贝主模块下的代码
    """
    add_data_process = multiprocessing.Process(target=add_data)
    read_data_process = multiprocessing.Process(target=read_data)

    add_data_process.start()
    # 主进程等待添加数据的子进程执行完成以后程序再继续往下执行，读取数据
    add_data_process.join()  # 堵塞其他子进程，等该子进程只完成之后，其他子进程才继续执行
    read_data_process.start()

    print("main: ", G_LIST)
