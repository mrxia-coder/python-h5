# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 21:21
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-进程执行带参数的任务-kwargs.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import time


# 定义带参数的任务
def task(count):
    for i in range(count):
        print(f"task is processing...{count}")
        time.sleep(0.5)
        count -= 1
    else:
        print("task is ended.")


if __name__ == '__main__':
    # args传入的参数必须是字典类型
    sub_process = multiprocessing.Process(target=task, kwargs={"count": 5})
    sub_process.start()
