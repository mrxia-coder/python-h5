# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 21:51
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 07-主进程会等待所有的子进程执行结束再结束.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import time


def task():
    for i in range(10):
        print(f"task is processing ... {i}")
        time.sleep(0.2)


if __name__ == '__main__':
    sub_process = multiprocessing.Process(target=task)
    sub_process.start()

    time.sleep(0.5)
    print("main process exit.")
    exit()
