# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 21:13
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-进程执行带有参数的任务-args.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import time


# 定义带参数的任务
def task(count):
    for i in range(count):
        print(f"task is processing...{count}")
        time.sleep(0.5)
        count -= 1
    else:
        print("task is ended.")


if __name__ == '__main__':
    # args传入的参数必须是元祖类型，如果只有一个参数，必须带上','符号
    sub_process = multiprocessing.Process(target=task, args=(5,))
    sub_process.start()
