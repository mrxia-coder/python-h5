# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 20:46
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-多进程的使用.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import time


# 跳舞任务
def dance():
    for i in range(5):
        print("跳舞中...")
        time.sleep(0.2)


# 唱歌任务
def sing():
    for i in range(5):
        print("唱歌中...")
        time.sleep(0.2)


if __name__ == '__main__':
    # 创建子进程
    # group: 表示进程组，目前只能使用None
    # target: 表示执行的目标任务名(函数名、方法名)
    # name: 进程名称, 默认是Process-1, .....
    dance_process = multiprocessing.Process(target=dance)
    sing_process = multiprocessing.Process(target=sing)

    # 启动进程执行对应的任务
    dance_process.start()
    sing_process.start()
