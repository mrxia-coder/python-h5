# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 21:07
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-获取当前父进程的pid.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import os
import time


def dance():
    # 获取当前进程的pid
    print("dance: pid = ", os.getpid())
    # 获取当前进程
    print("dance: process = ", multiprocessing.current_process())

    # 获取父进程的pid
    print("dance父进程: pid = ", os.getppid())

    for i in range(5):
        print("dance...")
        time.sleep(0.2)
        # 根据进程pid杀死进程 9表示kill -9 强制杀死进程
        os.kill(os.getpid(), 9)


def sing():
    # 获取当前进程的pid
    print("sing: pid = ", os.getpid())
    # 获取当前进程
    print("sing: process = ", multiprocessing.current_process())

    # 获取父进程的pid
    print("sing父进程: pid = ", os.getppid())

    for i in range(5):
        print("sing...")
        time.sleep(0.2)


if __name__ == '__main__':
    # 获取主进程号
    print("main: pid = ", os.getpid())
    # 获取主进程
    print("main: process = ", multiprocessing.current_process())

    # 创建子进程
    dance_process = multiprocessing.Process(target=dance, name="dance_process")
    sing_process = multiprocessing.Process(target=sing, name="sing_process")

    # 启动进程
    dance_process.start()
    sing_process.start()
