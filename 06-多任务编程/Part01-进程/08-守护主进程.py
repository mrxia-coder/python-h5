# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 21:56
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 08-守护主进程.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import time


def task():
    for i in range(10):
        print(f"task is processing ... {i}")
        time.sleep(0.2)


if __name__ == '__main__':
    # 创建子进程
    sub_process = multiprocessing.Process(target=task)

    # 设置守护主进程，主进程退出子进程直接销毁，子进程的生命周期依赖与主进程
    sub_process.daemon = True

    # 启动子进程
    sub_process.start()

    # 主进程退出
    time.sleep(0.5)
    print("main process exit.")
    exit()
