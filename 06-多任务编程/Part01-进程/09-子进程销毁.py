# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/10 22:00
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 09-子进程销毁.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import multiprocessing
import time


def task():
    for i in range(10):
        print(f"task is processing ... {i}")
        time.sleep(0.2)


if __name__ == '__main__':
    # 创建子进程
    sub_process = multiprocessing.Process(target=task)

    # 设置守护主进程，主进程退出子进程直接销毁，子进程的生命周期依赖与主进程
    # sub_process.daemon = True

    # 启动子进程
    sub_process.start()

    # 主进程退出
    time.sleep(0.5)
    # 执行子进程销毁命令
    sub_process.terminate()
    print("sub_process terminated.")
    print("main process exit.")
    exit()
