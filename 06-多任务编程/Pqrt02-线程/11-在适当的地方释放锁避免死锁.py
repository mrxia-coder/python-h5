# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/11 23:27
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 11-在适当的地方释放锁避免死锁.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import threading
import time

# 创建互斥锁
lock = threading.Lock()


# 根据下标去取值， 保证同一时刻只能有一个线程去取值
def get_value(index):
    # 上锁
    lock.acquire()
    print(threading.current_thread())
    my_list = [3, 6, 8, 1]
    # 判断下标释放越界
    if index >= len(my_list):
        print("下标越界:", index)
        lock.release()  # 在这里添加释放锁，避免死锁的出现
        return  # 到这里之后不会释放锁，容易导致死锁的情况
    value = my_list[index]
    print(value)
    time.sleep(0.2)
    # 释放锁
    lock.release()


if __name__ == '__main__':
    # 模拟大量线程去执行取值操作
    for i in range(10):
        sub_thread = threading.Thread(target=get_value, args=(i,))
        sub_thread.start()
