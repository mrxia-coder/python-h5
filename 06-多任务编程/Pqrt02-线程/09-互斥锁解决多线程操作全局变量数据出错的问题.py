# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/11 23:18
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 09-互斥锁解决多线程操作全局变量数据出错的问题.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import threading

# 定义全局变量
g_num = 0

# 创建互斥锁
lock = threading.Lock()


# 循环一次给全局变量加1
def sum_num1():
    # 上锁
    lock.acquire()
    for i in range(1000000):
        global g_num
        g_num += 1

    print("sum1:", g_num)
    # 解锁
    lock.release()


# 循环一次给全局变量加1
def sum_num2():
    # 上锁
    lock.acquire()
    for i in range(1000000):
        global g_num
        g_num += 1
    print("sum2:", g_num)
    # 解锁
    lock.release()


if __name__ == '__main__':
    # 创建两个线程
    first_thread = threading.Thread(target=sum_num1)
    second_thread = threading.Thread(target=sum_num2)

    # 启动线程
    first_thread.start()
    first_thread.join()
    # 启动线程
    second_thread.start()
