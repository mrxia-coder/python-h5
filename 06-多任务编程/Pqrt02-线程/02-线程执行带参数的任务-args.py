# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/11 22:12
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-线程执行带参数的任务-args.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import threading


def show_info(name, age):
    print(f"name:{name}, age:{age}")


if __name__ == '__main__':
    # 创建子线程
    sub_thread = threading.Thread(target=show_info, args=("Tom", 26))

    # 启动线程
    sub_thread.start()

