# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/11 22:25
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-主线程会等待所有的子线程执行结束再结束.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import threading
import time


# 测试主线程是否会等待子线程执行完成以后程序再退出
def show_info():
    for i in range(5):
        print("test:", i)
        time.sleep(0.5)


if __name__ == '__main__':
    sub_thread = threading.Thread(target=show_info)
    sub_thread.start()

    # 主线程延时1秒
    time.sleep(1)
    print("main thread exit")
    exit()
