# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/11 22:17
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-线程之间执行是无序的.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import threading
import time


def task():
    time.sleep(1)
    print(threading.current_thread())


if __name__ == '__main__':

    for i in range(5):
        sub_thread = threading.Thread(target=task)
        sub_thread.start()
        # 线程之间执行时无序的，具体哪个线程执行由CPU调度决定
