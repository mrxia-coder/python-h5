# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/11 22:00
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-多线程完成任务.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import threading
import time


def dance():
    # 获取当前线程
    print("dance thread:", threading.current_thread())
    for i in range(3):
        print(f"dancing... {i}")
        time.sleep(1)


def sing():
    print("dance thread:", threading.current_thread())
    for i in range(3):
        print(f"sing... {i}")


if __name__ == '__main__':
    print("main thread:", threading.current_thread())

    # 创建dance线程
    dance_thread = threading.Thread(target=dance)
    # 创建sing的线程
    sing_thread = threading.Thread(target=sing)

    # 启动线程
    dance_thread.start()
    sing_thread.start()
