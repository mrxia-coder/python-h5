# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 21:47
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 10-综合应用-烤地瓜.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class SweetPotato():
    def __init__(self):
        # 初始化被烤时间
        self.cook_time = 0
        # 初始化地瓜状态
        self.cook_status = "生的"
        # 初始化添加调料列表
        self.condiments = []

    def cook(self, time):
        """烤地瓜的方法"""
        self.cook_time += time
        if 0 <= self.cook_time < 3:
            self.cook_status = "生的"
        elif 3 <= self.cook_time < 5:
            self.cook_status = "半生不熟的"
        elif 5 <= self.cook_time < 8:
            self.cook_status = "熟的"
        elif self.cook_time >= 8:
            self.cook_status = "烤糊了"

    def add_condiments(self, condiment):
        """添加自定义调料的方法"""
        self.condiments.append(condiment)

    def __str__(self):
        """地瓜状态函数"""
        return f"这个地瓜已经烤了{self.cook_time}分钟，已经加入了{self.condiments}等调料，现在是{self.cook_status}。"


digua1 = SweetPotato()
print(digua1)
digua1.cook(3)
print(digua1)
digua1.cook(3)
print(digua1)

digua1.add_condiments("枸杞")
digua1.add_condiments("山药")
digua1.add_condiments("盐")
digua1.add_condiments("大蒜")
print(digua1)
