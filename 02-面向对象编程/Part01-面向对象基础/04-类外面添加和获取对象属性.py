# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 15:52
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-类外面添加和获取对象属性.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def wash(self):
        print("洗衣服")


haier1 = Washer()
# 添加对象属性
haier1.width = 400
haier1.height = 600

# 获取对象属性
print(haier1.width)
print(haier1.height)
