# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 15:56
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-类里面获取对象属性.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def print_info(self):
        print(f"洗衣机的宽度是{self.width}")
        print(f"洗衣机的高度是{self.height}")


haier1 = Washer()
# 添加对象属性
haier1.width = 400
haier1.height = 600

haier1.print_info()
