# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 16:06
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 07-魔法方法之带参数的__init__().py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def print_info(self):
        print(f"洗衣机的宽度是{self.width}, 高度是{self.height}")


# __init__()函数带参数时需要按照参数对应传入参数，不然实例化对象的时候会报错
haier1 = Washer(400, 600)
# haier1 = Washer()  # TypeError: __init__() missing 2 required positional arguments: 'width' and 'height'
haier1.print_info()