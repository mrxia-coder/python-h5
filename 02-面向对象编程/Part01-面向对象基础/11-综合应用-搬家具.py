# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 22:29
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 11-综合应用-搬家具.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Furniture():
    def __init__(self, name, area):
        # 家具的名字
        self.name = name
        # 家具的占地面积
        self.area = area

    def __str__(self):
        return f"这是{self.name}，占地面积是{self.area}。"


class Home():
    def __init__(self, address, area):
        # 房子的地理位置
        self.address = address
        # 房子的占地面积
        self.area = area
        # 房子的剩余面积
        self.free_area = area
        # 已经摆放的家具列表
        self.furniture = []

    def add_furniture(self, item):
        """摆放新家具"""
        if self.free_area >= item.area:
            self.furniture.append(item.name)
            self.free_area -= item.area
        else:
            print(f"{item.name}太大，房子已经放不下了！")

    def __str__(self):
        return f"房子位于{self.address},占地{self.area}，剩余面积{self.free_area},已经摆放的家具有:{self.furniture}。"


desk = Furniture(name="餐桌", area=4)
bed = Furniture("双人床", 8)
sofa = Furniture("沙发", 18)
tv = Furniture("彩电", 2)
cook_desk = Furniture("厨房灶台", 20)
basketball_ground = Furniture("篮球场", 300)

home1 = Home(address="北京", area=120)
print(home1)

home1.add_furniture(desk)
print(home1)

home1.add_furniture(bed)
home1.add_furniture(sofa)
home1.add_furniture(tv)
home1.add_furniture(cook_desk)

print(home1)

home1.add_furniture(basketball_ground)
print(home1)
