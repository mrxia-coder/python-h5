# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 16:11
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 09-魔法方法之__del__().py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def __del__(self):
        print(f'{self}对象已经被删除')


haier1 = Washer(400, 600)
del haier1  # <__main__.Washer object at 0x000001C27EBC6748>对象已经被删除
