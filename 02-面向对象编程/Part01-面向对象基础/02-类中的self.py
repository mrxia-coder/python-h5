# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 15:43
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-类中的self.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def wash(self):
        print("洗衣服")
        print(self)


haier = Washer()
print(haier)  # <__main__.Washer object at 0x000001FE91F86748>
haier.wash()  # <__main__.Washer object at 0x000001FE91F86748>
