# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 15:59
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 06-魔法方法之__init__().py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def __init__(self):
        self.width = 400
        self.height = 600

    def print_info(self):
        print(f"洗衣机的宽度是{self.width}, 高度是{self.height}")


haier1 = Washer()
haier1.print_info()
