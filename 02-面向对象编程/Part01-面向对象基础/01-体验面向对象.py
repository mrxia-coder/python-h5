# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 10:17
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-体验面向对象.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 需求：洗衣机，功能：洗衣服

# 1.定义洗衣机类
class Washer():
    def wash(self):
        print("能洗衣服")

# 2.创建对象
haier = Washer()

# 3.验证成果
print(haier)

# 使用wash功能  -- 实例方法/对象方法
haier.wash()
