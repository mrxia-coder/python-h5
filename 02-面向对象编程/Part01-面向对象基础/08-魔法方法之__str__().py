# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 16:09
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 08-魔法方法之__str__().py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def __str__(self):
        return "这是海尔洗衣机使用说明...."


haier1 = Washer(400, 600)
print(haier1)  # 会调用类方法__str__()
