# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/4 15:47
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-一个类创建多个对象.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Washer():
    def wash(self):
        print("洗衣服")
        print(self)


haier1 = Washer()
haier1.wash()  # <__main__.Washer object at 0x000002015A846748>


haier2 = Washer()
haier2.wash()  # <__main__.Washer object at 0x000002015A87FCC8>
