# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 16:48
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-子类重写父类同名方法和属性.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


daqiu = Prentice()
print(daqiu.kongfu)
daqiu.make_cake()
