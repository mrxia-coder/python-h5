# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 16:50
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-子类调用父类的同名方法和属性.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"

    def make_cake(self):
        # 如果是先调用了父类的属性和方法，父类属性会覆盖子类属性，故在调用属性前，先调用自己子类的初始化
        self.__init__()
        # Prentice.__init__(self)
        print(f"运用{self.kongfu}制作煎饼果子。")

    # 调用父类方法，但是为保证调用到的也是父类的属性，必须在调用方法前调用父类的初始化
    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)

    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)


daqiu = Prentice()
print(daqiu.kongfu)  # [独创秘制煎饼果子配方]
daqiu.make_cake()  # 运用[独创秘制煎饼果子配方]制作煎饼果子。

daqiu.make_master_cake()  # 运用[古法煎饼果子配方]制作煎饼果子。
daqiu.make_school_cake()  # 运用[新东方煎饼果子配方]制作煎饼果子。

daqiu.make_cake()  # 运用[独创秘制煎饼果子配方]制作煎饼果子。

print(Prentice.__mro__)
