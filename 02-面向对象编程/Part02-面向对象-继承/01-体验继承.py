# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 16:30
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-体验继承.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class A(object):
    def __init__(self):
        self.num = 1

    def pring_info(self):
        print(self.num)


class B(A):
    pass


result = B()
result.pring_info()  # 1
