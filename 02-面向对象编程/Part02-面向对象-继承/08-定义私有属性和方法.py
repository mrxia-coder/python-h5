# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 17:58
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 08-定义私有属性和方法.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"
        # 设置私有属性
        self.__money = 20000000

    # 定义私有方法
    def __print_info(self):
        print(self.__money)
        print(self.kongfu)

    def make_cake(self):
        self.__init__()
        print(f"运用{self.kongfu}制作煎饼果子。")

    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)

    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)


class Tusun(Prentice):
    pass

daqiu = Prentice()
# print(daqiu.__money)  # 无法访问私有属性
# daqiu.__print_info()  # 无法调用私有方法

tusun = Tusun()
# print(tusun.__monry)  # 无法访问私有属性
# tusun.__print_info()  # 无法调用私有方法

