## 1、继承的概念

生活中的继承，一般指的是子女继承父辈的财产。

![](http://static.staryjie.com/static/images/20200805161948.png)

### 1.1 拓展：新式类和经典类(旧式类)

#### 1.1.1 经典类(旧式类)

不由任意内置类型派生出的类，称之为经典类。

```python
class 类名:
    代码
    ......
```

#### 1.1.2 新式类

```python
class 类名(object):
  代码
```



### 1.2 Python中类的继承

Python面向对象的继承指的是多个类之间的所属关系，即子类默认继承父类的所有属性和方法，具体如下：

```python
# 父类A
class A(object):
    def __init__(self):
        self.num = 1

    def info_print(self):
        print(self.num)

# 子类B
class B(A):
    pass


result = B()
result.info_print()  # 1
```

> 在Python中，所有类默认继承object类，object类是顶级类或基类；其他子类叫做派生类。



## 2、单继承

> 故事主线：一个煎饼果子老师傅，在煎饼果子界摸爬滚打多年，研发了一套精湛的摊煎饼果子的技术。师父要把这套技术传授给他的唯一的最得意的徒弟。

分析：徒弟是不是要继承师父的所有技术？

```python
# 1. 师父类
class Master(object):
    def __init__(self):
        self.kongfu = '[古法煎饼果子配方]'

    def make_cake(self):
        print(f'运用{self.kongfu}制作煎饼果子')

        
# 2. 徒弟类
class Prentice(Master):
    pass


# 3. 创建对象daqiu
daqiu = Prentice()
# 4. 对象访问实例属性
print(daqiu.kongfu)
# 5. 对象调用实例方法
daqiu.make_cake()
```



## 3、多继承

> 故事推进：daqiu是个爱学习的好孩子，想学习更多的煎饼果子技术，于是，在百度搜索到新东方厨师学院，报班学习煎饼果子技术。

所谓多继承意思就是一个类同时继承了多个父类。

```python
class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    pass


class Prentice2(Master, School):
    pass

daqiu = Prentice()
print(daqiu.kongfu)  # [新东方煎饼果子配方]
daqiu.make_cake()  # 运用[新东方煎饼果子配方]制作煎饼果子。


daqiu2 = Prentice2()
print(daqiu2.kongfu)  # [古法煎饼果子配方]

daqiu2.make_cake()  # 运用[古法煎饼果子配方]制作煎饼果子。
```

> 注意：当一个类有多个父类的时候，默认使用第一个父类的同名属性和方法。



## 4、子类重写父类同名方法和属性

> 故事：daqiu掌握了师父和培训的技术后，自己潜心钻研出自己的独门配方的一套全新的煎饼果子技术。

```python
class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


daqiu = Prentice()
print(daqiu.kongfu)
daqiu.make_cake()

print(Prentice.__mro__)
```



## 5、子类调用父类的同名方法和属性

> 故事：很多顾客都希望也能吃到古法和新东方的技术的煎饼果子。

```python
class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"

    def make_cake(self):
        # 如果是先调用了父类的属性和方法，父类属性会覆盖子类属性，故在调用属性前，先调用自己子类的初始化
        self.__init__()
        # Prentice.__init__(self)
        print(f"运用{self.kongfu}制作煎饼果子。")

    # 调用父类方法，但是为保证调用到的也是父类的属性，必须在调用方法前调用父类的初始化
    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)

    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)


daqiu = Prentice()
print(daqiu.kongfu)  # [独创秘制煎饼果子配方]
daqiu.make_cake()  # 运用[独创秘制煎饼果子配方]制作煎饼果子。

daqiu.make_master_cake()  # 运用[古法煎饼果子配方]制作煎饼果子。
daqiu.make_school_cake()  # 运用[新东方煎饼果子配方]制作煎饼果子。

daqiu.make_cake()  # 运用[独创秘制煎饼果子配方]制作煎饼果子。

print(Prentice.__mro__)
```



## 6、多层继承

> 故事：N年后，daqiu老了，想要把所有技术传承给自己的徒弟。

```python
class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"

    def make_cake(self):
        # 如果是先调用了父类的属性和方法，父类属性会覆盖子类属性，故在调用属性前，先调用自己子类的初始化
        self.__init__()
        # Prentice.__init__(self)
        print(f"运用{self.kongfu}制作煎饼果子。")

    # 调用父类方法，但是为保证调用到的也是父类的属性，必须在调用方法前调用父类的初始化
    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)

    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)


class Tusun(Prentice):
    pass


tusun = Tusun()
print(tusun.kongfu)

tusun.make_cake()
tusun.make_master_cake()
tusun.make_school_cake()
```



## 7、super()调用父类方法

```python
class Master(object):
    def __init__(self):
        self.kongfu = '[古法煎饼果子配方]'

    def make_cake(self):
        print(f'运用{self.kongfu}制作煎饼果子')


class School(Master):
    def __init__(self):
        self.kongfu = '[黑马煎饼果子配方]'

    def make_cake(self):
        print(f'运用{self.kongfu}制作煎饼果子')

        # 方法2.1
        # super(School, self).__init__()
        # super(School, self).make_cake()

        # 方法2.2
        super().__init__()
        super().make_cake()


class Prentice(School):
    def __init__(self):
        self.kongfu = '[独创煎饼果子技术]'

    def make_cake(self):
        self.__init__()
        print(f'运用{self.kongfu}制作煎饼果子')

    # 子类调用父类的同名方法和属性：把父类的同名属性和方法再次封装
    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)

    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)

    # 一次性调用父类的同名属性和方法
    def make_old_cake(self):
        # 方法一：代码冗余；父类类名如果变化，这里代码需要频繁修改
        # Master.__init__(self)
        # Master.make_cake(self)
        # School.__init__(self)
        # School.make_cake(self)

        # 方法二: super()
        # 方法2.1 super(当前类名, self).函数()
        # super(Prentice, self).__init__()
        # super(Prentice, self).make_cake()

        # 方法2.2 super().函数()
        super().__init__()
        super().make_cake()


daqiu = Prentice()

daqiu.make_old_cake()
```





## 8、私有权限

### 8.1 定义私有属性和方法

在Python中，可以为实例属性和方法设置私有权限，即设置某个实例属性或实例方法不继承给子类。

> 故事：daqiu把技术传承给徒弟的同时，不想把自己的钱(2000000个亿)继承给徒弟，这个时候就要为`钱`这个实例属性设置私有权限。

设置私有权限的方法：在属性名和方法名 前面 加上两个下划线 __。

```python
class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"
        # 设置私有属性
        self.__money = 20000000

    # 定义私有方法
    def __print_info(self):
        print(self.__money)
        print(self.kongfu)

    def make_cake(self):
        self.__init__()
        print(f"运用{self.kongfu}制作煎饼果子。")

    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)

    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)


class Tusun(Prentice):
    pass

daqiu = Prentice()
# print(daqiu.__money)  # 无法访问私有属性
# daqiu.__print_info()  # 无法调用私有方法

tusun = Tusun()
# print(tusun.__monry)  # 无法访问私有属性
# tusun.__print_info()  # 无法调用私有方法
```

> 注意：私有属性和私有方法只能在类里面访问和修改。



### 8.2 获取和修改私有属性值

在Python中，一般定义函数名`get_xx`用来获取私有属性，定义`set_xx`用来修改私有属性值。

```python
class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    def __init__(self):
        self.kongfu = "[独创秘制煎饼果子配方]"
        # 设置私有属性
        self.__money = 20000000

    # 获取私有属性
    def get_money(self):
        return self.__money

    # 修改私有属性
    def set_money(self, money):
        self.__money = money

    # 定义私有方法
    def __print_info(self):
        print(self.__money)
        print(self.kongfu)

    def make_cake(self):
        self.__init__()
        print(f"运用{self.kongfu}制作煎饼果子。")

    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)

    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)


class Tusun(Prentice):
    pass


daqiu = Prentice()
print(daqiu.get_money())  # 20000000
daqiu.set_money(50000000)
print(daqiu.get_money())  # 50000000

tusun = Tusun()
print(tusun.get_money())  # 20000000
tusun.set_money(5000)
print(tusun.get_money())  # 5000
```

