# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 16:43
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-多继承.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class School(object):
    def __init__(self):
        self.kongfu = "[新东方煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(School, Master):
    pass


class Prentice2(Master, School):
    pass

daqiu = Prentice()
print(daqiu.kongfu)  # [新东方煎饼果子配方]
daqiu.make_cake()  # 运用[新东方煎饼果子配方]制作煎饼果子。


daqiu2 = Prentice2()
print(daqiu2.kongfu)  # [古法煎饼果子配方]

daqiu2.make_cake()  # 运用[古法煎饼果子配方]制作煎饼果子。
