# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 16:39
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-单继承.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Master(object):
    def __init__(self):
        self.kongfu = "[古法煎饼果子配方]"

    def make_cake(self):
        print(f"运用{self.kongfu}制作煎饼果子。")


class Prentice(Master):
    pass


daqiu = Prentice()
print(daqiu.kongfu)

daqiu.make_cake()
