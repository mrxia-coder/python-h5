# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 21:13
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 06-静态方法.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Dog(object):
    @staticmethod
    def info_print():
        print("这是一个狗类，用于创建狗实例...")


d1 = Dog()
# 静态方法既可以使用对象访问又可以使用类访问
d1.info_print()
Dog.info_print()
