# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 20:57
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-设置和访问类属性.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Dog(object):
    height = 120


d1 = Dog()
d2 = Dog()

print(Dog.height)  # 120
print(d1.height)  # 120
print(d2.height)  # 120
