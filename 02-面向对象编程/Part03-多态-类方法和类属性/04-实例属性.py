# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 21:03
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-实例属性.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Dog(object):
    def __init__(self):
        self.height = 120

    def info_print(self):
        print(self.height)


d1 = Dog()
print(d1.height)
# print(Dog.height)  # 实例属性不能通过类访问  AttributeError: type object 'Dog' has no attribute 'height'
d1.info_print()
