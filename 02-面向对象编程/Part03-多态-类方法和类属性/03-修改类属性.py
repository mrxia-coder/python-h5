# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 21:00
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-修改类属性.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Dog(object):
    height = 120


d1 = Dog()
d2 = Dog()


# 修改类属性
Dog.height = 122
print(Dog.height)  # 122
print(d1.height)
print(d2.height)

# 通过对象修改属性其实是创建了一个实例属性，而不是修改了类属性
d1.height = 119
print(Dog.height)  # 122
print(d1.height)  # 119
print(d2.height)  # 122
