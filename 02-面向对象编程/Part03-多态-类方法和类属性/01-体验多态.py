# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 20:50
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-体验多态.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Dog(object):
    # 父类方法
    def work(self):
        print("指谁咬谁!")


class ArmyDog(Dog):
    # 重写父类的方法
    def work(self):
        print("赶鸭子!")


class DrugDog(Dog):
    # 重写父类的方法
    def work(self):
        print("叼玩具!")


class Person(object):
    # 传入不同的对象，执行不同的代码，即不同的work函数
    def work_with_dog(self, dog):
        dog.work()


d1 = ArmyDog()
d2 = DrugDog()

tom = Person()
tom.work_with_dog(d1)  # 赶鸭子!
tom.work_with_dog(d2)  # 叼玩具!
