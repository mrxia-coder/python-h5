# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/5 21:09
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-类方法.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


class Dog(object):
    __height = 120

    @classmethod
    def get_height(cls):
        return cls.__height


d1 = Dog()
result = d1.get_height()
print(result)
