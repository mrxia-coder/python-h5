## 1、面向对象三大特性

### 1.1 封装

* 将属性和方法书写到类的里面的操作即为封装
* 封装可以为属性和方法添加私有权限

### 1.2 继承

* 子类默认继承父类的所有属性和方法
* 子类可以重写父类属性和方法

### 1.3 多态

* 传入不同的对象，产生不同的结果



## 2、多态

### 2.1 了解多态

多态指的是一类事物有多种形态，（一个抽象类有多个子类，因而多态的概念依赖于继承）。

#### 2.1.1 定义

多态是一种使用对象的方式，子类重写父类方法，调用不同子类对象的相同父类方法，可以产生不同的执行结果。

#### 2.1.2 好处

调用灵活，有了多态，更容易编写出通用的代码，做出通用的编程，以适应需求的不断变化！

#### 2.1.3 实现步骤

1. 定义父类，提供公共方法
2. 定义子类，并重写父类方法
3. 传递子类对象给调用者，可以看到不同子类执行效果不同

### 2.2 体验多态

```python
class Dog(object):
    # 父类方法
    def work(self):
        print("指谁咬谁!")


class ArmyDog(Dog):
    # 重写父类的方法
    def work(self):
        print("赶鸭子!")


class DrugDog(Dog):
    # 重写父类的方法
    def work(self):
        print("叼玩具!")


class Person(object):
    # 传入不同的对象，执行不同的代码，即不同的work函数
    def work_with_dog(self, dog):
        dog.work()


d1 = ArmyDog()
d2 = DrugDog()

tom = Person()
tom.work_with_dog(d1)  # 赶鸭子!
tom.work_with_dog(d2)  # 叼玩具!
```



## 3、 类属性和实例属性

### 3.1 类属性

#### 3.1.1 设置和访问类属性

* 类属性就是 **类对象** 所拥有的属性，它被 **该类的所有实例对象 所共有**。
* 类属性可以使用 **类对象** 或 **实例对象** 访问。

```python
class Dog(object):
    height = 120


d1 = Dog()
d2 = Dog()

print(Dog.height)  # 120
print(d1.height)  # 120
print(d2.height)  # 120
```

> 类属性的优点：
>
> 1. **记录的某项数据 始终保持一致时**，则定义类属性。
> 2. **实例属性** 要求 **每个对象** 为其 **单独开辟一份内存空间** 来记录数据，而 **类属性** 为全类所共有 ，**仅占用一份内存**，**更加节省内存空间**。



#### 3.1.2 修改类属性

类属性只能通过类对象修改，不能通过实例对象修改，如果通过实例对象修改类属性，表示的是创建了一个实例属性。

```python
class Dog(object):
    height = 120


d1 = Dog()
d2 = Dog()


# 修改类属性
Dog.height = 122
print(Dog.height)  # 122
print(d1.height)
print(d2.height)

# 通过对象修改属性其实是创建了一个实例属性，而不是修改了类属性
d1.height = 119
print(Dog.height)  # 122
print(d1.height)  # 119
print(d2.height)  # 122
```



### 3.2 实例属性

```python
class Dog(object):
    def __init__(self):
        self.height = 120

    def info_print(self):
        print(self.height)


d1 = Dog()
print(d1.height)  # 120
# print(Dog.height)  # 实例属性不能通过类访问  AttributeError: type object 'Dog' has no attribute 'height'
d1.info_print()  # 120
```



## 4、类方法和静态方法

### 4.1 类方法

#### 4.1.1 类方法的特点

需要用装饰器`@classmethod`来标识其为类方法，对于类方法，**第一个参数必须是类对象**，一般以`cls`作为第一个参数。

#### 4.1.2 类方法使用场景

* 当方法中 **需要使用类对象** (如访问私有类属性等)时，定义类方法
* 类方法一般和类属性配合使用

```python
class Dog(object):
    __height = 120

    @classmethod
    def get_height(cls):
        return cls.__height


d1 = Dog()
result = d1.get_height()
print(result)
```



### 4.2 静态方法

#### 4.2.1 静态方法的特点

* 需要通过装饰器`@staticmethod`来进行修饰，**静态方法既不需要传递类对象也不需要传递实例对象（形参没有self/cls）**。
* 静态方法 也能够通过 **实例对象** 和 **类对象** 去访问。

#### 4.2.2 静态方法使用场景

* 当方法中 **既不需要使用实例对象**(如实例对象，实例属性)，**也不需要使用类对象** (如类属性、类方法、创建实例等)时，定义静态方法
* **取消不需要的参数传递**，有利于 **减少不必要的内存占用和性能消耗**

```python
class Dog(object):
    @staticmethod
    def info_print():
        print("这是一个狗类，用于创建狗实例...")


d1 = Dog()
# 静态方法既可以使用对象访问又可以使用类访问
d1.info_print()
Dog.info_print()
```