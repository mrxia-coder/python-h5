#!/bin/bash


dtime=`date +%Y_%m_%d_%H_%M_%s`

git fetch origin master:branch-$dtime

git merge branch-$dtime

git branch -D branch-$dtime