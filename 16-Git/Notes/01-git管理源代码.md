## 1、Git简介

### 1.1 Git是什么？

Git 是目前世界上最先进的分布式版本控制系统（没有之一）



### 1.2 Git的作用

源代码的管理和版本控制。



### 1.3 Git的诞生

- 作者是 Linux 之父：Linus Benedict Torvalds
- 当初开发 Git 仅仅是为了辅助 Linux 内核的开发（管理源代码）

> git 开发时间表
>
> - git 的产生是 Linux Torvals 在无奈被逼的情况下创造的，我看了一下时间历程：
>     - 2005 年 4 月3 日开始开发 git
>     - 2005 年 4 月 6 日项目发布
>     - 2005 年 4 月 7 日 Git 开始作为自身的版本控制工具
>     - 2005 年 4 月 18 日发生第一个多分支合并
>     - 2005 年 4 月 29 日 Git 的性能达到 Linux 预期
>     - 2005年 7 月 26 日 Linux 功成身退，将 Git 维护权交给 Git 另一个主要贡献者 Junio C Hamano，直到现在
>
> Git 迅速成为最流行的分布式版本控制系统，尤其是 2008 年，GitHub 网站上线了，它为开源项目免费提供 Git 存储，无数开源项目开始迁移至 GitHub，包括 jQuery，PHP，Ruby 等等



### 1.4 Git管理源代码特点

1. `Git`是分布式管理.服务器和客户端都有版本控制能力,都能进行代码的提交、合并、...

    ![](http://static.staryjie.com/static/images/20200921084544.png)

2. `Git`会在根目录下创建一个`.git`隐藏文件夹，作为本地代码仓库



### 1.5 Git操作流程图解

```bash
Git服务器 --> 本地仓库 --> 客户端 --> 本地仓库 --> Git服务器
```

![](http://static.staryjie.com/static/images/20200921084830.png)

 

## 2、Git工作区暂存区和仓库区

![](http://static.staryjie.com/static/images/20200921090855.png)



### 2.1 工作区

- 对于`添加`、`修改`、`删除`文件的操作，都发生在工作区中



### 2.2 暂存区

- 暂存区指将工作区中的操作完成小阶段的存储，是版本库的一部分



### 2.3 仓库区

* 仓库区表示个人开发的一个小阶段的完成
    * 仓库区中记录的各版本是可以查看并回退的
    * 但是在暂存区的版本一旦提交就再也没有了



## 3、Git单人操作本地仓库

### 3.1 安装git

```bash
yum install -y git
```



### 3.2 查看git安装结果

```bash
git --version
```



### 3.3 创建项目

```bash
mkdir -pv /opt/code/test
```

> `/opt/code/test`中的`test`目录作为工作项目。



### 3.4 创建本地仓库

```bash
# 进入工作项目目录
cd /opt/code/test

# 创建本地仓库
git init
```

![](http://static.staryjie.com/static/images/20200921091648.png)



### 3.5 配置个人信息

```bash
git config user.name "staryjie"
git config user.email "staryjie@163.com"
```

![](http://static.staryjie.com/static/images/20200921091822.png)

> 如果没有在项目中配置个人信息，默认会使用全局配置中的用户和邮箱。



### 3.6 编写代码

```bash
echo "print("Hello Git")" >> ./hello.py
```



### 3.7 查看文件状态

```bash
git status
```

新建的文件和修改后的文件都在工作区：

![](http://static.staryjie.com/static/images/20200921092420.png)

```bash
# git add 代码
git add hello.py
```



### 3.8 将工作区文件添加到暂存区

git add 文件之后，文件进入暂存区：

![](http://static.staryjie.com/static/images/20200921092646.png)

```bash
# git commit 代码
git commit -m "first commit"
```



### 3.9 将暂存区文件提交到仓库区

git commit 文件之后，文件进入仓库区(本地仓库)：

![](http://static.staryjie.com/static/images/20200921092942.png)

```bash
# 模拟开发
echo "num1 = 100" >> ./hello.py
```



### 3.10 仓库区的文件编辑后又回到工作区

修改代码文件之后，文件又再次进入工作区：

![](http://static.staryjie.com/static/images/20200921093153.png)

```bash
# 新建代码文件
touch login.py
```

新建的文件必须通过`git add`追踪才有工作区、暂存区和仓库区。

![](http://static.staryjie.com/static/images/20200921093439.png)

```bash
# git add hello.py
git add hello.py
```

![](http://static.staryjie.com/static/images/20200921093622.png)

```bash
# 将hello.py提交到仓库区
git commit -m "add a variable"
```

![](http://static.staryjie.com/static/images/20200921093742.png)

> 这时候只剩下`login.py`这个未跟踪的文件了。



### 3.11 查看历史版本

```bash
git log
# 或者
git reflog
```

![](http://static.staryjie.com/static/images/20200921094238.png)

> git reflog 可以查看所有分支的所有操作记录（包括commit和reset的操作），包括已经被删除的commit记录，git log 则不能察看已经删除了的commit记录。



### 3.12 回退版本

#### 3.12.1 回退版本方法一

- `HEAD`表示当前最新版本
- `HEAD^`表示当前最新版本的前一个版本
- `HEAD^^`表示当前最新版本的前两个版本，**以此类推...**
- `HEAD~1`表示当前最新版本的前一个版本
- `HEAD~10`表示当前最新版本的前10个版本，**以此类推...**

```bash
# 回退到当前最新版本的前一个版本
git reset --hard HEAD^
```



#### 3.12.2 回退版本方法二

**当版本非常多时可选择的方案**：

通过每个版本的版本号回退到指定版本：

```bash
git reset --hard 版本号
```

> 其中版本号可以通过`git log`或者`git reflog`查看
>
> ![](http://static.staryjie.com/static/images/20200921094704.png)
>
> ![](http://static.staryjie.com/static/images/20200921094822.png)
>
> git commit时会自动生成版本号，而且是唯一的，一般情况下用版本号的前7位即可。



### 3.13 撤销修改

- 只能撤销工作区、暂存区的代码,不能撤销仓库区的代码
- 撤销仓库区的代码就相当于回退版本操作



#### 3.13.1 撤销工作区代码

```bash
# 查看当前代码状态
cat hello.py
```

![](http://static.staryjie.com/static/images/20200921122650.png)

```bash
# 编辑代码
echo "num2 = 200" >> hello.py
```

![](http://static.staryjie.com/static/images/20200921122816.png)

```bash
# 这时候我们需要回退代码
git checkout hello.py
```

![](http://static.staryjie.com/static/images/20200921123126.png)

![](http://static.staryjie.com/static/images/20200921123247.png)



#### 3.13.2 撤销暂存区的代码

基于上面的代码，我们将代码改到如下所示：

![](http://static.staryjie.com/static/images/20200921123443.png)

```bash
# git add 将代码文件提交到暂存区
git add hello.py
```

![](http://static.staryjie.com/static/images/20200921123537.png)

```bash
# 回退暂存区的代码
git reset HEAD hello.py  # 这里只是将文件从暂存区放回工作区
git checkout hello.py  # 这里实现代码的回退
```

![](http://static.staryjie.com/static/images/20200921123723.png)



## 4、Git远程仓库GitHub

> 提示：Github网站作为远程代码仓库时的操作和本地代码仓库一样的，只是仓库位置不同而已！

### 4.1 创建远程仓库

1. 登录GitHub

    ![](http://static.staryjie.com/static/images/20200921124841.png)

2. 创建新的仓库

    ![](http://static.staryjie.com/static/images/20200921125302.png)

3. 点击`Create repository`仓库创建完成

    ![](http://static.staryjie.com/static/images/20200921125450.png)

    远程仓库的地址为：https://github.com/staryjie/newbee.git



### 4.2 配置SSHKey

在上一步的图中，仓库操作有`HTTPS`和`SSH`以及`GitHub CLI`三种，其中`SSH`操作模式需要通过ssh协议进行，不论是哪种方式，如果是私有仓库，则在clone或者push的时候都需要输入用户名密码进行验证。

为了方便起见，我们一般会设置自己的SSHKey，通过SSHKey免密操作远程仓库。

GitHub添加SSHKey的步骤如下：

1. 点击右上角头像右侧的下三角形，选择`Settings`

    ![](http://static.staryjie.com/static/images/20200921130330.png)

2. 选择左侧的`SSH and GPG keys`，然后点击右上角的`New SSH key`

    ![](http://static.staryjie.com/static/images/20200921130519.png)

3. 在需要操作远程仓库的主机上生成SSH Key

    ```bash
    ssh-keygen -t rsa -C "staryjie.com"
    ```

    ![](http://static.staryjie.com/static/images/20200921130856.png)

4. 配置SSH Key

    将服务器上生成的公钥`id_rsa.pub`的内容复制到GitHub的SSH Key中：

    ![](http://static.staryjie.com/static/images/20200921131103.png)

5. 点击`Add SSH key`即可

    ![](http://static.staryjie.com/static/images/20200921131404.png)



### 4.3 克隆项目

1. 进入存放项目代码的目录

    ```bash
    cd /opt/code/
    ```

2. 克隆项目

    ```bash
    git clone https://github.com/staryjie/newbee.git
    ```

    ![](http://static.staryjie.com/static/images/20200921132126.png)

3. 然后进行相关的配置和开发工作



### 4.4 多人协同开发

#### 4.4.1 克隆项目

分别模拟`zhangsan`和`staryjie`两个用户同时对项目进行开发。

**zhangsan：**

```bash
# 克隆项目
cd /opt/code/zhangsan/
git clone https://github.com/staryjie/newbee.git

# 配置git信息
cd /opt/code/zhangsan/newbee/
git config user.name "zhangsan"
git config user.email "zhangsan@163.com"
```

![](http://static.staryjie.com/static/images/20200921175013.png)

**staryjie：**

```bash
# 克隆项目
cd /opt/code/staryjie/
git clone https://github.com/staryjie/newbee.git

# 配置git信息
cd /opt/code/staryjie/newbee/
git config user.name "staryjie"
git config user.email "staryjie@163.com"
```

![](http://static.staryjie.com/static/images/20200921175055.png)



#### 4.4.2 模拟zhangsan开发

```bash
# 进入项目目录
cd /opt/code/zhangsan/newbee/

# 编写代码
echo "zs_name='zhangsan'" >> login.py

# 加入暂存区
git add login.py

# 提交到仓库区
git commit -m "添加一个zs_name变量"

# 推送至远程仓库
git push -u origin master
```



#### 4.4.3 模拟staryjie开发

> 因为前面zhangsan已经开发并提交至远程仓库了，staryjie要想接着zhangsan的代码继续开发，必须要先拉取远程仓库最新的代码。
>
> 实际开发过程中要加强团队交流，有任何代码提交应该互相通知，每天开始开发之前也应该拉取一遍最新的代码，然后再进行开发。

```bash
# 进入项目目录
cd /opt/code/staryjie/newbee

# 拉取最新代码
git pull

# 继续开发
echo "sj_name='staryjie'" >> login.py

# 加入暂存区
git add login.py

# 提交到仓库区
git commit -m "添加一个sj_name变量"

# 推送至远程仓库
git push -u origin master
```



#### 4.4.4 zhangsan同步代码

zhansgan如果要继续开发，必须也要拉取远程仓库最新的代码：

```bash
git pull
```



> 按照上述2、3、4的步骤重复操作，即可实现基本的多人协助开发。



#### 4.4.5 小结

- 要使用git命令操作仓库，需要进入到仓库内部
- 要同步服务器代码就执行：`git pull`
- 本地仓库记录版本就执行：`git commit -am '版本描述'`
- 推送代码到服务器就执行：`git push`
- 编辑代码前要先`pull`，编辑完再`commit`，最后推送是`push`



### 4.5 代码冲突

- **提示**：多人协同开发时，避免不了会出现代码冲突的情况
- **原因**：多人同时修改了同一个文件
- **危害**：会影响正常的开发进度
- **注意**：一旦出现代码冲突，必须先解决再做后续开发



#### 4.5.1 zhangsan编辑代码

```bash
git pull

echo "num1 = 100" >> login.py
git add login.py
git commit -m "添加一个num1变量"
git push -u origin master
```



#### 4.5.2 staryjie编辑代码

> 这里staryjie并没有拉取远程仓库最新的代码。这就是大多数代码冲突发生的原因。
>
> 还有可能就是在staryjie拉取最新代码之后，zhangsan又提交了最新代码到远程仓库，然后staryjie并不知道，所以认为自己当前的代码已经是最新的代码。

```bash
echo "num1 = 111" >> login.py
git add login.py
git commit -m "添加一个变量"
git push -u origin master
```

这时候会提示无法push：

![](http://static.staryjie.com/static/images/20200921181311.png)



#### 4.5.3 解决冲突

- 原则：谁冲突谁解决，并且一定要协商解决
- 方案：保留所有代码 或者 保留某一人代码
- 解决完冲突代码后，依然需要`add`、`commit`、`push`

1. staryjie拉取最新代码

    ```bash
    git pull
    ```

    ![](http://static.staryjie.com/static/images/20200921182148.png)

    ![](http://static.staryjie.com/static/images/20200921182235.png)

2. 修改冲突

    协商后修改代码：

    ```bash
    zs_name='zhangsan'
    sj_name='staryjie'
    num1 = 111
    num2 = 100
    ```

3. 提交代码

    ```bash
    git add login.py
    git commit -am "解决冲突"
    git push -u origin master
    ```

    ![](http://static.staryjie.com/static/images/20200921182725.png)

4. zhangsan拉取最新代码

    ```bash
    git pull
    ```

> 冲突发生的原因是某个人没有拉取最新代码，并且两个人编辑了同一行代码。



#### 4.5.4 补充

- **容易冲突的操作方式**
    - 多个人同时操作了同一个文件
    - 一个人一直写不提交
    - 修改之前不更新最新代码
    - 提交之前不更新最新代码
    - 擅自修改同事代码
- **减少冲突的操作方式**
    - 养成良好的操作习惯,先`pull`在修改,修改完立即`commit`和`push`
    - 一定要确保自己正在修改的文件是最新版本的
    - 各自开发各自的模块
    - 如果要修改公共文件,一定要先确认有没有人正在修改
    - 下班前一定要提交代码,上班第一件事拉取最新代码
    - 一定不要擅自修改同事的代码



### 4.6 标签

当某一个大版本完成之后,需要打一个标签，用于记录和备份大版本代码和版本号，便于之后的维护和回退。

#### 4.6.1 添加标签

```bash
echo "print('Over')" >> login.py

git commit -am "login开发完成"

# 添加标签
git tag -a v1.0 -m "login开发完成 version 1.0"

# 推送到远程仓库
git push origin v1.0
```

![](http://static.staryjie.com/static/images/20200921184646.png)



#### 4.6.2 查看标签

可以通过`git tag`查看版本标签信息：

```bash
git tag
```



#### 4.6.3 删除标签

* 删除本地标签

    ```bash
    git tag -d v1.0
    ```

* 删除远程标签

    ```bash
    git push origin --delete tag v1.0
    # 或者
    git push --delete v1.0
    ```



### 4.7 分支

- 作用：
    - 区分生产环境代码以及开发环境代码
    - 研究新的功能或者攻关难题
    - 解决线上bug
- 特点：
    - 项目开发中公用分支包括master、dev
    - 分支master是默认分支，用于发布，当需要发布时将dev分支合并到master分支
    - 分支dev是用于开发的分支，开发完阶段性的代码后，需要合并到master分支



#### 4.7.1 查看分支

```bash
git branch
```

![](http://static.staryjie.com/static/images/20200921210234.png)

> 在没有创建其他分支之前，只有一个master分支。



#### 4.7.2 创建分支

```bash
cd /opt/code/staryjie/newbee/
git pull

# 创建一个新的代码文件pay.py
touch pay.py

git commit -am "创建支付代码"
git push -u origin master

# 创建支付分支
git checkout -b pay

# 查看当前所在的分支
git branch

# 编写代码
echo "print('This is payment code')" >> pay.py

git commit -am "编写第一行pay代码"
git push -u origin pay
# 或者
git push --set-upstream origin pay
```

![](http://static.staryjie.com/static/images/20200921211640.png)



#### 4.7.3 切换分支

```bash
git checkout master
```

![](http://static.staryjie.com/static/images/20200921211305.png)

> pay分支提交代码之后，在其他分支是看不到它的内容的。



#### 3.7.4 合并分支

合并分支必须在master节点(希望被合并到哪个分支就在哪个分支，一般都在主分支上合并)上。

```bash
# 切换到master分支
git checkout master

# 检查是否在需要被合并的分支
git branch

# 合并分支
git merge pay

# 提交至远程仓库
git push -u origin master
```



#### 3.7.5 删除分支

```bash
# 删除本地分支
git branch -D dev

# 删除远程仓库分支
# 1.先切换到其他分支
git checkout master
git branch -D dev
git push origin --delete dev
```

