# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 23:02
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-调用自己定制的模块.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import my_module1


my_module1.testA(1, 2)
