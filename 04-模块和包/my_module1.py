# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 22:57
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : my_module1.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


def testA(a, b):
    print(a + b)


# __name__是系统变量，是模块标识符，如果是模块文件自身调用，则值为：__main__，否则就这个模块文件的文件名
# print(__name__)  # __main__

if __name__ == '__main__':
    # 测试代码, 只在当前文件运行中调用该函数才会执行，否则不会执行该函数
    testA(1, 2)
