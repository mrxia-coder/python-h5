# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 23:44
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 04-调用有__all__列表的模块.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


from my_module2 import *

testA()
testB()
