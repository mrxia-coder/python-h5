# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 23:33
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-模块的定位顺序.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 1. 自己的文件名不要和已有模块名重复，否则导致模块功能无法使用
# import random
# print(random.randint(0, 2))


# 2. 当使用from .. import xx的时候如果功能名字重复，那么调用的时候使用的是后调用的函数
# def sleep(n):
#     print(n)


# from time import sleep


# def sleep(n):
#     print(n)


# sleep(2)


import sys
print(sys.path)  # 可以看到Python查找模块的目录列表
