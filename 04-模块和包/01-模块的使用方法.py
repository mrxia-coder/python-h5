# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 22:48
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-模块的使用方法.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 1. import 模块名
import math

# 开平方
print(math.sqrt(9))  # 3.0


# 2. from ... import ...
from math import sqrt

# 开平方
print(sqrt(9))  # 3.0


# 3. from ... import \*
from math import *

# 开平方
print(sqrt(9))  # 3.0


# 4. as定义别名： import 模块名 as 别名
import time as tt
tt.sleep(2)
print("Hello")

from time import sleep as sl
sl(2)
print("Hello")
