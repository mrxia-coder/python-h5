# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 23:32
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : my_module2.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


__all__ = ["testA"]


def testA():
    print("testA")


def testB():
    print("testB")
