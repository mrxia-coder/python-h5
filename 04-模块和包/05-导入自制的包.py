# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 23:50
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 05-导入自制的包.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm

# 方法一
"""
1. 导入   import 包名.模块名
2. 调用   包名.模块名.功能()
"""
# import mypackage.my_module1


# mypackage.my_module1.info_print1()


# 方法二
"""
1. 导入   from 包名 import *
2. 调用   包名.功能()
"""
from mypackage import *


my_module1.info_print1()
my_module2.info_print2()
