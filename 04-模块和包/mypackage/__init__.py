# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/7 23:48
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : __init__.py.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


# 允许导入包中的所有模块
__all__ = ["my_module1", "my_module2"]
