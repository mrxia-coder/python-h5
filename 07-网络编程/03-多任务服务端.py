# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/12 22:05
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 03-多任务服务端.py
# @Version : v1.0.0
# @Desc    : 
# @ide     : PyCharm


import socket
import threading


def handle_client_requests(server_client_socket, ip_port):
    # 循环接收客户端发送的消息
    while True:
        recv_data = server_client_socket.recv(1024)
        if recv_data:
            # 打印接收消息
            print(f"接收到客户端{ip_port}消息：{recv_data.decode('gbk')}")

            # 回复消息
            server_client_socket.send(f"您的消息 [{recv_data.decode('gbk')}] 正在处理中...请稍等！".encode('gbk'))
        else:
            print(f"客户端{ip_port}已下线，关闭连接...")
            break

    server_client_socket.close()


if __name__ == '__main__':
    # 创建tcp服务端套接字
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 设置端口号复用，让程序退出端口号立即释放
    # 参数1: 表示当前套接字  socket.SOL_SOCKET
    # 参数2: 设置端口号复用选项  socket.SO_REUSEADDR
    # 参数3: 设置端口号复用选项对应的值  True
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

    # 给程序绑定端口号
    # 第一个参数可以不设置，表示监听本机任意一个IP地址
    tcp_server_socket.bind(("", 8080))

    # 设置监听
    # 128:最大等待建立连接的个数， 提示： 目前是单任务的服务端，同一时刻只能服务与一个客户端，后续使用多任务能够让服务端同时服务与多个客户端，
    # 不需要让客户端进行等待建立连接
    # listen后的这个套接字只负责接收客户端连接请求，不能收发消息，收发消息使用返回的这个新套接字来完成
    tcp_server_socket.listen(128)

    # 等待客户端建立连接，连接建立成功都会返回一个新的套接字，这个套接字负责与客户端进行通信
    # result = tcp_server_socket.accept()
    # print(result)  # (<socket.socket fd=508, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('192.168.50.29', 8080), raddr=('192.168.50.29', 4962)>, ('192.168.50.29', 4962))

    while True:
        server_client_socket, ip_port = tcp_server_socket.accept()
        print(f"客户端{ip_port}已经建立连接，开始通信...")
        # 通过建立新的子线程处理不同客户端的通信
        sub_thread = threading.Thread(target=handle_client_requests, args=(server_client_socket, ip_port))
        # 设置线程守护主线程
        sub_thread.setDaemon(True)
        # 启动子线程开始处理客户端消息
        sub_thread.start()

    # 关闭监听套接字,终止和客户端提供建立新的连接请求的服务，已经建立连接的客户端还能正常使用
    tcp_server_socket.close()  # 被动套接字可以不用关闭，因为服务端程序需要一直运行
