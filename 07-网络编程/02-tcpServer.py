# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/12 21:30
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 02-tcpServer.py
# @Version : v1.0.0
# @Desc    : TCP服务端代码
# @ide     : PyCharm


import socket

if __name__ == '__main__':
    # 创建tcp服务端套接字
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 设置端口号复用，让程序退出端口号立即释放
    # 参数1: 表示当前套接字  socket.SOL_SOCKET
    # 参数2: 设置端口号复用选项  socket.SO_REUSEADDR
    # 参数3: 设置端口号复用选项对应的值  True
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

    # 给程序绑定端口号
    # 第一个参数可以不设置，表示监听本机任意一个IP地址
    tcp_server_socket.bind(("", 8080))

    # 设置监听
    # 128:最大等待建立连接的个数， 提示： 目前是单任务的服务端，同一时刻只能服务与一个客户端，后续使用多任务能够让服务端同时服务与多个客户端，
    # 不需要让客户端进行等待建立连接
    # listen后的这个套接字只负责接收客户端连接请求，不能收发消息，收发消息使用返回的这个新套接字来完成
    tcp_server_socket.listen(128)

    # 等待客户端建立连接，连接建立成功都会返回一个新的套接字，这个套接字负责与客户端进行通信
    # result = tcp_server_socket.accept()
    # print(result)  # (<socket.socket fd=508, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('192.168.50.29', 8080), raddr=('192.168.50.29', 4962)>, ('192.168.50.29', 4962))
    server_client_socket, ip_port = tcp_server_socket.accept()
    """
        tcp_server_socket这个套接字只负责监听客户端连接请求，不与任何客户端进行其他的操作，
        server_client_socket这个新的套接字负责与建立连接的客户端进行通信、收发消息，类似于一个客服
    """
    # print(server_client_socket)
    print(f"客户端的IP地址为:{ip_port[0]}\t端口号为:{ip_port[1]}")

    # 接收客户端发送的数据
    recv_data = server_client_socket.recv(1024)
    print(f"客户端发送消息长度为:{len(recv_data)}")
    print(f"接收到客户端消息：{recv_data.decode('gbk')}")

    # 发送数据到客户端
    send_data = "已经收到请求，正在审批...请等待！".encode("gbk")
    server_client_socket.send(send_data)

    # 关闭与客户端通信的套接字
    server_client_socket.close()

    # 关闭监听套接字,终止和客户端提供建立新的连接请求的服务，已经建立连接的客户端还能正常使用
    tcp_server_socket.close()
