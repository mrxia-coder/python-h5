# -*- coding: utf-8 -*-
# @Project : Hm-Python5
# @Time    : 2020/8/12 21:12
# @Author  : staryjie
# @Site    : blog.staryjie.com
# @File    : 01-tcpClient.py
# @Version : v1.0.0
# @Desc    : TCP客户端代码
# @ide     : PyCharm


import socket


if __name__ == '__main__':
    # 创建TCP客户端套接字
    # 1. AF_INET: 表示IPV4
    # 2. SOCK_STREAM：TCP传输协议
    tcp_client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_client_socket.bind(("", 65534))  # 客户端不用强制绑定端口号
    # 和服务端程序建立连接
    tcp_client_socket.connect(("192.168.50.29", 8080))
    # 代码执行到这里，连接建立成功

    while True:
        # 准备发生数据
        send_data = input("请输入要发送的消息(quit退出)：").strip()
        if send_data.lower() == 'quit':
            break
        # 发生数据
        tcp_client_socket.send(send_data.encode("gbk"))

        # 接收数据
        recv_data = tcp_client_socket.recv(1024)
        # recd_data接收到的数据是二进制的
        print(recv_data.decode('gbk'))

        # 对二进制数据进行编码
        recv_content = recv_data.decode("gbk")
        print(f"接收到服务端的数据为：{recv_content}")

    # 关闭套接字
    tcp_client_socket.close()
