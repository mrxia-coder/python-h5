# Python学习笔记



# 1、Python基础语法

### 1.1 变量和数据类型

* [1.1.1 Python简介](01-Python基础语法/Part01-变量和数据类型/Notes/01-Python简介.md)

* [1.1.2 Python解释器](01-Python基础语法/Part01-变量和数据类型/Notes/02-Python解释器.md)

* [1.1.3 注释](01-Python基础语法/Part01-变量和数据类型/Notes/03-注释.md)

* [1.1.4 变量](01-Python基础语法/Part01-变量和数据类型/Notes/04-变量.md)

* [1.1.5 Bug和Debug](01-Python基础语法/Part01-变量和数据类型/Notes/05-bug和debug.md)

* [1.1.6 认识数据类型](01-Python基础语法/Part01-变量和数据类型/Notes/06-认识数据类型.md)

* [1.1.7 格式化输出](01-Python基础语法/Part01-变量和数据类型/Notes/07-输出.md)



### 1.2 数据类型转换和运算符

* [1.2.1 输入](01-Python基础语法/Part02-数据类型转换和运算符/Notes/01-输入.md)
* [1.2.2 数据类型转换](01-Python基础语法/Part02-数据类型转换和运算符/Notes/02-数据类型转换.md)
* [1.2.3 运算符](01-Python基础语法/Part02-数据类型转换和运算符/Notes/03-运算符.md)



### 1.3 if条件语句

* [1.3.1 条件语句](01-Python基础语法/Part03-if语句/Notes/01-条件语句.md)
* [1.3.2 三目运算符](01-Python基础语法/Part03-if语句/Notes/02-三目运算符.md)



### 1.4 循环语句

* [1.4.1 循环](01-Python基础语法/Part04-循环/Notes/循环.md)



### 1.5 数据序列

* [1.5.1 字符串](01-Python基础语法/Part05-数据序列/Notes/01-字符串.md)
* [1.5.2 列表](01-Python基础语法/Part05-数据序列/Notes/02-列表.md)
* [1.5.3 元祖](01-Python基础语法/Part05-数据序列/Notes/03-元祖.md)
* [1.5.4 字典](01-Python基础语法/Part05-数据序列/Notes/04-字典.md)
* [1.5.5 集合](01-Python基础语法/Part05-数据序列/Notes/05-集合.md)
* [1.5.6 公共操作方法](01-Python基础语法/Part05-数据序列/Notes/06-公共操作.md)
* [1.5.7 推导式](01-Python基础语法/Part05-数据序列/Notes/07-推导式.md)



### 1.6 函数

* [1.6.1 函数基础](01-Python基础语法/Part06-函数/Notes/01-函数基础.md)
* [1.6.2 函数提高](01-Python基础语法/Part06-函数/Notes/02-函数提高.md)
* [1.6.3 函数应用：学员管理系统](01-Python基础语法/Part06-函数/Notes/03-函数应用-学员管理系统.md)
* [1.6.4 递归函数](01-Python基础语法/Part06-函数/Notes/04-递归函数.md)
* [1.6.5 lambda表达式(匿名函数)](01-Python基础语法/Part06-函数/Notes/05-lambda表达式(匿名函数).md)
* [1.6.6 高阶函数](01-Python基础语法/Part06-函数/Notes/06-高阶函数.md)



### 1.7 文件操作

* [1.7.1 文件操作](01-Python基础语法/Part07-文件操作/Notes/01-文件操作的作用.md)



### 1.8 面向对象编程

* [1.8.1 面向对象基础](02-面向对象编程/Part01-面向对象基础/Notes/01-面向对象基础.md)
* [1.8.2 面向对象-继承](02-面向对象编程/Part02-面向对象-继承/Notes/01-面向对象-继承.md)
* [1.8.2 多态-类方法和类属性](02-面向对象编程/Part03-多态-类方法和类属性/Notes/01-多态-类方法和类属性.md)



### 1.9 异常

* [1.9. 1 异常](03-异常/Notes/01-异常.md)



### 1.10 模块和包

* [1.10.1 模块和包](04-模块和包/Notes/01-模块和包.md)



### 1.11 案例：面向对象版学员管理系统

* [面向对象版学员管理系统文档](05-案例-面向对象版学员管理系统/Notes/01-面向对象版学员管理系统.md)



## 2、Python高级

### 2.1 多任务编程

* [多任务编程](06-多任务编程/Notes/01-多任务编程.md)



### 2.2 网络编程

* [网络编程](07-网络编程)



### 2.2 HTTP协议和静态Web服务器

* [2.2.1 HTTP协议](08-HTTP协议和静态Web服务器/Part01-HTTP协议/Notes/01-HTTP协议.md)
* [2.2.2 静态Web服务器](08-HTTP协议和静态Web服务器/Part02-静态Web服务器/Notes/01-静态Web服务器.md)



### 2.3 前端开发基础

* [2.3.1 HTML和CSS](09-前端开发基础/Part01-HTML+CSS/Notes/01-HTML&CSS.md)
* [2.3.2 JavaScript](09-前端开发基础/Part02-JavaScript/Notes/01-javascript.md)
* [2.3.3 jQuery](09-前端开发基础/Part03-jQuery/Notes/01-jQuery.md)



### 2.4 MySQL数据库

* [2.4.1 MySQL数据库的基本使用](10-MySQL数据库/Part01-MySQL数据库的基本使用/Notes/01-MySQL数据库的基本使用.md)
* [2.4.2 MySQL数据库的条件查询](10-MySQL数据库/Part02-MySQL数据库的条件查询/Notes/02-MySQL数据库的条件查询.md)
* [2.4.3 MySQL数据量的高级使用](10-MySQL数据库/Part03-MySQL数据库的高级使用/Notes/03-MySQL数据库的高级使用.md)



### 2.5 闭包和装饰器

* [2.5.1 闭包](11-闭包和装饰器/Notes/1-闭包.md)
* [2.5.2 装饰器](11-闭包和装饰器/Notes/2-装饰器.md)



### 2.6 mini-web框架

* [2.6.1 mini-web框架](12-mini-web框架/Notes/1-mini-web框架.md)



### 2.7 正则表达式

* [2.7.1 property属性、with语句和上下文管理器](13-正则表达式/Notes/1-property属性、with语句和上下文管理器.md)
* [2.7.2 生成器、深浅拷贝](13-正则表达式/Notes/2-生成器、深浅拷贝.md)
* [2.7.3 正则表达式](13-正则表达式/Notes/3-正则表达式.md)



### 2.8 Redis数据库

* [2.8.1 Redis数据库基础](14-redis数据库/Notes/1-redis数据库.md)
* [2.8.2 Redis数据库高级](14-redis数据库/Notes/2-redis数据库高级操作.md)



### 2.9 Django框架

* [2.9.1 Django框架流程](15-Django框架/Part01-Django流程/Notes/01-Django流程.md)
* [2.9.2 Django模型](15-Django框架/Part02-Django模型/Notes/01-Django模型.md)
* [2.9.3 Django视图](15-Django框架/Part03-Django视图/Notes/01-Django视图.md)
* [2.9.4 Django模板](15-Django框架/Part04-Django模板/Notes/01-Django模板.md)



### 2.10 Git管理源代码

* [2.10.1 Git管理源代码](16-Git/Notes/01-git管理源代码.md)



### 2.11 前端框架Vue.js

* [2.11.1 Vue基本使用](17-Vue/Notes/01-vue的基本使用.md)

